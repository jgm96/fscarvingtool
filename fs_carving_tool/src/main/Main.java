package main;

import java.awt.EventQueue;
import java.util.Locale;
import javax.swing.UIManager;

import logic.Application;
import ui.gui.window.MainWindow;


/**
 * The Class Main.
 */
public class Main {

	/**
	 * Main application class. Launches program.
	 *
	 * @param args the arguments
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				runApp();			
			}
		});
	}
	
	/**
	 * Run app.
	 */
	private static void runApp() {
		try {			
			Application app = new Application();
			Locale.setDefault(new Locale(app.getFsSettingsManager().getSettings().get("locale")));			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());			
			@SuppressWarnings("unused")			
			MainWindow window = new MainWindow(app);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
