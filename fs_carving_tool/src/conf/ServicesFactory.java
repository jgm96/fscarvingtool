package conf;

import business.FsCaseService;
import business.FsExtensionService;
import business.impl.FsCaseServiceImpl;
import business.impl.FsExtensionServiceImpl;

public class ServicesFactory {

	public static FsCaseService getFsCaseService() {
		return new FsCaseServiceImpl();
	}

	public static FsExtensionService getFsExtensionService() {
		return new FsExtensionServiceImpl();
	}

}
