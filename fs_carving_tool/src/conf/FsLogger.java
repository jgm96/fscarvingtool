package conf;

import org.apache.log4j.Logger;

/**
 * The Class FsLogger.
 */
public class FsLogger {
	
	/** The logger. */
	private static Logger logger = Logger.getLogger(FsLogger.class.getName());
	
	/**
	 * Gets the single instance of FsLogger.
	 *
	 * @return single instance of FsLogger
	 */
	public static Logger getInstance() {
		return logger;
	}	
	

}
