package ui.gui.swing;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.JPanel;
import javax.swing.JTree;


/**
 * The Class FileTree.
 */
public class FileTree extends JPanel {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The file tree. */
	private JTree fileTree;	
	
	/** The file system model. */
	private FileSystemModel fileSystemModel;

	/**
	 *  Construct a FileTree.
	 *
	 * @param directory the directory
	 */
	public FileTree(File directory) {
		setLayout(new BorderLayout());		
		fileSystemModel = new FileSystemModel(directory);
	    fileTree = new JTree(fileSystemModel);
	    fileTree.setEditable(true);
	    add(BorderLayout.CENTER, fileTree);	    

	}
	
	/**
	 * Reload tree.
	 *
	 * @param directory the directory
	 */
	public void reloadTree(File directory) {
		fileSystemModel = new FileSystemModel(directory);
		fileTree.setModel(fileSystemModel);		
	}	

}
