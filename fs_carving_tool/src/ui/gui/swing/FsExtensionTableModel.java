package ui.gui.swing;

import javax.swing.table.DefaultTableModel;


/**
 * The Class FsExtensionTableModel.
 */
public class FsExtensionTableModel extends DefaultTableModel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new fs extension table model.
	 *
	 * @param columnNames the column names
	 * @param rowCount the row count
	 */
	public FsExtensionTableModel(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

}
