package ui.gui.window;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.help.HelpSetException;
import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileSystemView;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;

import com.toedter.calendar.JCalendar;

import conf.FsLogger;
import logic.Application;
import logic.io.reader.FileSigsGCK_Reader;
import model.FsCase;
import model.FsExtension;
import model.exception.PresentationException;
import ui.gui.Messages;
import ui.gui.swing.FileTree;
import util.HexUtil;
import util.StringUtil;


/**
 * The Class MainWindow.
 */
public class MainWindow {

	/** The frm fs forensics. */
	private JFrame frmFsForensics;
	
	/** The menu bar. */
	private JMenuBar menuBar;
	
	/** The mn file. */
	private JMenu mnFile;
	
	/** The mntm quit. */
	private JMenuItem mntmQuit;
	
	/** The mntm new case. */
	private JMenuItem mntmNewCase;
	
	/** The mntm open case. */
	private JMenuItem mntmOpenCase;
	
	/** The sp 1. */
	private JSeparator sp1;
	
	/** The mn help. */
	private JMenu mnHelp;
	
	/** The mntm about. */
	private JMenuItem mntmAbout;
	
	/** The mn edit. */
	private JMenu mnEdit;
	
	/** The mn file extensions. */
	private JMenu mnFileExtensions;
	
	/** The mntm new extension. */
	private JMenuItem mntmNewExtension;
	
	/** The mntm import extension. */
	private JMenuItem mntmImportExtension;
	
	/** The sp 2. */
	private JSeparator sp2;
	
	/** The sp 3. */
	private JSeparator sp3;
	
	/** The mntm preferences. */
	private JMenuItem mntmPreferences;
	
	/** The mntm manage extensions. */
	private JMenuItem mntmManageExtensions;
	
	/** The mntm contents. */
	private JMenuItem mntmContents;
	
	/** The logo icon. */
	private ImageIcon logoIcon;
	
	/** The metadata img icon. */
	private ImageIcon metadataImgIcon;
	
	/** The app. */
	private Application app;
	
	/** The jfc. */
	private JFileChooser jfc;
	
	/** The extensions window. */
	private ExtensionsWindow extensionsWindow;
	
	/** The calendar. */
	private JCalendar calendar;
	
	/** The loading icon. */
	private ImageIcon loadingIcon = new ImageIcon(getClass().getResource("/ui/gui/img/ajax-loader.gif"));
	
	/** The case update listener. */
	private KeyListener caseUpdateListener = new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent e) {
			checkSaveCaseButtonEnabled();
		}
	};

	/** The current dir. */
	private File currentDir;

	/** String routes. */
	private String logoPath = "/ui/gui/img/fingerprint_search.png";
	
	/** The mntm load gtk file. */
	private JMenuItem mntmLoadGtkFile;
	
	/** The sp 0. */
	private JSeparator sp0;
	
	/** The mntm reset extensions. */
	private JMenuItem mntmResetExtensions;
	
	/** The pn central. */
	private JPanel pnCentral;
	
	/** The pn options. */
	private JPanel pnOptions;
	
	/** The btn manage case. */
	private JButton btnManageCase;
	
	/** The btn manage extensions. */
	private JButton btnManageExtensions;
	
	/** The btn metadata extractor. */
	private JButton btnMetadataExtractor;
	
	/** The btn file signature analyzer. */
	private JButton btnFileSignatureAnalyzer;
	
	/** The pn case management. */
	private JPanel pnCaseManagement;
	
	/** The pn metadata extractor. */
	private JPanel pnMetadataExtractor;
	
	/** The pn file signature analyzer. */
	private JPanel pnFileSignatureAnalyzer;
	
	/** The btn about. */
	private JButton btnAbout;
	
	/** The lbl metadata extractor. */
	private JLabel lblMetadataExtractor;
	
	/** The lbl select A file. */
	private JLabel lblSelectAFile;
	
	/** The btn browse. */
	private JButton btnBrowse;
	
	/** The btn save. */
	private JButton btnSave;
	
	/** The scroll pane. */
	private JScrollPane scrollPane;
	
	/** The pn thumbnail. */
	private JPanel pnThumbnail;
	
	/** The lbl file. */
	private JLabel lblFile;
	
	/** The lbl size. */
	private JLabel lblSize;
	
	/** The pn preview. */
	private JPanel pnPreview;
	
	/** The lbl manage current case. */
	private JLabel lblManageCurrentCase;
	
	/** The lbl case name. */
	private JLabel lblCaseName;
	
	/** The lbl investigator. */
	private JLabel lblInvestigator;
	
	/** The lbl organization. */
	private JLabel lblOrganization;
	
	/** The lbl contact details. */
	private JLabel lblContactDetails;
	
	/** The tx case name. */
	private JTextField txCaseName;
	
	/** The tx investigator. */
	private JTextField txInvestigator;
	
	/** The tx organization. */
	private JTextField txOrganization;
	
	/** The tx contact details. */
	private JTextField txContactDetails;
	
	/** The lbl case folder. */
	private JLabel lblCaseFolder;
	
	/** The tx case folder. */
	private JTextField txCaseFolder;
	
	/** The button. */
	private JButton button;
	
	/** The check box log case activity. */
	private JCheckBox checkBoxLogCaseActivity;
	
	/** The lbl case date. */
	private JLabel lblCaseDate;
	
	/** The pn calendar. */
	private JPanel pnCalendar;
	
	/** The btn save case. */
	private JButton btnSaveCase;
	
	/** The pn welcome. */
	private JPanel pnWelcome;
	
	/** The tp metadata. */
	private JTextPane tpMetadata;

	/** ***************** METADATA **********************. */
	private JLabel lblThumbnail;
	
	/** The pn details. */
	private JPanel pnDetails;
	
	/** The btn extract metadata. */
	private JButton btnExtractMetadata;
	
	/** The lbl extension. */
	private JLabel lblExtension;
	
	/** The pn image. */
	private JPanel pnImage;
	
	/** The pn system icon. */
	private JPanel pnSystemIcon;
	
	/** The lbl systemicon. */
	private JLabel lblSystemicon;
	
	/** The lbl icon. */
	private JLabel lblIcon;
	
	/** The lbl path. */
	private JLabel lblPath;

	/** The logger. */
	private Logger logger = FsLogger.getInstance();
	
	/** The lbl select A directory. */
	private JLabel lblSelectADirectory;
	
	/** The bt file signature browse. */
	private JButton btFileSignatureBrowse;
	
	/** The bt file signature save. */
	private JButton btFileSignatureSave;
	
	/** The btn file signature analyse. */
	private JButton btnFileSignatureAnalyse;
	
	/** The lbl file signature analysis. */
	private JLabel lblFileSignatureAnalysis;
	
	/** The chckbx recursive search. */
	private JCheckBox chckbxRecursiveSearch;
	
	/** The split pane fs analysis. */
	private JSplitPane splitPaneFsAnalysis;
	
	/** The sp file system view. */
	private JScrollPane spFileSystemView;
	
	/** The sp file analysis output. */
	private JScrollPane spFileAnalysisOutput;
	
	/** The tp file analysis output. */
	private JTextPane tpFileAnalysisOutput;
	
	/** The pn file system view. */
	private JPanel pnFileSystemView;
	
	/** The lbl fs forensics icon. */
	private JLabel lblFsForensicsIcon;
	
	/** The lbl welcome to fs. */
	private JLabel lblWelcomeToFs;
	
	/** The pn icon. */
	private JPanel pnIcon;
	
	/** The pn quick acess menu. */
	private JPanel pnQuickAcessMenu;
	
	/** The btn load case. */
	private JButton btnLoadCase;
	
	/** The btn help. */
	private JButton btnHelp;
	
	/** The btn preferences. */
	private JButton btnPreferences;
	
	/** The lbl description. */
	private JLabel lblDescription;
	
	/** The tx description. */
	private JTextField txDescription;
	
	/** The chckbx extract metadata. */
	private JCheckBox chckbxExtractMetadata;
	
	/** The chckbx show detailed output. */
	private JCheckBox chckbxShowDetailedOutput;
	
	/** The pn RDS query tool. */
	private JPanel pnRDSQueryTool;
	
	/** The split pane RDS explorer. */
	private JSplitPane splitPaneRDSExplorer;
	
	/** The sp RDS file system view. */
	private JScrollPane spRDSFileSystemView;
	
	/** The sp RDS analysis output. */
	private JScrollPane spRDSAnalysisOutput;
	
	/** The lbl select RDS directory. */
	private JLabel lblSelectRDSDirectory;
	
	/** The btn RDS browse. */
	private JButton btnRDSBrowse;
	
	/** The btn RDS save. */
	private JButton btnRDSSave;
	
	/** The btn RDS analyse. */
	private JButton btnRDSAnalyse;
	
	/** The label. */
	private JLabel label;
	
	/** The pn RDS file system view. */
	private FileTree pnRDSFileSystemView;
	
	/** The btn rds query tool. */
	private JButton btnRdsQueryTool;
	
	/** The tp RDS query tool. */
	private JTextPane tpRDSQueryTool;
	
	/** The lbl loading. */
	private JLabel lblLoading;
	
	/** The lbl loading file sig. */
	private JLabel lblLoadingFileSig;

	/**
	 * Creates Main Window.
	 *
	 * @param app the app
	 */
	public MainWindow(Application app) {
		this.app = app;
		logoIcon = new ImageIcon(getClass().getResource(logoPath));
		initialize();
		loadHelp();
		currentDir = SystemUtils.IS_OS_LINUX ? new File("/home/") : new File("C:/");				
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFsForensics = new JFrame();
		frmFsForensics.setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource(logoPath)));
		frmFsForensics.setTitle(Messages.getString("MainWindow.Title.text")); //$NON-NLS-1$
		frmFsForensics.setBounds(100, 100, 947, 582);
		frmFsForensics.setMinimumSize(new Dimension(765, 481));
		frmFsForensics.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFsForensics.setJMenuBar(getMenuBar());
		frmFsForensics.getContentPane().setLayout(new BorderLayout(0, 0));
		frmFsForensics.getContentPane().add(getPnOptions(), BorderLayout.WEST);
		frmFsForensics.getContentPane().add(getPnCentral(), BorderLayout.CENTER);
		frmFsForensics.setLocationRelativeTo(null);
		frmFsForensics.setVisible(true);
		showPanels(false, false, false, false, false);
		enableCaseButtons(false);
		frmFsForensics.pack();
		Image image = logoIcon.getImage();
		Image newimg = image.getScaledInstance(pnIcon.getWidth(), pnIcon.getHeight(), Image.SCALE_SMOOTH);
		logoIcon = new ImageIcon(newimg);
		lblFsForensicsIcon.setIcon(logoIcon);
	}

	/**
	 * Gets the frm fs forensics.
	 *
	 * @return the frm fs forensics
	 */
	public JFrame getFrmFsForensics() {
		return frmFsForensics;
	}

	/**
	 * Gets the menu bar.
	 *
	 * @return the menu bar
	 */
	private JMenuBar getMenuBar() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getMnFile());
			menuBar.add(getMnEdit());
			menuBar.add(getMnHelp());
		}
		return menuBar;
	}

	/**
	 * Gets the mn file.
	 *
	 * @return the mn file
	 */
	private JMenu getMnFile() {
		if (mnFile == null) {
			mnFile = new JMenu(Messages.getString("MainWindow.mnFile.text")); //$NON-NLS-1$
			mnFile.setHorizontalAlignment(SwingConstants.LEFT);
			mnFile.add(getMntmNewCase());
			mnFile.add(getMntmOpenCase());
			mnFile.add(getSp1());
			mnFile.add(getMntmQuit());
		}
		return mnFile;
	}

	/**
	 * Gets the mntm quit.
	 *
	 * @return the mntm quit
	 */
	private JMenuItem getMntmQuit() {
		if (mntmQuit == null) {
			mntmQuit = new JMenuItem(Messages.getString("MainWindow.mnQuit.text")); //$NON-NLS-1$
			mntmQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
			mntmQuit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
		}
		return mntmQuit;
	}

	/**
	 * Gets the mntm new case.
	 *
	 * @return the mntm new case
	 */
	private JMenuItem getMntmNewCase() {
		if (mntmNewCase == null) {
			mntmNewCase = new JMenuItem(Messages.getString("MainWindow.mntmNewCase.text"));
			mntmNewCase.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					launchNewFsCaseWindow();
				}
			});
			mntmNewCase.setHorizontalTextPosition(SwingConstants.LEFT);
		}
		return mntmNewCase;
	}

	/**
	 * Launch new fs case window.
	 */
	private void launchNewFsCaseWindow() {
		NewFsCaseDialog dialog = new NewFsCaseDialog(app, this, null);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
	}

	/**
	 * Gets the mntm open case.
	 *
	 * @return the mntm open case
	 */
	private JMenuItem getMntmOpenCase() {
		if (mntmOpenCase == null) {
			mntmOpenCase = new JMenuItem(Messages.getString("MainWindow.mntmOpenCase.text")); //$NON-NLS-1$
			mntmOpenCase.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					launchCasesWindow();
				}
			});
		}
		return mntmOpenCase;
	}

	/**
	 * Launch cases window.
	 */
	private void launchCasesWindow() {
		LoadCasesDialog dialog = new LoadCasesDialog(app, this);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
	}

	/**
	 * Gets the sp 1.
	 *
	 * @return the sp 1
	 */
	private JSeparator getSp1() {
		if (sp1 == null) {
			sp1 = new JSeparator();
		}
		return sp1;
	}

	/**
	 * Gets the mn help.
	 *
	 * @return the mn help
	 */
	private JMenu getMnHelp() {
		if (mnHelp == null) {
			mnHelp = new JMenu(Messages.getString("MainWindow.mnHelp.text")); //$NON-NLS-1$
			mnHelp.add(getMntmContents());
			mnHelp.add(getMntmAbout());
		}
		return mnHelp;
	}

	/**
	 * Gets the mntm about.
	 *
	 * @return the mntm about
	 */
	private JMenuItem getMntmAbout() {
		if (mntmAbout == null) {
			mntmAbout = new JMenuItem(Messages.getString("MainWindow.mntmAbout.text")); //$NON-NLS-1$
			mntmAbout.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					showAbout();
				}
			});
		}
		return mntmAbout;
	}

	/**
	 * Gets the mn edit.
	 *
	 * @return the mn edit
	 */
	private JMenu getMnEdit() {
		if (mnEdit == null) {
			mnEdit = new JMenu(Messages.getString("MainWindow.mnEdit.text")); //$NON-NLS-1$
			mnEdit.add(getMnFileExtensions());
			mnEdit.add(getSp3());
			mnEdit.add(getMntmPreferences());
		}
		return mnEdit;
	}

	/**
	 * Gets the mn file extensions.
	 *
	 * @return the mn file extensions
	 */
	private JMenu getMnFileExtensions() {
		if (mnFileExtensions == null) {
			mnFileExtensions = new JMenu(Messages.getString("MainWindow.mnFileExtensions.text")); //$NON-NLS-1$
			mnFileExtensions.add(getMntmNewExtension());
			mnFileExtensions.add(getSp2());
			mnFileExtensions.add(getMntmImportExtension());
			mnFileExtensions.add(getMntmLoadGtkFile());
			mnFileExtensions.add(getSp0());
			mnFileExtensions.add(getMntmManageExtensions());
			mnFileExtensions.add(getMntmResetExtensions());
		}
		return mnFileExtensions;
	}

	/**
	 * Gets the mntm new extension.
	 *
	 * @return the mntm new extension
	 */
	private JMenuItem getMntmNewExtension() {
		if (mntmNewExtension == null) {
			mntmNewExtension = new JMenuItem(Messages.getString("MainWindow.mntmNewExtension.text")); //$NON-NLS-1$
			mntmNewExtension.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JDialog dialog = new NewFsExtensionDialog(app, getMainWindow(), null);
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setModal(true);
					dialog.setVisible(true);
				}
			});
		}
		return mntmNewExtension;
	}

	/**
	 * Gets the main window.
	 *
	 * @return the main window
	 */
	private MainWindow getMainWindow() {
		return this;
	}

	/**
	 * Gets the mntm import extension.
	 *
	 * @return the mntm import extension
	 */
	private JMenuItem getMntmImportExtension() {
		if (mntmImportExtension == null) {
			mntmImportExtension = new JMenuItem(Messages.getString("MainWindow.mntmImportExtension.text")); //$NON-NLS-1$
			mntmImportExtension.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
					jfc.setMultiSelectionEnabled(true);
					int returnValue = jfc.showOpenDialog(null);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						for (File file : jfc.getSelectedFiles()) {
							try {
								FsExtension fsExtensionNew = FileSigsGCK_Reader.getInstance()
										.loadGTKFsExtension((file));
								FsExtension fsExtensionOld = app.getFsExtensionManager()
										.getFsExtensionByNameAndFileSignature(fsExtensionNew.getExtension(),
												HexUtil.bytesToHex(fsExtensionNew.getFileSignature()));
								if (fsExtensionOld != null) {
									int reply = JOptionPane.showConfirmDialog(getFrmFsForensics(),
											Messages.getString("NewFsExtensionWindow.UpdateQuestion.text"),
											Messages.getString("Shared.Cofirmation.text"), JOptionPane.YES_NO_OPTION,
											JOptionPane.QUESTION_MESSAGE);
									if (reply == JOptionPane.YES_OPTION) {
										app.getFsExtensionManager().updateFsExtension(fsExtensionOld, fsExtensionNew);
										JOptionPane.showMessageDialog(getFrmFsForensics(),
												Messages.getString("MainWindow.FileImportSuccess.text"),
												Messages.getString("Shared.Message.text"),
												JOptionPane.INFORMATION_MESSAGE);
									}
								} else {
									app.getFsExtensionManager().newFsExtension(fsExtensionNew);
									JOptionPane.showMessageDialog(getFrmFsForensics(),
											Messages.getString("MainWindow.FileImportSuccess.text"),
											Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
								}
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage());
							} catch (PresentationException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage());
								JOptionPane.showMessageDialog(getFrmFsForensics(),
										Messages.getString("MainWindow.FileImportError.text"),
										Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
							}
						}
					}
				}
			});
		}
		return mntmImportExtension;
	}

	/**
	 * Gets the sp 2.
	 *
	 * @return the sp 2
	 */
	private JSeparator getSp2() {
		if (sp2 == null) {
			sp2 = new JSeparator();
		}
		return sp2;
	}

	/**
	 * Gets the sp 3.
	 *
	 * @return the sp 3
	 */
	private JSeparator getSp3() {
		if (sp3 == null) {
			sp3 = new JSeparator();
		}
		return sp3;
	}

	/**
	 * Gets the mntm preferences.
	 *
	 * @return the mntm preferences
	 */
	private JMenuItem getMntmPreferences() {
		if (mntmPreferences == null) {
			mntmPreferences = new JMenuItem(Messages.getString("MainWindow.mntmPreferences.text")); //$NON-NLS-1$
			mntmPreferences.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					launchPreferencesDialog();
				}
			});
		}
		return mntmPreferences;
	}

	/**
	 * Gets the mntm manage extensions.
	 *
	 * @return the mntm manage extensions
	 */
	private JMenuItem getMntmManageExtensions() {
		if (mntmManageExtensions == null) {
			mntmManageExtensions = new JMenuItem(Messages.getString("MainWindow.mntmManageExtensions.text")); //$NON-NLS-1$
			mntmManageExtensions.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					displayExtensionsWindow();
				}
			});
		}
		return mntmManageExtensions;
	}

	/**
	 * Gets the mntm contents.
	 *
	 * @return the mntm contents
	 */
	private JMenuItem getMntmContents() {
		if (mntmContents == null) {
			mntmContents = new JMenuItem(Messages.getString("MainWindow.mntmContents.text"));
			mntmContents.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		}
		return mntmContents;
	}

	/**
	 * Gets the mntm load gtk file.
	 *
	 * @return the mntm load gtk file
	 */
	private JMenuItem getMntmLoadGtkFile() {
		if (mntmLoadGtkFile == null) {
			mntmLoadGtkFile = new JMenuItem(Messages.getString("MainWindow.mntmLoadGckFile.text")); //$NON-NLS-1$
			mntmLoadGtkFile.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					if (app.getFsSettingsManager().isImportedFsSignatures()) {
						JOptionPane.showMessageDialog(getFrmFsForensics(),
								Messages.getString("MainWindow.FileExtensionsSetAlreadyImported.text"),
								Messages.getString("Shared.Message.text"), JOptionPane.WARNING_MESSAGE);
					} else {
						jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
						int returnValue = jfc.showOpenDialog(null);
						if (returnValue == JFileChooser.APPROVE_OPTION) {
							File selectedFile = jfc.getSelectedFile();
							try {
								app.getFsExtensionManager().createFsExtensions(
										FileSigsGCK_Reader.getInstance().loadGTKFsExtensions(selectedFile));
								app.getFsExtensionManager().deleteRepeatedExtensions();
								app.getFsSettingsManager().setImportedFsSignatures(true);
								JOptionPane.showMessageDialog(getFrmFsForensics(),
										Messages.getString("MainWindow.FileImportSuccess.text"),
										Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage());
							} catch (PresentationException e2) {
								JOptionPane.showMessageDialog(getFrmFsForensics(),
										Messages.getString("MainWindow.FileImportError.text"),
										Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
								e2.printStackTrace();
								logger.error(e2.getMessage());
							}
						}
					}
				}

			});
		}
		return mntmLoadGtkFile;
	}

	/**
	 * Gets the sp 0.
	 *
	 * @return the sp 0
	 */
	private JSeparator getSp0() {
		if (sp0 == null) {
			sp0 = new JSeparator();
		}
		return sp0;
	}

	/**
	 * Gets the mntm reset extensions.
	 *
	 * @return the mntm reset extensions
	 */
	private JMenuItem getMntmResetExtensions() {
		if (mntmResetExtensions == null) {
			mntmResetExtensions = new JMenuItem(Messages.getString("MainWindow.mntmResetExtensions.text")); //$NON-NLS-1$
			mntmResetExtensions.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int reply = JOptionPane.showConfirmDialog(getFrmFsForensics(),
							Messages.getString("MainWindow.MessageConfirmFullDeletion.text"),
							Messages.getString("Shared.Cofirmation.text"), JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);
					if (reply == JOptionPane.YES_OPTION) {
						int rows = 0;
						try {
							rows = app.getFsExtensionManager().deleteAllFsExtensions();
							app.getFsSettingsManager().setImportedFsSignatures(false);
							JOptionPane.showMessageDialog(getFrmFsForensics(),
									rows + " " + Messages.getString("MainWindow.MessageDeletedRows.text"),
									Messages.getString("Shared.Info.text"), JOptionPane.INFORMATION_MESSAGE);
						} catch (PresentationException e1) {
							e1.printStackTrace();
							logger.error(e1.getMessage());
						}
					}
				}
			});
		}
		return mntmResetExtensions;
	}

	/**
	 * Gets the pn central.
	 *
	 * @return the pn central
	 */
	private JPanel getPnCentral() {
		if (pnCentral == null) {
			pnCentral = new JPanel();
			pnCentral.setLayout(new CardLayout(0, 0));
			pnCentral.add(getPnWelcome(), "name_148064914136847");
			pnCentral.add(getPnCaseManagement(), "name_1041554955005845");
			pnCentral.add(getPnMetadataExtractor(), "name_1041594889209607");
			pnCentral.add(getPnFileSignatureAnalyzer(), "name_1041662314109719");
			pnCentral.add(getPnRDSQueryTool(), "name_602073446026000");
		}
		return pnCentral;
	}

	/**
	 * Gets the pn options.
	 *
	 * @return the pn options
	 */
	private JPanel getPnOptions() {
		if (pnOptions == null) {
			pnOptions = new JPanel();
			pnOptions.setLayout(new GridLayout(0, 1, 0, 0));
			pnOptions.add(getBtnManageCase());
			pnOptions.add(getBtnManageExtensions());
			pnOptions.add(getBtnMetadataExtractor());
			pnOptions.add(getBtnFileSignatureAnalyzer());
			pnOptions.add(getBtnRdsQueryTool());
			pnOptions.add(getBtnAbout());
		}
		return pnOptions;
	}

	/**
	 * Gets the btn manage case.
	 *
	 * @return the btn manage case
	 */
	private JButton getBtnManageCase() {
		if (btnManageCase == null) {
			btnManageCase = new JButton(Messages.getString("MainWindow.btnManageCase.text")); //$NON-NLS-1$
			btnManageCase.setHorizontalAlignment(SwingConstants.LEFT);
			btnManageCase.setHorizontalTextPosition(SwingConstants.RIGHT);
			ImageIcon img = new ImageIcon(getClass().getResource("/ui/gui/img/case_details_logo.png"));
			Image newImg = img.getImage().getScaledInstance(40, 43, Image.SCALE_SMOOTH);
			btnManageCase.setIcon(new ImageIcon(newImg));
			btnManageCase.setIconTextGap(15);
			btnManageCase.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fillManageCasesPanel();
					showPanels(false, true, false, false, false);
				}
			});
		}
		return btnManageCase;
	}

	/**
	 * Gets the btn manage extensions.
	 *
	 * @return the btn manage extensions
	 */
	private JButton getBtnManageExtensions() {
		if (btnManageExtensions == null) {
			btnManageExtensions = new JButton(Messages.getString("MainWindow.btnManageExtensions.text")); //$NON-NLS-1$
			btnManageExtensions.setHorizontalAlignment(SwingConstants.LEFT);
			ImageIcon img = new ImageIcon(getClass().getResource("/ui/gui/img/file_extensions_management_icon.png"));
			Image newImg = img.getImage().getScaledInstance(35, 35, Image.SCALE_SMOOTH);
			btnManageExtensions.setIcon(new ImageIcon(newImg));
			btnManageExtensions.setIconTextGap(15);
			btnManageExtensions.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					displayExtensionsWindow();
					if (app.getCurrentFsCase() != null) {
						showPanels(false, false, false, false, false);
					}
				}
			});
		}
		return btnManageExtensions;
	}

	/**
	 * Fill manage cases panel.
	 */
	public void fillManageCasesPanel() {
		FsCase current = app.getCurrentFsCase();
		txCaseName.setText(current.getname());
		txCaseFolder.setText(current.getCaseFolder());
		txContactDetails.setText(current.getContactDetails());
		txInvestigator.setText(current.getInvestigator());
		txOrganization.setText(current.getOrganization());
		txDescription.setText(current.getDescription());
		calendar.setDate(current.getCaseDate());
		checkBoxLogCaseActivity.setSelected(current.isLogCaseActivity());
	}

	/**
	 * Gets the btn metadata extractor.
	 *
	 * @return the btn metadata extractor
	 */
	private JButton getBtnMetadataExtractor() {
		if (btnMetadataExtractor == null) {
			btnMetadataExtractor = new JButton(Messages.getString("MainWindow.btnMetadataExtractor.text")); //$NON-NLS-1$
			btnMetadataExtractor.setHorizontalAlignment(SwingConstants.LEFT);
			ImageIcon img = new ImageIcon(getClass().getResource("/ui/gui/img/metadata_icon.png"));
			Image newImg = img.getImage().getScaledInstance(35, 35, Image.SCALE_SMOOTH);
			btnMetadataExtractor.setIcon(new ImageIcon(newImg));
			btnMetadataExtractor.setIconTextGap(15);
			btnMetadataExtractor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					showPanels(false, false, true, false, false);
				}
			});
		}
		return btnMetadataExtractor;
	}

	/**
	 * Gets the btn file signature analyzer.
	 *
	 * @return the btn file signature analyzer
	 */
	private JButton getBtnFileSignatureAnalyzer() {
		if (btnFileSignatureAnalyzer == null) {
			btnFileSignatureAnalyzer = new JButton(Messages.getString("MainWindow.btnFileSignatureAnalyzer.text")); //$NON-NLS-1$
			btnFileSignatureAnalyzer.setHorizontalAlignment(SwingConstants.LEFT);
			ImageIcon img = new ImageIcon(getClass().getResource("/ui/gui/img/checksum_icon.png"));
			Image newImg = img.getImage().getScaledInstance(40, 43, Image.SCALE_SMOOTH);
			btnFileSignatureAnalyzer.setIcon(new ImageIcon(newImg));
			btnFileSignatureAnalyzer.setIconTextGap(15);
			btnFileSignatureAnalyzer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					showPanels(false, false, false, true, false);
				}
			});
		}
		return btnFileSignatureAnalyzer;
	}

	/**
	 * Gets the pn case management.
	 *
	 * @return the pn case management
	 */
	private JPanel getPnCaseManagement() {
		if (pnCaseManagement == null) {
			pnCaseManagement = new JPanel();
			pnCaseManagement.setVisible(false);
			GroupLayout gl_pnCaseManagement = new GroupLayout(pnCaseManagement);
			gl_pnCaseManagement.setHorizontalGroup(
				gl_pnCaseManagement.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_pnCaseManagement.createSequentialGroup()
						.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnCaseManagement.createSequentialGroup()
								.addGap(32)
								.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.TRAILING)
									.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_pnCaseManagement.createSequentialGroup()
											.addGap(14)
											.addComponent(getLblInvestigator(), GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
										.addGroup(gl_pnCaseManagement.createSequentialGroup()
											.addGap(19)
											.addComponent(getLblCaseName(), GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))
										.addGroup(gl_pnCaseManagement.createSequentialGroup()
											.addGap(12)
											.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
												.addComponent(getLblDescription(), GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
												.addComponent(getLblOrganization(), GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))))
									.addComponent(getLblContactDetails(), GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
									.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
										.addComponent(getLblCaseDate())
										.addComponent(getLblCaseFolder(), GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)))
								.addGap(18)
								.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.TRAILING)
									.addGroup(Alignment.LEADING, gl_pnCaseManagement.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(getTxCaseFolder(), GroupLayout.PREFERRED_SIZE, 461, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
										.addComponent(getButton(), GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED))
									.addGroup(gl_pnCaseManagement.createSequentialGroup()
										.addComponent(getPnCalendar(), GroupLayout.PREFERRED_SIZE, 243, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
											.addGroup(gl_pnCaseManagement.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
												.addComponent(getCheckBoxLogCaseActivity(), GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
												.addGap(46))
											.addGroup(gl_pnCaseManagement.createSequentialGroup()
												.addGap(68)
												.addComponent(getBtnSaveCase()))))
									.addGroup(gl_pnCaseManagement.createSequentialGroup()
										.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.TRAILING)
											.addComponent(getTxContactDetails(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE)
											.addComponent(getTxDescription(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE)
											.addComponent(getTxCaseName(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE)
											.addComponent(getTxOrganization(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE)
											.addComponent(getTxInvestigator(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE))
										.addGap(50))))
							.addGroup(gl_pnCaseManagement.createSequentialGroup()
								.addGap(34)
								.addComponent(getLblManageCurrentCase())))
						.addGap(15))
			);
			gl_pnCaseManagement.setVerticalGroup(
				gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnCaseManagement.createSequentialGroup()
						.addContainerGap()
						.addComponent(getLblManageCurrentCase(), GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.BASELINE)
							.addComponent(getTxCaseName(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblCaseName()))
						.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnCaseManagement.createSequentialGroup()
								.addGap(14)
								.addComponent(getLblInvestigator()))
							.addGroup(gl_pnCaseManagement.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(getTxInvestigator(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnCaseManagement.createSequentialGroup()
								.addGap(14)
								.addComponent(getLblOrganization()))
							.addGroup(gl_pnCaseManagement.createSequentialGroup()
								.addGap(11)
								.addComponent(getTxOrganization(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblDescription())
							.addComponent(getTxDescription(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblContactDetails())
							.addComponent(getTxContactDetails(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblCaseFolder())
							.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.BASELINE)
								.addComponent(getTxCaseFolder(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getButton())))
						.addGroup(gl_pnCaseManagement.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnCaseManagement.createSequentialGroup()
								.addGap(70)
								.addComponent(getCheckBoxLogCaseActivity())
								.addGap(38)
								.addComponent(getBtnSaveCase()))
							.addGroup(gl_pnCaseManagement.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(getLblCaseDate()))
							.addGroup(gl_pnCaseManagement.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getPnCalendar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGap(67))
			);
			pnCaseManagement.setLayout(gl_pnCaseManagement);
		}
		return pnCaseManagement;
	}

	/**
	 * Gets the pn metadata extractor.
	 *
	 * @return the pn metadata extractor
	 */
	private JPanel getPnMetadataExtractor() {
		if (pnMetadataExtractor == null) {
			pnMetadataExtractor = new JPanel();
			GroupLayout gl_pnMetadataExtractor = new GroupLayout(pnMetadataExtractor);
			gl_pnMetadataExtractor.setHorizontalGroup(gl_pnMetadataExtractor.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_pnMetadataExtractor.createSequentialGroup().addContainerGap()
							.addGroup(gl_pnMetadataExtractor.createParallelGroup(Alignment.TRAILING)
									.addGroup(gl_pnMetadataExtractor.createSequentialGroup()
											.addComponent(getPnThumbnail(), GroupLayout.PREFERRED_SIZE, 261,
													GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED).addComponent(getScrollPane(),
													GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE))
									.addGroup(gl_pnMetadataExtractor.createSequentialGroup().addGap(14)
											.addGroup(gl_pnMetadataExtractor.createParallelGroup(Alignment.LEADING)
													.addGroup(gl_pnMetadataExtractor.createSequentialGroup()
															.addComponent(getLblSelectAFile()).addGap(16)
															.addComponent(getBtnBrowse(), GroupLayout.PREFERRED_SIZE,
																	86, GroupLayout.PREFERRED_SIZE)
															.addPreferredGap(ComponentPlacement.RELATED)
															.addComponent(getBtnSave(), GroupLayout.PREFERRED_SIZE, 87,
																	GroupLayout.PREFERRED_SIZE)
															.addPreferredGap(ComponentPlacement.RELATED, 205,
																	Short.MAX_VALUE)
															.addComponent(getBtnExtractMetadata(),
																	GroupLayout.PREFERRED_SIZE, 107,
																	GroupLayout.PREFERRED_SIZE))
													.addGroup(gl_pnMetadataExtractor.createSequentialGroup()
															.addComponent(getLblMetadataExtractor())
															.addPreferredGap(ComponentPlacement.RELATED, 390,
																	Short.MAX_VALUE)))))
							.addGap(16)));
			gl_pnMetadataExtractor.setVerticalGroup(gl_pnMetadataExtractor.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnMetadataExtractor.createSequentialGroup().addGap(26)
							.addComponent(getLblMetadataExtractor()).addGap(26)
							.addGroup(gl_pnMetadataExtractor.createParallelGroup(Alignment.BASELINE)
									.addComponent(getLblSelectAFile()).addComponent(getBtnExtractMetadata())
									.addComponent(getBtnSave()).addComponent(getBtnBrowse()))
							.addGap(18)
							.addGroup(gl_pnMetadataExtractor.createParallelGroup(Alignment.LEADING)
									.addComponent(getPnThumbnail(), GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
									.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE))
							.addContainerGap()));
			pnMetadataExtractor.setLayout(gl_pnMetadataExtractor);
		}
		return pnMetadataExtractor;
	}

	/**
	 * Gets the pn file signature analyzer.
	 *
	 * @return the pn file signature analyzer
	 */
	private JPanel getPnFileSignatureAnalyzer() {
		if (pnFileSignatureAnalyzer == null) {
			pnFileSignatureAnalyzer = new JPanel();
			GroupLayout gl_pnFileSignatureAnalyzer = new GroupLayout(pnFileSignatureAnalyzer);
			gl_pnFileSignatureAnalyzer.setHorizontalGroup(gl_pnFileSignatureAnalyzer
					.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnFileSignatureAnalyzer.createSequentialGroup().addGroup(gl_pnFileSignatureAnalyzer
							.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnFileSignatureAnalyzer.createSequentialGroup().addContainerGap().addComponent(
									getSplitPaneFsAnalysis(), GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE))
							.addGroup(gl_pnFileSignatureAnalyzer.createSequentialGroup().addGap(24)
									.addGroup(gl_pnFileSignatureAnalyzer.createParallelGroup(Alignment.LEADING)
											.addComponent(getLblFileSignatureAnalysis(), GroupLayout.PREFERRED_SIZE,
													299, GroupLayout.PREFERRED_SIZE)
											.addGroup(gl_pnFileSignatureAnalyzer.createSequentialGroup()
													.addComponent(getLblSelectADirectory(), GroupLayout.PREFERRED_SIZE,
															111, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.UNRELATED)
													.addComponent(getBtFileSignatureBrowse(),
															GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(getBtFileSignatureSave(), GroupLayout.PREFERRED_SIZE,
															87, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addGroup(gl_pnFileSignatureAnalyzer
															.createParallelGroup(Alignment.LEADING)
															.addGroup(gl_pnFileSignatureAnalyzer.createSequentialGroup()
																	.addComponent(getChckbxExtractMetadata(),
																			GroupLayout.DEFAULT_SIZE, 114,
																			Short.MAX_VALUE)
																	.addPreferredGap(ComponentPlacement.RELATED)
																	.addComponent(getChckbxRecursiveSearch())
																	.addGap(39))
															.addGroup(gl_pnFileSignatureAnalyzer.createSequentialGroup()
																	.addComponent(getChckbxShowDetailedOutput(),
																			GroupLayout.PREFERRED_SIZE, 140,
																			GroupLayout.PREFERRED_SIZE)
																	.addGap(120)))
													.addGroup(gl_pnFileSignatureAnalyzer
															.createParallelGroup(Alignment.LEADING, false)
															.addComponent(getBtnFileSignatureAnalyse(),
																	GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
															.addComponent(getLblLoadingFileSig(),
																	GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
																	Short.MAX_VALUE))))))
							.addGap(5)));
			gl_pnFileSignatureAnalyzer.setVerticalGroup(gl_pnFileSignatureAnalyzer
					.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnFileSignatureAnalyzer.createSequentialGroup().addGap(26)
							.addComponent(getLblFileSignatureAnalysis(), GroupLayout.PREFERRED_SIZE, 19,
									GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_pnFileSignatureAnalyzer.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_pnFileSignatureAnalyzer.createSequentialGroup().addGap(1)
											.addGroup(gl_pnFileSignatureAnalyzer.createParallelGroup(Alignment.LEADING)
													.addComponent(getChckbxShowDetailedOutput())
													.addComponent(getLblLoadingFileSig(), GroupLayout.PREFERRED_SIZE,
															16, GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(ComponentPlacement.RELATED)
											.addGroup(gl_pnFileSignatureAnalyzer.createParallelGroup(Alignment.LEADING)
													.addGroup(gl_pnFileSignatureAnalyzer
															.createParallelGroup(Alignment.BASELINE)
															.addComponent(getBtFileSignatureSave())
															.addComponent(getBtFileSignatureBrowse())
															.addComponent(getChckbxExtractMetadata(),
																	GroupLayout.PREFERRED_SIZE, 22,
																	GroupLayout.PREFERRED_SIZE))
													.addGroup(gl_pnFileSignatureAnalyzer
															.createParallelGroup(Alignment.BASELINE)
															.addComponent(getBtnFileSignatureAnalyse(),
																	GroupLayout.PREFERRED_SIZE, 23,
																	GroupLayout.PREFERRED_SIZE)
															.addComponent(getChckbxRecursiveSearch()))))
									.addGroup(gl_pnFileSignatureAnalyzer.createSequentialGroup().addGap(30)
											.addComponent(getLblSelectADirectory())))
							.addGap(18)
							.addComponent(getSplitPaneFsAnalysis(), GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
							.addGap(4)));
			pnFileSignatureAnalyzer.setLayout(gl_pnFileSignatureAnalyzer);
		}
		return pnFileSignatureAnalyzer;
	}

	/**
	 * Gets the btn about.
	 *
	 * @return the btn about
	 */
	private JButton getBtnAbout() {
		if (btnAbout == null) {
			btnAbout = new JButton(Messages.getString("MainWindow.btnAbout.text")); //$NON-NLS-1$
			btnAbout.setHorizontalAlignment(SwingConstants.LEFT);
			ImageIcon img = new ImageIcon(getClass().getResource("/ui/gui/img/about_icon.png"));
			Image newImg = img.getImage().getScaledInstance(35, 35, Image.SCALE_SMOOTH);
			btnAbout.setIcon(new ImageIcon(newImg));
			btnAbout.setIconTextGap(15);
			btnAbout.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					showAbout();
				}
			});
		}
		return btnAbout;
	}

	/**
	 * Gets the lbl metadata extractor.
	 *
	 * @return the lbl metadata extractor
	 */
	private JLabel getLblMetadataExtractor() {
		if (lblMetadataExtractor == null) {
			lblMetadataExtractor = new JLabel(Messages.getString("MainWindow.lblMetadataExtractor.text")); //$NON-NLS-1$
			lblMetadataExtractor.setFont(new Font("Tahoma", Font.BOLD, 15));
		}
		return lblMetadataExtractor;
	}

	/**
	 * Gets the lbl select A file.
	 *
	 * @return the lbl select A file
	 */
	private JLabel getLblSelectAFile() {
		if (lblSelectAFile == null) {
			lblSelectAFile = new JLabel(Messages.getString("MainWindow.lblSelectAFile.text")); //$NON-NLS-1$
		}
		return lblSelectAFile;
	}

	/**
	 * Gets the btn browse.
	 *
	 * @return the btn browse
	 */
	private JButton getBtnBrowse() {
		if (btnBrowse == null) {
			btnBrowse = new JButton(Messages.getString("MainWindow.btnBrowse.text")); //$NON-NLS-1$
			btnBrowse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
					int returnValue = jfc.showOpenDialog(null);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						app.setMetadataCurrentFile(jfc.getSelectedFile());
						String extension = FilenameUtils.getExtension(app.getMetadataCurrentFile().getAbsolutePath());
						try {
							if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("jpeg")
									|| extension.equalsIgnoreCase("png") || extension.equalsIgnoreCase("tiff")) {
								Image img = ImageIO.read(app.getMetadataCurrentFile()).getScaledInstance(100, 75,
										Image.SCALE_SMOOTH);
								lblThumbnail.setIcon(new ImageIcon(img));
								if (app.getCurrentFsCase().isLogCaseActivity()) {
									logger.info("Loaded image file. ");
									logger.info(StringUtil.generateFileHeader(app.getMetadataCurrentFile()));
								}
							} else {
								metadataImgIcon = new ImageIcon(getClass().getResource("/ui/gui/img/generic_file.jpg"));
								lblThumbnail.setIcon(new ImageIcon(
										metadataImgIcon.getImage().getScaledInstance(100, 75, Image.SCALE_SMOOTH)));
								logger.info("Loaded file: ");
								logger.info(StringUtil.generateFileHeader(app.getMetadataCurrentFile()));
							}
							fillDetailLabels();
							btnExtractMetadata.setEnabled(true);
						} catch (IOException e1) {
							e1.printStackTrace();
							logger.error(e1.getMessage());
						}
					}

				}
			});
		}
		return btnBrowse;

	}

	/**
	 * Fill detail labels.
	 */
	private void fillDetailLabels() {
		File metadataCurrentFile = app.getMetadataCurrentFile();

		Icon ico = FileSystemView.getFileSystemView().getSystemIcon(metadataCurrentFile);
		Image img = ((ImageIcon) ico).getImage().getScaledInstance(20, 20, Image.SCALE_SMOOTH);
		lblSystemicon.setIcon(new ImageIcon(img));
		tpMetadata.setText("");
		StringBuilder sb = new StringBuilder(64);
		sb.append("<html>");
		sb.append(Messages.getString("MainWindow.MetadataExtractor.lblFilename.text")).append("  ")
				.append(FilenameUtils.getBaseName(metadataCurrentFile.getName()));
		sb.append("</html>");
		lblFile.setText(sb.toString());
		lblSize.setText(Messages.getString("MainWindow.MetadataExtractor.lblSize.text") + "  "
				+ metadataCurrentFile.length() / 1024 + " KB");
		lblExtension.setText(Messages.getString("MainWindow.MetadataExtractor.lblExtension.text") + "  "
				+ FilenameUtils.getExtension(metadataCurrentFile.getAbsolutePath()).toUpperCase());
		sb = new StringBuilder(64);
		sb.append("<html>");
		sb.append(Messages.getString("MainWindow.lblPath.text")).append("  ")
				.append(metadataCurrentFile.getParentFile().getAbsolutePath());
		sb.append("</html>");
		lblPath.setText(sb.toString());
	}

	/**
	 * Gets the btn save.
	 *
	 * @return the btn save
	 */
	private JButton getBtnSave() {
		if (btnSave == null) {
			btnSave = new JButton(Messages.getString("MainWindow.btnSave.text")); //$NON-NLS-1$
			btnSave.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						app.exportMetadataToTextFile();
						JOptionPane.showMessageDialog(getFrmFsForensics(),
								Messages.getString("MainWindow.MetadataExportSuccess.text"),
								Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
						if (app.getCurrentFsCase().isLogCaseActivity()) {
							logger.info(
									"Metadata saved to file in directory: " + app.getCurrentFsCase().getCaseFolder());
						}
					} catch (PresentationException e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(getFrmFsForensics(),
								Messages.getString("MainWindow.MetadataExportError.text"),
								Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
						logger.error(e1.getMessage());
					}
				}
			});
			btnSave.setEnabled(false);
		}
		return btnSave;
	}

	/**
	 * Gets the scroll pane.
	 *
	 * @return the scroll pane
	 */
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBorder(new TitledBorder(null, Messages.getString("MainWindow.MetadataPanelTitle.text"),
					TitledBorder.LEADING, TitledBorder.TOP, null, null));
			scrollPane.setViewportView(getTpMetadata());
		}
		return scrollPane;
	}

	/**
	 * Gets the pn thumbnail.
	 *
	 * @return the pn thumbnail
	 */
	private JPanel getPnThumbnail() {
		if (pnThumbnail == null) {
			pnThumbnail = new JPanel();
			GroupLayout gl_pnThumbnail = new GroupLayout(pnThumbnail);
			gl_pnThumbnail.setHorizontalGroup(gl_pnThumbnail.createParallelGroup(Alignment.TRAILING)
					.addComponent(getPnPreview(), Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 261,
							GroupLayout.PREFERRED_SIZE)
					.addComponent(getPnDetails(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE));
			gl_pnThumbnail.setVerticalGroup(gl_pnThumbnail.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnThumbnail.createSequentialGroup()
							.addComponent(getPnPreview(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
									GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getPnDetails(), GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)));
			pnThumbnail.setLayout(gl_pnThumbnail);
		}
		return pnThumbnail;
	}

	/**
	 * Gets the lbl file.
	 *
	 * @return the lbl file
	 */
	private JLabel getLblFile() {
		if (lblFile == null) {
			lblFile = new JLabel(Messages.getString("MainWindow.MetadataExtractor.lblFilename.text")); //$NON-NLS-1$
			lblFile.setFont(lblFile.getFont().deriveFont(Font.BOLD));
		}
		return lblFile;
	}

	/**
	 * Gets the lbl size.
	 *
	 * @return the lbl size
	 */
	private JLabel getLblSize() {
		if (lblSize == null) {
			lblSize = new JLabel(Messages.getString("MainWindow.MetadataExtractor.lblSize.text"));
			lblSize.setFont(lblFile.getFont().deriveFont(Font.BOLD));
		}
		return lblSize;
	}

	/**
	 * Gets the pn preview.
	 *
	 * @return the pn preview
	 */
	private JPanel getPnPreview() {
		if (pnPreview == null) {
			pnPreview = new JPanel();
			pnPreview.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
					Messages.getString("MainWindow.MetadataExtractor.Preview.text"), TitledBorder.LEADING,
					TitledBorder.TOP, null, new Color(0, 0, 0)));
			GroupLayout gl_pnPreview = new GroupLayout(pnPreview);
			gl_pnPreview.setHorizontalGroup(gl_pnPreview.createParallelGroup(Alignment.LEADING).addGroup(gl_pnPreview
					.createSequentialGroup().addGap(39)
					.addGroup(gl_pnPreview.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_pnPreview.createSequentialGroup()
									.addComponent(getPnSystemIcon(), GroupLayout.PREFERRED_SIZE, 24,
											GroupLayout.PREFERRED_SIZE)
									.addGap(35))
							.addGroup(gl_pnPreview.createSequentialGroup()
									.addComponent(getLblIcon(), GroupLayout.PREFERRED_SIZE, 51,
											GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)))
					.addComponent(getPnImage(), GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(23, Short.MAX_VALUE)));
			gl_pnPreview.setVerticalGroup(gl_pnPreview.createParallelGroup(Alignment.LEADING).addGroup(gl_pnPreview
					.createSequentialGroup().addGap(22)
					.addGroup(gl_pnPreview.createParallelGroup(Alignment.LEADING)
							.addComponent(getPnImage(), GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_pnPreview.createSequentialGroup()
									.addComponent(getLblIcon(), GroupLayout.PREFERRED_SIZE, 10,
											GroupLayout.PREFERRED_SIZE)
									.addGap(11).addComponent(getPnSystemIcon(), GroupLayout.PREFERRED_SIZE, 34,
											GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(26, Short.MAX_VALUE)));
			pnPreview.setLayout(gl_pnPreview);
		}
		return pnPreview;
	}

	/**
	 * Show panels.
	 *
	 * @param welcome the welcome
	 * @param manageCases the manage cases
	 * @param metadataExtractor the metadata extractor
	 * @param fileSignatureAnalysis the file signature analysis
	 * @param rdsQueryTool the rds query tool
	 */
	public void showPanels(boolean welcome, boolean manageCases, boolean metadataExtractor,
			boolean fileSignatureAnalysis, boolean rdsQueryTool) {
		pnWelcome.setVisible(welcome);
		pnMetadataExtractor.setVisible(metadataExtractor);
		pnFileSignatureAnalyzer.setVisible(fileSignatureAnalysis);
		pnCaseManagement.setVisible(manageCases);
		pnRDSQueryTool.setVisible(rdsQueryTool);
	}

	/**
	 * Show about.
	 */
	private void showAbout() {
		Image image = logoIcon.getImage();
		Image newimg = image.getScaledInstance(90, 100, Image.SCALE_SMOOTH);
		logoIcon = new ImageIcon(newimg);
		JOptionPane.showMessageDialog(getFrmFsForensics(),
				Messages.getString("MainWindow.AboutContentDescription.text") + "\n\n"
						+ Messages.getString("MainWindow.AboutContentVersion.text") + "\n"
						+ Messages.getString("MainWindow.AboutContentCopyright.text") + "\n"
						+ Messages.getString("MainWindow.AboutContentDegree.text") + "\n"
						+ Messages.getString("MainWindow.AboutContentDeveloper.text") + "\n"
						+ Messages.getString("MainWindow.AboutContentDirector.text"),
				Messages.getString("MainWindow.AboutHeader.text"), JOptionPane.INFORMATION_MESSAGE, logoIcon);
	}

	/**
	 * Display extensions window.
	 */
	private void displayExtensionsWindow() {
		extensionsWindow = new ExtensionsWindow(app, this);
		extensionsWindow.setVisible(true);
	}

	/**
	 * Gets the lbl manage current case.
	 *
	 * @return the lbl manage current case
	 */
	private JLabel getLblManageCurrentCase() {
		if (lblManageCurrentCase == null) {
			lblManageCurrentCase = new JLabel(Messages.getString("MainWindow.lblManageCurrentCase.text")); //$NON-NLS-1$
			lblManageCurrentCase.setFont(new Font("Tahoma", Font.BOLD, 15));
		}
		return lblManageCurrentCase;
	}

	/**
	 * Gets the lbl case name.
	 *
	 * @return the lbl case name
	 */
	private JLabel getLblCaseName() {
		if (lblCaseName == null) {
			lblCaseName = new JLabel(Messages.getString("NewFsCaseDialog.CaseName.text")); //$NON-NLS-1$
		}
		return lblCaseName;
	}

	/**
	 * Gets the lbl investigator.
	 *
	 * @return the lbl investigator
	 */
	private JLabel getLblInvestigator() {
		if (lblInvestigator == null) {
			lblInvestigator = new JLabel(Messages.getString("NewFsCaseDialog.Investigator.text")); //$NON-NLS-1$
		}
		return lblInvestigator;
	}

	/**
	 * Gets the lbl organization.
	 *
	 * @return the lbl organization
	 */
	private JLabel getLblOrganization() {
		if (lblOrganization == null) {
			lblOrganization = new JLabel(Messages.getString("NewFsCaseDialog.Organization.text")); //$NON-NLS-1$
		}
		return lblOrganization;
	}

	/**
	 * Gets the lbl contact details.
	 *
	 * @return the lbl contact details
	 */
	private JLabel getLblContactDetails() {
		if (lblContactDetails == null) {
			lblContactDetails = new JLabel(Messages.getString("NewFsCaseDialog.ContactDetails.text")); //$NON-NLS-1$
		}
		return lblContactDetails;
	}

	/**
	 * Gets the tx case name.
	 *
	 * @return the tx case name
	 */
	private JTextField getTxCaseName() {
		if (txCaseName == null) {
			txCaseName = new JTextField();
			txCaseName.setColumns(10);
			txCaseName.addKeyListener(caseUpdateListener);
		}
		return txCaseName;
	}

	/**
	 * Gets the tx investigator.
	 *
	 * @return the tx investigator
	 */
	private JTextField getTxInvestigator() {
		if (txInvestigator == null) {
			txInvestigator = new JTextField();
			txInvestigator.setColumns(10);
			txInvestigator.addKeyListener(caseUpdateListener);
		}
		return txInvestigator;
	}

	/**
	 * Gets the tx organization.
	 *
	 * @return the tx organization
	 */
	private JTextField getTxOrganization() {
		if (txOrganization == null) {
			txOrganization = new JTextField();
			txOrganization.setColumns(10);
			txOrganization.addKeyListener(caseUpdateListener);
		}
		return txOrganization;
	}

	/**
	 * Gets the tx contact details.
	 *
	 * @return the tx contact details
	 */
	private JTextField getTxContactDetails() {
		if (txContactDetails == null) {
			txContactDetails = new JTextField();
			txContactDetails.setColumns(10);
			txContactDetails.addKeyListener(caseUpdateListener);
		}
		return txContactDetails;
	}

	/**
	 * Gets the lbl case folder.
	 *
	 * @return the lbl case folder
	 */
	private JLabel getLblCaseFolder() {
		if (lblCaseFolder == null) {
			lblCaseFolder = new JLabel(Messages.getString("NewFsCaseDialog.CaseFolder.text")); //$NON-NLS-1$
		}
		return lblCaseFolder;
	}

	/**
	 * Gets the tx case folder.
	 *
	 * @return the tx case folder
	 */
	private JTextField getTxCaseFolder() {
		if (txCaseFolder == null) {
			txCaseFolder = new JTextField();
			txCaseFolder.setText("\r\n");
			txCaseFolder.setEnabled(false);
			txCaseFolder.setColumns(10);
		}
		return txCaseFolder;
	}

	/**
	 * Gets the button.
	 *
	 * @return the button
	 */
	private JButton getButton() {
		if (button == null) {
			button = new JButton(Messages.getString("MainWindow.btnBrowse.text")); //$NON-NLS-1$
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JFileChooser chooser = new JFileChooser();
					chooser.setCurrentDirectory(currentDir); // $NON-NLS-1$
					chooser.setDialogTitle(Messages.getString("NewFsCaseDialog.SelectDirectory.text"));
					chooser.setAcceptAllFileFilterUsed(false);
					chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
						txCaseFolder.setText(chooser.getSelectedFile().getAbsolutePath());
						checkSaveCaseButtonEnabled();
					}
				}
			});
		}
		return button;
	}

	/**
	 * Gets the check box log case activity.
	 *
	 * @return the check box log case activity
	 */
	private JCheckBox getCheckBoxLogCaseActivity() {
		if (checkBoxLogCaseActivity == null) {
			checkBoxLogCaseActivity = new JCheckBox(Messages.getString("MainWindow.checkBoxLogCaseActivity.text")); //$NON-NLS-1$
			checkBoxLogCaseActivity.addItemListener(new ItemListener() {

				public void itemStateChanged(ItemEvent e) {
					checkSaveCaseButtonEnabled();
				}
			});
		}
		return checkBoxLogCaseActivity;
	}

	/**
	 * Gets the lbl case date.
	 *
	 * @return the lbl case date
	 */
	private JLabel getLblCaseDate() {
		if (lblCaseDate == null) {
			lblCaseDate = new JLabel(Messages.getString("MainWindow.lblCaseDate.text")); //$NON-NLS-1$
		}
		return lblCaseDate;
	}

	/**
	 * Gets the pn calendar.
	 *
	 * @return the pn calendar
	 */
	private JPanel getPnCalendar() {
		if (pnCalendar == null) {
			pnCalendar = new JPanel();
			calendar = new JCalendar();
			pnCalendar.add(calendar);
			calendar.addPropertyChangeListener("calendar", new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent e) {
					checkSaveCaseButtonEnabled();
				}
			});
			calendar.setTodayButtonVisible(true);

		}
		return pnCalendar;
	}

	/**
	 * Gets the btn save case.
	 *
	 * @return the btn save case
	 */
	private JButton getBtnSaveCase() {
		if (btnSaveCase == null) {
			btnSaveCase = new JButton(Messages.getString("MainWindow.btnSaveCase.text")); //$NON-NLS-1$
			btnSaveCase.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					FsCase current = app.getCurrentFsCase();
					current.setCaseDate(calendar.getDate());
					current.setCaseFolder(txCaseFolder.getText());
					current.setOrganization(txOrganization.getText());
					current.setDescription(txDescription.getText());
					current.setContactDetails(txContactDetails.getText());
					current.setname(txCaseName.getText());
					current.setLogCaseActivity(checkBoxLogCaseActivity.isSelected());
					try {
						app.getFsCaseManager().updateFsCase(current);
						btnSaveCase.setEnabled(false);
						if (current.isLogCaseActivity()) {
							logger.info("Updated FsCase: ");
							logger.info(StringUtil.generateCaseHeader(current));
						}
					} catch (PresentationException e1) {
						e1.printStackTrace();
						logger.error(e1.getMessage());
					}
				}
			});
			btnSaveCase.setEnabled(false);
		}
		return btnSaveCase;
	}

	/**
	 * Enable case buttons.
	 *
	 * @param b the b
	 */
	public void enableCaseButtons(boolean b) {
		btnFileSignatureAnalyzer.setEnabled(b);
		btnManageCase.setEnabled(b);
		btnMetadataExtractor.setEnabled(b);
		btnRdsQueryTool.setEnabled(b);
	}

	/**
	 * Gets the pn welcome.
	 *
	 * @return the pn welcome
	 */
	private JPanel getPnWelcome() {
		if (pnWelcome == null) {
			pnWelcome = new JPanel();
			GroupLayout gl_pnWelcome = new GroupLayout(pnWelcome);
			gl_pnWelcome.setHorizontalGroup(
				gl_pnWelcome.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnWelcome.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_pnWelcome.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnWelcome.createSequentialGroup()
								.addComponent(getPnQuickAcessMenu(), GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE)
								.addGap(18)
								.addComponent(getPnIcon(), GroupLayout.PREFERRED_SIZE, 294, GroupLayout.PREFERRED_SIZE)
								.addGap(45))
							.addComponent(getLblWelcomeToFs(), GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE))
						.addGap(59))
			);
			gl_pnWelcome.setVerticalGroup(
				gl_pnWelcome.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnWelcome.createSequentialGroup()
						.addGap(28)
						.addComponent(getLblWelcomeToFs(), GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addGap(36)
						.addGroup(gl_pnWelcome.createParallelGroup(Alignment.LEADING)
							.addComponent(getPnIcon(), GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
							.addComponent(getPnQuickAcessMenu(), GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE))
						.addGap(190))
			);
			pnWelcome.setLayout(gl_pnWelcome);
		}
		return pnWelcome;
	}

	/**
	 * Check save case button enabled.
	 */
	private void checkSaveCaseButtonEnabled() {
		FsCase current = app.getCurrentFsCase();
		if (current.getname().equals(txCaseName.getText()) && current.getCaseDate().equals(calendar.getDate())
				&& current.getCaseFolder().equals(txCaseFolder.getText())
				&& current.getContactDetails().equals(txContactDetails.getText())
				&& current.getOrganization().equals(txOrganization.getText())
				&& current.getDescription().equals(txDescription.getText())
				&& current.getInvestigator().equals(txInvestigator.getText())
				&& current.isLogCaseActivity() == checkBoxLogCaseActivity.isSelected()) {
			btnSaveCase.setEnabled(false);
		} else {
			btnSaveCase.setEnabled(true);
		}

	}

	/**
	 * Gets the tp metadata.
	 *
	 * @return the tp metadata
	 */
	private JTextPane getTpMetadata() {
		if (tpMetadata == null) {
			tpMetadata = new JTextPane();
			tpMetadata.setEditable(false);
			tpMetadata.setBorder(new LineBorder(new Color(0, 0, 0)));
			tpMetadata.setContentType("text/html");
		}
		return tpMetadata;
	}

	/**
	 * Gets the lbl thumbnail.
	 *
	 * @return the lbl thumbnail
	 */
	private JLabel getLblThumbnail() {
		if (lblThumbnail == null) {
			lblThumbnail = new JLabel();
			lblThumbnail.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return lblThumbnail;
	}

	/**
	 * Gets the pn details.
	 *
	 * @return the pn details
	 */
	private JPanel getPnDetails() {
		if (pnDetails == null) {
			pnDetails = new JPanel();
			pnDetails.setBorder(new TitledBorder(null, Messages.getString("MainWindow.MetadataExtractor.Details.text"),
					TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDetails.setLayout(new GridLayout(4, 0, 0, 0));
			pnDetails.add(getLblFile());
			pnDetails.add(getLblSize());
			pnDetails.add(getLblExtension());
			pnDetails.add(getLblPath());
		}
		return pnDetails;
	}

	/**
	 * Gets the btn extract metadata.
	 *
	 * @return the btn extract metadata
	 */
	private JButton getBtnExtractMetadata() {
		if (btnExtractMetadata == null) {
			btnExtractMetadata = new JButton(Messages.getString("MainWindow.btnExtractMetadata.text")); //$NON-NLS-1$
			btnExtractMetadata.setEnabled(false);
			btnExtractMetadata.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						tpMetadata.setText(app.getMetadataOfCurrentFile());
						btnSave.setEnabled(true);
					} catch (PresentationException e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(getFrmFsForensics(),
								Messages.getString("MainWindow.MetadataObtentionError.text"),
								Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
						logger.error(e1.getMessage());
					}
				}
			});
		}
		return btnExtractMetadata;
	}

	/**
	 * Gets the lbl extension.
	 *
	 * @return the lbl extension
	 */
	private JLabel getLblExtension() {
		if (lblExtension == null) {
			lblExtension = new JLabel(Messages.getString("MainWindow.MetadataExtractor.lblExtension.text"));
			lblExtension.setFont(lblExtension.getFont().deriveFont(Font.BOLD));
		}
		return lblExtension;
	}

	/**
	 * Gets the pn image.
	 *
	 * @return the pn image
	 */
	private JPanel getPnImage() {
		if (pnImage == null) {
			pnImage = new JPanel();
			pnImage.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			pnImage.setLayout(new BorderLayout(0, 0));
			pnImage.add(getLblThumbnail(), BorderLayout.CENTER);
		}
		return pnImage;
	}

	/**
	 * Gets the pn system icon.
	 *
	 * @return the pn system icon
	 */
	private JPanel getPnSystemIcon() {
		if (pnSystemIcon == null) {
			pnSystemIcon = new JPanel();
			pnSystemIcon.setBorder(null);
			pnSystemIcon.setLayout(new BorderLayout(0, 0));
			pnSystemIcon.add(getLblSystemicon(), BorderLayout.CENTER);
		}
		return pnSystemIcon;
	}

	/**
	 * Gets the lbl systemicon.
	 *
	 * @return the lbl systemicon
	 */
	private JLabel getLblSystemicon() {
		if (lblSystemicon == null) {
			lblSystemicon = new JLabel("");
			lblSystemicon.setBorder(null);
		}
		return lblSystemicon;
	}

	/**
	 * Gets the lbl icon.
	 *
	 * @return the lbl icon
	 */
	private JLabel getLblIcon() {
		if (lblIcon == null) {
			lblIcon = new JLabel(Messages.getString("MainWindow.lblIcon.text")); //$NON-NLS-1$
		}
		return lblIcon;
	}

	/**
	 * Gets the lbl path.
	 *
	 * @return the lbl path
	 */
	private JLabel getLblPath() {
		if (lblPath == null) {
			lblPath = new JLabel(Messages.getString("MainWindow.lblPath.text")); //$NON-NLS-1$
			lblPath.setFont(lblPath.getFont().deriveFont(Font.BOLD));
		}
		return lblPath;
	}

	/**
	 * Gets the lbl select A directory.
	 *
	 * @return the lbl select A directory
	 */
	private JLabel getLblSelectADirectory() {
		if (lblSelectADirectory == null) {
			lblSelectADirectory = new JLabel(Messages.getString("MainWindow.lblSelectDirectory.text")); //$NON-NLS-1$
		}
		return lblSelectADirectory;
	}

	/**
	 * Gets the bt file signature browse.
	 *
	 * @return the bt file signature browse
	 */
	private JButton getBtFileSignatureBrowse() {
		if (btFileSignatureBrowse == null) {
			btFileSignatureBrowse = new JButton(Messages.getString("MainWindow.btnBrowse.text")); //$NON-NLS-1$
			btFileSignatureBrowse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
					jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					jfc.setAcceptAllFileFilterUsed(false);
					int returnValue = jfc.showOpenDialog(null);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						app.setFileSigAnalysisCurrentDirectory(jfc.getSelectedFile());
						((FileTree) pnFileSystemView).reloadTree(app.getFileSigAnalysisCurrentDirectory());
						tpFileAnalysisOutput.setText("");
						btnSave.setEnabled(false);
					}
				}
			});
		}

		return btFileSignatureBrowse;
	}

	/**
	 * Gets the bt file signature save.
	 *
	 * @return the bt file signature save
	 */
	private JButton getBtFileSignatureSave() {
		if (btFileSignatureSave == null) {
			btFileSignatureSave = new JButton(Messages.getString("MainWindow.btnSave.text")); //$NON-NLS-1$
			btFileSignatureSave.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						app.exportFileSignatureAnalysisToTextFile();
						JOptionPane.showMessageDialog(getFrmFsForensics(),
								Messages.getString("MainWindow.FileSignatureAnalysisExportSuccess.text"),
								Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
						if (app.getCurrentFsCase().isLogCaseActivity()) {
							logger.info("File signature analysis saved to file in directory: "
									+ app.getCurrentFsCase().getCaseFolder());
						}
					} catch (PresentationException e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(getFrmFsForensics(),
								Messages.getString("MainWindow.FileSignatureAnalysisExportError.text"),
								Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
						logger.error(e1.getMessage());
					}
				}
			});
			btFileSignatureSave.setEnabled(false);
		}
		return btFileSignatureSave;
	}

	/**
	 * Gets the btn file signature analyse.
	 *
	 * @return the btn file signature analyse
	 */
	private JButton getBtnFileSignatureAnalyse() {
		if (btnFileSignatureAnalyse == null) {
			btnFileSignatureAnalyse = new JButton(Messages.getString("MainWindow.btnAnalyse.text")); //$NON-NLS-1$
			btnFileSignatureAnalyse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					lblLoadingFileSig.setVisible(true);
					tpFileAnalysisOutput.setText("");
					btFileSignatureSave.setEnabled(false);
					btnFileSignatureAnalyse.setEnabled(false);
					Thread t1 = new Thread(new Runnable() {
						public void run() {
							try {
								String report = app.analyzeCurrentDirectory(chckbxRecursiveSearch.isSelected(),
										chckbxExtractMetadata.isSelected(), chckbxShowDetailedOutput.isSelected());
								tpFileAnalysisOutput.setText(report);
								btFileSignatureSave.setEnabled(true);
								lblLoadingFileSig.setVisible(false);
								btnFileSignatureAnalyse.setEnabled(true);
								if (app.getCurrentFsCase().isLogCaseActivity()) {
									logger.info("File signature analysis performed:");
									logger.info(app.getCurrentFsReport());
								}
							} catch (Exception e1) {
								e1.printStackTrace();
								btFileSignatureSave.setEnabled(true);
								lblLoadingFileSig.setVisible(false);
								btnFileSignatureAnalyse.setEnabled(true);
								logger.error(e1.getMessage());
								JOptionPane.showMessageDialog(getFrmFsForensics(),
										Messages.getString("MainWindow.FileSignatureAnalysisError.text"),
										Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
							}
						}
					});
					while (true) {
						if (lblLoadingFileSig.isVisible()) {
							try {
								t1.start();
								break;
							} catch (IllegalThreadStateException e1) {
							}
						}
					}

				}
			});
		}
		return btnFileSignatureAnalyse;
	}

	/**
	 * Gets the lbl file signature analysis.
	 *
	 * @return the lbl file signature analysis
	 */
	private JLabel getLblFileSignatureAnalysis() {
		if (lblFileSignatureAnalysis == null) {
			lblFileSignatureAnalysis = new JLabel(Messages.getString("MainWindow.lblFileSignatureAnalysis.text")); //$NON-NLS-1$
			lblFileSignatureAnalysis.setFont(new Font("Tahoma", Font.BOLD, 15));
		}
		return lblFileSignatureAnalysis;
	}

	/**
	 * Gets the chckbx recursive search.
	 *
	 * @return the chckbx recursive search
	 */
	private JCheckBox getChckbxRecursiveSearch() {
		if (chckbxRecursiveSearch == null) {
			chckbxRecursiveSearch = new JCheckBox(Messages.getString("MainWindow.chckbxRecursiveSearch.text")); //$NON-NLS-1$
		}
		return chckbxRecursiveSearch;
	}

	/**
	 * Gets the split pane fs analysis.
	 *
	 * @return the split pane fs analysis
	 */
	private JSplitPane getSplitPaneFsAnalysis() {
		if (splitPaneFsAnalysis == null) {
			splitPaneFsAnalysis = new JSplitPane();
			splitPaneFsAnalysis.setResizeWeight(0.3);
			splitPaneFsAnalysis.setLeftComponent(getSpFileSystemView());
			splitPaneFsAnalysis.setRightComponent(getSpFileAnalysisOutput());
		}
		return splitPaneFsAnalysis;
	}

	/**
	 * Gets the sp file system view.
	 *
	 * @return the sp file system view
	 */
	private JScrollPane getSpFileSystemView() {
		if (spFileSystemView == null) {
			spFileSystemView = new JScrollPane();
			spFileSystemView.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			spFileSystemView.setBorder(new TitledBorder(null, Messages.getString("MainWindow.FileSystemViewTitle.text"),
					TitledBorder.LEADING, TitledBorder.TOP, null, null));
			spFileSystemView.setViewportView(getPnFileSystemView());
		}
		return spFileSystemView;
	}

	/**
	 * Gets the sp file analysis output.
	 *
	 * @return the sp file analysis output
	 */
	private JScrollPane getSpFileAnalysisOutput() {
		if (spFileAnalysisOutput == null) {
			spFileAnalysisOutput = new JScrollPane();
			spFileAnalysisOutput.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
					Messages.getString("MainWindow.AnalysisOutputTitle.text"), TitledBorder.LEADING, TitledBorder.TOP,
					null, new Color(0, 0, 0)));
			spFileAnalysisOutput.setViewportView(getTpFileAnalysisOutput());
		}
		return spFileAnalysisOutput;
	}

	/**
	 * Gets the tp file analysis output.
	 *
	 * @return the tp file analysis output
	 */
	private JTextPane getTpFileAnalysisOutput() {
		if (tpFileAnalysisOutput == null) {
			tpFileAnalysisOutput = new JTextPane();
			tpFileAnalysisOutput.setEditable(false);
			tpFileAnalysisOutput.setFont(new Font("Tahoma", Font.PLAIN, 11));
			tpFileAnalysisOutput.setContentType("text/html");
		}
		return tpFileAnalysisOutput;
	}

	/**
	 * Gets the pn file system view.
	 *
	 * @return the pn file system view
	 */
	private JPanel getPnFileSystemView() {
		if (pnFileSystemView == null) {
			pnFileSystemView = new FileTree(app.getFileSigAnalysisCurrentDirectory());
		}
		return pnFileSystemView;
	}

	/**
	 * Gets the lbl fs forensics icon.
	 *
	 * @return the lbl fs forensics icon
	 */
	private JLabel getLblFsForensicsIcon() {
		if (lblFsForensicsIcon == null) {
			lblFsForensicsIcon = new JLabel("");
		}
		return lblFsForensicsIcon;
	}

	/**
	 * Gets the lbl welcome to fs.
	 *
	 * @return the lbl welcome to fs
	 */
	private JLabel getLblWelcomeToFs() {
		if (lblWelcomeToFs == null) {
			lblWelcomeToFs = new JLabel(Messages.getString("MainWindow.lblWelcomeToFs.text")); //$NON-NLS-1$
			lblWelcomeToFs.setFont(new Font("Tahoma", Font.BOLD, 15));
		}
		return lblWelcomeToFs;
	}

	/**
	 * Gets the pn icon.
	 *
	 * @return the pn icon
	 */
	private JPanel getPnIcon() {
		if (pnIcon == null) {
			pnIcon = new JPanel();
			pnIcon.setLayout(new BorderLayout(0, 0));
			pnIcon.add(getLblFsForensicsIcon(), BorderLayout.CENTER);
		}
		return pnIcon;
	}

	/**
	 * Gets the pn quick acess menu.
	 *
	 * @return the pn quick acess menu
	 */
	private JPanel getPnQuickAcessMenu() {
		if (pnQuickAcessMenu == null) {
			pnQuickAcessMenu = new JPanel();
			pnQuickAcessMenu.setLayout(new GridLayout(3, 0, 0, 0));
			pnQuickAcessMenu.add(getBtnLoadCase());
			pnQuickAcessMenu.add(getBtnHelp());
			pnQuickAcessMenu.add(getBtnPreferences());
		}
		return pnQuickAcessMenu;
	}

	/**
	 * Gets the btn load case.
	 *
	 * @return the btn load case
	 */
	private JButton getBtnLoadCase() {
		if (btnLoadCase == null) {
			btnLoadCase = new JButton(Messages.getString("MainWindow.btnLoadCase.text")); //$NON-NLS-1$
			btnLoadCase.setHorizontalTextPosition(SwingConstants.RIGHT);
			btnLoadCase.setHorizontalAlignment(SwingConstants.LEFT);
			ImageIcon img = new ImageIcon(getClass().getResource("/ui/gui/img/openfolder_icon.png"));
			Image newImg = img.getImage().getScaledInstance(40, 45, Image.SCALE_SMOOTH);
			btnLoadCase.setIcon(new ImageIcon(newImg));
			btnLoadCase.setIconTextGap(15);
			btnLoadCase.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					launchCasesWindow();
				}
			});
		}
		return btnLoadCase;
	}

	/**
	 * Gets the btn help.
	 *
	 * @return the btn help
	 */
	private JButton getBtnHelp() {
		if (btnHelp == null) {
			btnHelp = new JButton(Messages.getString("MainWindow.btnHelp.text")); //$NON-NLS-1$
			btnHelp.setHorizontalAlignment(SwingConstants.LEFT);
			ImageIcon img = new ImageIcon(getClass().getResource("/ui/gui/img/help_icon.png"));
			Image newImg = img.getImage().getScaledInstance(45, 45, Image.SCALE_SMOOTH);
			btnHelp.setIcon(new ImageIcon(newImg));
			btnHelp.setIconTextGap(15);
		}
		return btnHelp;
	}

	/**
	 * Gets the btn preferences.
	 *
	 * @return the btn preferences
	 */
	private JButton getBtnPreferences() {
		if (btnPreferences == null) {
			btnPreferences = new JButton(Messages.getString("MainWindow.btnPreferences.text")); //$NON-NLS-1$
			btnPreferences.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					launchPreferencesDialog();
				}
			});
			btnPreferences.setHorizontalAlignment(SwingConstants.LEFT);
			ImageIcon img = new ImageIcon(getClass().getResource("/ui/gui/img/settings_icon.png"));
			Image newImg = img.getImage().getScaledInstance(60, 60, Image.SCALE_SMOOTH);
			btnPreferences.setIcon(new ImageIcon(newImg));
		}
		return btnPreferences;
	}

	/**
	 * Launch preferences dialog.
	 */
	private void launchPreferencesDialog() {
		PreferencesDialog dialog = new PreferencesDialog(app, getMainWindow());
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setModal(true);
		dialog.setVisible(true);
	}

	/**
	 * Load help.
	 */
	private void loadHelp() {

		URL hsURL;
		HelpSet hs;

		try {
			File file = new File("help/help_set.hs");
			hsURL = file.toURI().toURL();
			hs = new HelpSet(null, hsURL);
			HelpBroker hb = hs.createHelpBroker();
			hb.initPresentation();

			hb.enableHelpKey(getFrmFsForensics(), "intro", hs);
			hb.enableHelpOnButton(mntmContents, "intro", hs);
			hb.enableHelpOnButton(btnHelp, "intro", hs);
		} catch (IOException | HelpSetException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Gets the lbl description.
	 *
	 * @return the lbl description
	 */
	private JLabel getLblDescription() {
		if (lblDescription == null) {
			lblDescription = new JLabel(Messages.getString("MainWindow.lblDescription.text")); //$NON-NLS-1$
		}
		return lblDescription;
	}

	/**
	 * Gets the tx description.
	 *
	 * @return the tx description
	 */
	private JTextField getTxDescription() {
		if (txDescription == null) {
			txDescription = new JTextField();
			txDescription.setColumns(10);
			txDescription.addKeyListener(caseUpdateListener);
		}
		return txDescription;
	}

	/**
	 * Gets the chckbx extract metadata.
	 *
	 * @return the chckbx extract metadata
	 */
	private JCheckBox getChckbxExtractMetadata() {
		if (chckbxExtractMetadata == null) {
			chckbxExtractMetadata = new JCheckBox(Messages.getString("MainWindow.chckbxExtractMetadata.text")); //$NON-NLS-1$
		}
		return chckbxExtractMetadata;
	}

	/**
	 * Gets the chckbx show detailed output.
	 *
	 * @return the chckbx show detailed output
	 */
	private JCheckBox getChckbxShowDetailedOutput() {
		if (chckbxShowDetailedOutput == null) {
			chckbxShowDetailedOutput = new JCheckBox(Messages.getString("MainWindow.chckbxShowDetailedOutput.text")); //$NON-NLS-1$
		}
		return chckbxShowDetailedOutput;
	}

	/**
	 * Gets the pn RDS query tool.
	 *
	 * @return the pn RDS query tool
	 */
	private JPanel getPnRDSQueryTool() {
		if (pnRDSQueryTool == null) {
			pnRDSQueryTool = new JPanel();
			GroupLayout gl_pnRDSQueryTool = new GroupLayout(pnRDSQueryTool);
			gl_pnRDSQueryTool.setHorizontalGroup(gl_pnRDSQueryTool.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_pnRDSQueryTool.createSequentialGroup().addGroup(gl_pnRDSQueryTool
							.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnRDSQueryTool.createSequentialGroup().addContainerGap().addComponent(
									getSplitPaneRDSExplorer(), GroupLayout.DEFAULT_SIZE, 695, Short.MAX_VALUE))
							.addGroup(gl_pnRDSQueryTool.createSequentialGroup().addGap(19)
									.addGroup(gl_pnRDSQueryTool.createParallelGroup(Alignment.LEADING)
											.addGroup(gl_pnRDSQueryTool.createSequentialGroup()
													.addComponent(getLblSelectRDSDirectory(),
															GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.UNRELATED)
													.addComponent(getBtnRDSBrowse(), GroupLayout.PREFERRED_SIZE, 86,
															GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.UNRELATED)
													.addComponent(getBtnRDSSave(), GroupLayout.PREFERRED_SIZE, 87,
															GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED, 267, Short.MAX_VALUE)
													.addComponent(getBtnRDSAnalyse(), GroupLayout.PREFERRED_SIZE, 95,
															GroupLayout.PREFERRED_SIZE))
											.addGroup(gl_pnRDSQueryTool.createSequentialGroup()
													.addComponent(getLabel(), GroupLayout.PREFERRED_SIZE, 273,
															GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED,
															GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
													.addComponent(getLblLoading()).addGap(17)))))
							.addContainerGap()));
			gl_pnRDSQueryTool.setVerticalGroup(gl_pnRDSQueryTool.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_pnRDSQueryTool.createSequentialGroup().addGap(30)
							.addComponent(getLabel(), GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_pnRDSQueryTool.createParallelGroup(Alignment.LEADING)
									.addComponent(getLblLoading())
									.addGroup(gl_pnRDSQueryTool.createSequentialGroup().addGap(31)
											.addGroup(gl_pnRDSQueryTool.createParallelGroup(Alignment.BASELINE)
													.addComponent(getLblSelectRDSDirectory())
													.addComponent(getBtnRDSBrowse()).addComponent(getBtnRDSSave())
													.addComponent(getBtnRDSAnalyse()))))
							.addGap(21)
							.addComponent(getSplitPaneRDSExplorer(), GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
							.addContainerGap()));
			pnRDSQueryTool.setLayout(gl_pnRDSQueryTool);
		}
		return pnRDSQueryTool;
	}

	/**
	 * Gets the split pane RDS explorer.
	 *
	 * @return the split pane RDS explorer
	 */
	private JSplitPane getSplitPaneRDSExplorer() {
		if (splitPaneRDSExplorer == null) {
			splitPaneRDSExplorer = new JSplitPane();
			splitPaneRDSExplorer.setResizeWeight(0.3);
			splitPaneRDSExplorer.setLeftComponent(getSpRDSFileSystemView());
			splitPaneRDSExplorer.setRightComponent(getSpRDSAnalysisOutput());
		}
		return splitPaneRDSExplorer;
	}

	/**
	 * Gets the sp RDS file system view.
	 *
	 * @return the sp RDS file system view
	 */
	private JScrollPane getSpRDSFileSystemView() {
		if (spRDSFileSystemView == null) {
			spRDSFileSystemView = new JScrollPane();
			spRDSFileSystemView.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			spRDSFileSystemView
					.setBorder(new TitledBorder(null, Messages.getString("MainWindow.FileSystemViewTitle.text"),
							TitledBorder.LEADING, TitledBorder.TOP, null, null));
			spRDSFileSystemView.setViewportView(getPnRDSFileSystemView());
		}
		return spRDSFileSystemView;
	}

	/**
	 * Gets the sp RDS analysis output.
	 *
	 * @return the sp RDS analysis output
	 */
	private JScrollPane getSpRDSAnalysisOutput() {
		if (spRDSAnalysisOutput == null) {
			spRDSAnalysisOutput = new JScrollPane();
			spRDSAnalysisOutput.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
					Messages.getString("MainWindow.AnalysisOutputTitle.text"), TitledBorder.LEADING, TitledBorder.TOP,
					null, new Color(0, 0, 0)));
			spRDSAnalysisOutput.setViewportView(getTpRDSQueryTool());
		}
		return spRDSAnalysisOutput;
	}

	/**
	 * Gets the lbl select RDS directory.
	 *
	 * @return the lbl select RDS directory
	 */
	private JLabel getLblSelectRDSDirectory() {
		if (lblSelectRDSDirectory == null) {
			lblSelectRDSDirectory = new JLabel(Messages.getString("MainWindow.lblSelectDirectory.text")); //$NON-NLS-1$
		}
		return lblSelectRDSDirectory;
	}

	/**
	 * Gets the btn RDS browse.
	 *
	 * @return the btn RDS browse
	 */
	private JButton getBtnRDSBrowse() {
		if (btnRDSBrowse == null) {
			btnRDSBrowse = new JButton(Messages.getString("MainWindow.btnBrowse.text")); //$NON-NLS-1$
			btnRDSBrowse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
					jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					jfc.setAcceptAllFileFilterUsed(false);
					int returnValue = jfc.showOpenDialog(null);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						app.setRdsQueryToolCurrentDirectory(jfc.getSelectedFile());
						((FileTree) pnRDSFileSystemView).reloadTree(app.getRdsQueryToolCurrentDirectory());
						tpRDSQueryTool.setText("");
						btnRDSSave.setEnabled(false);
					}
				}
			});
		}
		return btnRDSBrowse;
	}

	/**
	 * Gets the btn RDS save.
	 *
	 * @return the btn RDS save
	 */
	private JButton getBtnRDSSave() {
		if (btnRDSSave == null) {
			btnRDSSave = new JButton(Messages.getString("MainWindow.btnSave.text")); //$NON-NLS-1$
			btnRDSSave.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						app.exportRDSQueryToTextFile();
						JOptionPane.showMessageDialog(getFrmFsForensics(),
								Messages.getString("MainWindow.RDSQueryToolExportSuccess.text"),
								Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
					} catch (PresentationException e1) {
						e1.printStackTrace();
						logger.error(e1.getMessage());
						JOptionPane.showMessageDialog(getFrmFsForensics(),
								Messages.getString("MainWindow.RDSQueryToolExportError.text"),
								Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
						logger.error(e1.getMessage());

					}
				}
			});
			btnRDSSave.setEnabled(false);
		}
		return btnRDSSave;
	}

	/**
	 * Gets the btn RDS analyse.
	 *
	 * @return the btn RDS analyse
	 */
	private JButton getBtnRDSAnalyse() {
		if (btnRDSAnalyse == null) {
			btnRDSAnalyse = new JButton(Messages.getString("MainWindow.btnAnalyse.text")); //$NON-NLS-1$
			btnRDSAnalyse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					lblLoading.setVisible(true);
					tpRDSQueryTool.setText("");
					btnRDSAnalyse.setEnabled(false);
					btnRDSBrowse.setEnabled(false);
					btnRDSSave.setEnabled(false);
					Thread t2 = new Thread(new Runnable() {
						public void run() {
							try {
								tpRDSQueryTool.setText(getApplication().rdsQueryToCurrentDirectory());
								lblLoading.setVisible(false);
								btnRDSAnalyse.setEnabled(true);
								btnRDSBrowse.setEnabled(true);
								if (app.getCurrentFsCase().isLogCaseActivity()) {
									logger.info("RDS Tool executed succesfully.");
									logger.info(app.getRdsQueryTool().getReport(app.getRdsQueryToolCurrentDirectory()));
								}
								btnRDSSave.setEnabled(true);
							} catch (Exception e) {
								e.printStackTrace();
								lblLoading.setVisible(false);
								btnRDSAnalyse.setEnabled(true);
								btnRDSBrowse.setEnabled(true);
								logger.error(e.getMessage());
								JOptionPane.showMessageDialog(getFrmFsForensics(),
										Messages.getString("MainWindow.RDSQueryToolError.text"),
										Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
							}
						}
					});
					while (true) {
						if (lblLoading.isVisible()) {
							try {
								Thread.sleep(100);
								t2.start();
								break;
							} catch (IllegalThreadStateException | InterruptedException e1) {
							}

						}
					}

				}
			});

		}
		return btnRDSAnalyse;
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel(Messages.getString("RDSQueryPanel.RDSQueryTitle")); //$NON-NLS-1$
			label.setFont(new Font("Tahoma", Font.BOLD, 15));
		}
		return label;
	}

	/**
	 * Gets the pn RDS file system view.
	 *
	 * @return the pn RDS file system view
	 */
	private FileTree getPnRDSFileSystemView() {
		if (pnRDSFileSystemView == null) {
			pnRDSFileSystemView = new FileTree(app.getRdsQueryToolCurrentDirectory());
		}
		return pnRDSFileSystemView;
	}

	/**
	 * Gets the btn rds query tool.
	 *
	 * @return the btn rds query tool
	 */
	private JButton getBtnRdsQueryTool() {
		if (btnRdsQueryTool == null) {
			btnRdsQueryTool = new JButton(Messages.getString("MainWindow.btnRdsQueryTool.text")); //$NON-NLS-1$
			btnRdsQueryTool.setHorizontalAlignment(SwingConstants.LEFT);
			btnRdsQueryTool.setEnabled(false);
			ImageIcon img = new ImageIcon(getClass().getResource("/ui/gui/img/rds_analyzer_icon.png"));
			Image newImg = img.getImage().getScaledInstance(35, 35, Image.SCALE_SMOOTH);
			btnRdsQueryTool.setIcon(new ImageIcon(newImg));
			btnRdsQueryTool.setIconTextGap(15);
			btnRdsQueryTool.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					showPanels(false, false, false, false, true);
				}
			});
		}
		return btnRdsQueryTool;
	}

	/**
	 * Gets the tp RDS query tool.
	 *
	 * @return the tp RDS query tool
	 */
	private JTextPane getTpRDSQueryTool() {
		if (tpRDSQueryTool == null) {
			tpRDSQueryTool = new JTextPane();
			tpRDSQueryTool.setFont(new Font("Tahoma", Font.PLAIN, 11));
			tpRDSQueryTool.setEditable(false);
			tpRDSQueryTool.setContentType("text/html");
		}
		return tpRDSQueryTool;
	}

	/**
	 * Gets the lbl loading.
	 *
	 * @return the lbl loading
	 */
	private JLabel getLblLoading() {
		if (lblLoading == null) {
			lblLoading = new JLabel(Messages.getString("MainWindow.lblLoading.text"), loadingIcon, JLabel.CENTER); //$NON-NLS-1$
			lblLoading.setVisible(false);
		}
		return lblLoading;
	}

	/**
	 * Gets the lbl loading file sig.
	 *
	 * @return the lbl loading file sig
	 */
	private JLabel getLblLoadingFileSig() {
		if (lblLoadingFileSig == null) {
			lblLoadingFileSig = new JLabel(Messages.getString("MainWindow.lblLoading.text"), loadingIcon, //$NON-NLS-1$
					SwingConstants.CENTER);
			lblLoadingFileSig.setVisible(false);
		}
		return lblLoadingFileSig;
	}

	/**
	 * Gets the application.
	 *
	 * @return the application
	 */
	private Application getApplication() {
		return app;
	}
}
