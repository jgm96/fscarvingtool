package ui.gui.window;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.toedter.calendar.JDateChooser;

import logic.Application;
import model.FsCase;
import model.exception.PresentationException;
import ui.gui.Messages;
import util.StringUtil;


/**
 * The Class LoadCasesDialog.
 */
public class LoadCasesDialog extends JDialog {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The content panel. */
	private final JPanel contentPanel = new JPanel();	
	
	/** The main window. */
	private MainWindow mainWindow;
	
	/** The app. */
	private Application app;	
	
	/** The filtered cases. */
	private List<FsCase> filteredCases;
	
	/** The ml cases. */
	private DefaultListModel<String> mlCases;
	
	/** The list cases. */
	private JList<String> listCases;	
	
	/** The pn buttons. */
	private JPanel pnButtons;
	
	/** The btn open. */
	private JButton btnOpen;
	
	/** The btn close. */
	private JButton btnClose;
	
	/** The btn new. */
	private JButton btnNew;
	
	/** The btn delete. */
	private JButton btnDelete;
	
	/** The lbl my cases. */
	private JLabel lblMyCases;
	
	/** The main panel. */
	private JPanel mainPanel;
	
	/** The pn left. */
	private JPanel pnLeft;
	
	/** The pn right. */
	private JPanel pnRight;
	
	/** The scroll pane. */
	private JScrollPane scrollPane;
	
	/** The load cases dialog. */
	private LoadCasesDialog loadCasesDialog;
	
	/** The lbl case name. */
	private JLabel lblCaseName;
	
	/** The tx case name. */
	private JTextField txCaseName;
	
	/** The lbl investigator. */
	private JLabel lblInvestigator;
	
	/** The tx investigator. */
	private JTextField txInvestigator;
	
	/** The lbl organization. */
	private JLabel lblOrganization;
	
	/** The tx organization. */
	private JTextField txOrganization;
	
	/** The lbl description. */
	private JLabel lblDescription;
	
	/** The tx description. */
	private JTextField txDescription;
	
	/** The lbl min date. */
	private JLabel lblMinDate;
	
	/** The lbl max date. */
	private JLabel lblMaxDate;
	
	/** The lbl logic mask. */
	private JLabel lblLogicMask;
	
	/** The tx min date. */
	private JDateChooser txMinDate;
	
	/** The tx maxdate. */
	private JDateChooser txMaxdate;
	
	/** The cb logic mask. */
	private JComboBox<String> cbLogicMask;
	
	/** The btn search. */
	private JButton btnSearch;

	/**
	 * Create the dialog.
	 *
	 * @param app the app
	 * @param mainWindow the main window
	 */
	public LoadCasesDialog(Application app, MainWindow mainWindow) {		
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(LoadCasesDialog.class.getResource("/ui/gui/img/fingerprint_search.png"))); //$NON-NLS-1$
		this.app = app;
		this.mainWindow = mainWindow;
		setModal(true);				
		setTitle(Messages.getString("LoadCasesWindow.Title.text")); //$NON-NLS-1$
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(LoadCasesDialog.class.getResource("/ui/gui/img/fingerprint_search.png"))); //$NON-NLS-1$
		setBounds(100, 100, 623, 552);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JPanel pnCentral = new JPanel();
			contentPanel.add(pnCentral, BorderLayout.CENTER);
			GroupLayout gl_pnCentral = new GroupLayout(pnCentral);
			gl_pnCentral.setHorizontalGroup(
				gl_pnCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnCentral.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.LEADING)
							.addComponent(getMainPanel(), GroupLayout.PREFERRED_SIZE, 556, Short.MAX_VALUE)
							.addComponent(getLblMyCases(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE))
						.addContainerGap())
			);
			gl_pnCentral.setVerticalGroup(
				gl_pnCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnCentral.createSequentialGroup()
						.addGap(20)
						.addComponent(getLblMyCases(), GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(getMainPanel(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap())
			);
			pnCentral.setLayout(gl_pnCentral);
		}
		contentPanel.add(getPnButtons(), BorderLayout.SOUTH);
		initialize();
		loadCasesDialog = this;
		this.setLocationRelativeTo(mainWindow.getFrmFsForensics());
	}
	
	/**
	 * Gets the load cases dialog.
	 *
	 * @return the load cases dialog
	 */
	private LoadCasesDialog getLoadCasesDialog() {
		return loadCasesDialog;
	}
	
	/**
	 * Initialize.
	 */
	private void initialize() {
		filteredCases = app.getFsCaseManager().getFsCases();
		resetMlList();
	}

	/**
	 * Gets the pn buttons.
	 *
	 * @return the pn buttons
	 */
	private JPanel getPnButtons() {
		if (pnButtons == null) {
			pnButtons = new JPanel();
			GroupLayout gl_pnButtons = new GroupLayout(pnButtons);
			gl_pnButtons.setHorizontalGroup(
				gl_pnButtons.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnButtons.createSequentialGroup()
						.addGap(5)
						.addComponent(getBtnOpen())
						.addGap(5)
						.addComponent(getBtnDelete())
						.addGap(5)
						.addComponent(getBtnNew())
						.addPreferredGap(ComponentPlacement.RELATED, 298, Short.MAX_VALUE)
						.addComponent(getBtnClose()))
			);
			gl_pnButtons.setVerticalGroup(
				gl_pnButtons.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnButtons.createSequentialGroup()
						.addGap(5)
						.addGroup(gl_pnButtons.createParallelGroup(Alignment.LEADING)
							.addComponent(getBtnOpen())
							.addComponent(getBtnDelete())
							.addGroup(gl_pnButtons.createParallelGroup(Alignment.BASELINE)
								.addComponent(getBtnNew())
								.addComponent(getBtnClose()))))
			);
			pnButtons.setLayout(gl_pnButtons);
		}
		return pnButtons;
	}
	
	/**
	 * Gets the btn open.
	 *
	 * @return the btn open
	 */
	private JButton getBtnOpen() {
		if (btnOpen == null) {
			btnOpen = new JButton(Messages.getString("LoadCasesDialog.btnOpen.text")); //$NON-NLS-1$
			btnOpen.setEnabled(false);
			btnOpen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					app.setCurrentFsCase(filteredCases.get(listCases.getSelectedIndex()));
					mainWindow.enableCaseButtons(true);
					mainWindow.showPanels(false, true, false, false, false);
					mainWindow.fillManageCasesPanel();
					dispose();
				}
			});
		}
		return btnOpen;
	}
	
	/**
	 * Gets the btn close.
	 *
	 * @return the btn close
	 */
	private JButton getBtnClose() {
		if (btnClose == null) {
			btnClose = new JButton(Messages.getString("LoadCasesDialog.btnClose.text")); //$NON-NLS-1$
			btnClose.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			btnClose.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return btnClose;
	}
	
	/**
	 * Gets the btn new.
	 *
	 * @return the btn new
	 */
	private JButton getBtnNew() {
		if (btnNew == null) {
			btnNew = new JButton(Messages.getString("LoadCasesDialog.btnNew.text")); //$NON-NLS-1$
			btnNew.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					NewFsCaseDialog dialog = new NewFsCaseDialog(app, mainWindow, loadCasesDialog);
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				}
			});
		}
		return btnNew;
	}
	
	/**
	 * Gets the btn delete.
	 *
	 * @return the btn delete
	 */
	private JButton getBtnDelete() {
		if (btnDelete == null) {
			btnDelete = new JButton(Messages.getString("LoadCasesDialog.btnDelete.text")); //$NON-NLS-1$
			btnDelete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int reply = JOptionPane.showConfirmDialog(getLoadCasesDialog(),
							Messages.getString("LoadCasesWindow.MessageConfirmDeletion.text"),
							Messages.getString("Shared.Cofirmation.text"), JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);
					if (reply == JOptionPane.YES_OPTION) {
						try {
							app.getFsCaseManager().deleteFsCase(
									app.getFsCaseManager().getFsCases().get(listCases.getSelectedIndex()));
							resetMlList();
							mainWindow.enableCaseButtons(false);
							mainWindow.showPanels(true, false, false, false, false);

						} catch (PresentationException e1) {
							e1.printStackTrace();
						}
					}
				}
			});
			btnDelete.setEnabled(false);
		}
		return btnDelete;
	}
	
	/**
	 * Gets the lbl my cases.
	 *
	 * @return the lbl my cases
	 */
	private JLabel getLblMyCases() {
		if (lblMyCases == null) {
			lblMyCases = new JLabel(Messages.getString("LoadCasesDialog.lblMyCases.text")); //$NON-NLS-1$
			lblMyCases.setFont(new Font("Tahoma", Font.BOLD, 15));
		}
		return lblMyCases;
	}
	
	/**
	 * Gets the main panel.
	 *
	 * @return the main panel
	 */
	private JPanel getMainPanel() {
		if (mainPanel == null) {
			mainPanel = new JPanel();
			mainPanel.setLayout(new GridLayout(1, 0, 0, 0));
			mainPanel.add(getPnLeft());
			mainPanel.add(getPnRight());
		}
		return mainPanel;
	}
	
	/**
	 * Gets the pn left.
	 *
	 * @return the pn left
	 */
	private JPanel getPnLeft() {
		if (pnLeft == null) {
			pnLeft = new JPanel();
			pnLeft.setBorder(new TitledBorder(null, Messages.getString("LoadCasesDialog.CasesTitle.text"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnLeft.setLayout(new BorderLayout(0, 0));
			pnLeft.add(getScrollPane());
		}
		return pnLeft;
	}
	
	/**
	 * Gets the pn right.
	 *
	 * @return the pn right
	 */
	private JPanel getPnRight() {
		if (pnRight == null) {
			pnRight = new JPanel();
			pnRight.setBorder(new TitledBorder(null, Messages.getString("LoadCasesDialog.FilterCasesTitle.text"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_pnRight = new GroupLayout(pnRight);
			gl_pnRight.setHorizontalGroup(
				gl_pnRight.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnRight.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_pnRight.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnRight.createParallelGroup(Alignment.TRAILING)
								.addComponent(getLblDescription())
								.addComponent(getLblLogicMask())
								.addGroup(gl_pnRight.createParallelGroup(Alignment.LEADING)
									.addComponent(getLblMaxDate())
									.addComponent(getLblMinDate())))
							.addComponent(getLblCaseName(), GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblInvestigator())
							.addComponent(getLblOrganization(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_pnRight.createParallelGroup(Alignment.LEADING)
							.addGroup(Alignment.TRAILING, gl_pnRight.createSequentialGroup()
								.addGroup(gl_pnRight.createParallelGroup(Alignment.LEADING)
									.addComponent(getTxDescription(), GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
									.addComponent(getTxMinDate(), Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
									.addComponent(getTxMaxdate(), GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
									.addGroup(gl_pnRight.createSequentialGroup()
										.addComponent(getCbLogicMask(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(getBtnSearch(), GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)))
								.addContainerGap())
							.addGroup(Alignment.TRAILING, gl_pnRight.createSequentialGroup()
								.addGroup(gl_pnRight.createParallelGroup(Alignment.TRAILING)
									.addComponent(getTxInvestigator(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
									.addComponent(getTxCaseName(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
									.addComponent(getTxOrganization(), GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE))
								.addContainerGap())))
			);
			gl_pnRight.setVerticalGroup(
				gl_pnRight.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnRight.createSequentialGroup()
						.addGap(16)
						.addGroup(gl_pnRight.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblCaseName())
							.addComponent(getTxCaseName(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_pnRight.createParallelGroup(Alignment.BASELINE)
							.addComponent(getTxInvestigator(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblInvestigator()))
						.addGap(17)
						.addGroup(gl_pnRight.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblOrganization())
							.addComponent(getTxOrganization(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_pnRight.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblDescription())
							.addComponent(getTxDescription(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_pnRight.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblMinDate())
							.addComponent(getTxMinDate(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_pnRight.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblMaxDate())
							.addComponent(getTxMaxdate(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(27)
						.addGroup(gl_pnRight.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblLogicMask())
							.addComponent(getCbLogicMask(), GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
							.addComponent(getBtnSearch()))
						.addContainerGap(110, Short.MAX_VALUE))
			);
			pnRight.setLayout(gl_pnRight);
		}
		return pnRight;
	}
	
	/**
	 * Gets the scroll pane.
	 *
	 * @return the scroll pane
	 */
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			mlCases = new DefaultListModel<String>();
			listCases = new JList<String>(mlCases);				
			listCases.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					if (0 <= listCases.getSelectedIndex()
							&& listCases.getSelectedIndex() < filteredCases.size()) {
						btnOpen.setEnabled(true);
						btnDelete.setEnabled(true);
					} else {
						btnOpen.setEnabled(false);
						btnDelete.setEnabled(false);
					}
				}
			});			
			scrollPane.setViewportView(listCases);	
		}
		return scrollPane;
	}	
	
	/**
	 * Filter cases.
	 */
	private void filterCases() {
		filteredCases = app.getFsCaseManager().filterCases(txCaseName.getText(), txInvestigator.getText(), txOrganization.getText(), 
				txDescription.getText(), txMinDate.getDate(), txMaxdate.getDate(), cbLogicMask.getSelectedIndex());
	}
	
	/**
	 * Reset ml list.
	 */
	private void resetMlList() {
		mlCases.clear();
		for (FsCase fsCase : filteredCases) {
			mlCases.addElement(fsCase.getname() + " - " + StringUtil.convertDateToFancyString(fsCase.getCaseDate())); //$NON-NLS-1$
		}
	}
	
	/**
	 * Gets the lbl case name.
	 *
	 * @return the lbl case name
	 */
	private JLabel getLblCaseName() {
		if (lblCaseName == null) {
			lblCaseName = new JLabel(Messages.getString("LoadCasesDialog.lblCaseName")); //$NON-NLS-1$
		}
		return lblCaseName;
	}
	
	/**
	 * Gets the tx case name.
	 *
	 * @return the tx case name
	 */
	private JTextField getTxCaseName() {
		if (txCaseName == null) {
			txCaseName = new JTextField();
			txCaseName.setColumns(10);
		}
		return txCaseName;
	}
	
	/**
	 * Gets the lbl investigator.
	 *
	 * @return the lbl investigator
	 */
	private JLabel getLblInvestigator() {
		if (lblInvestigator == null) {
			lblInvestigator = new JLabel(Messages.getString("LoadCasesDialog.lblInvestigator")); //$NON-NLS-1$
		}
		return lblInvestigator;
	}
	
	/**
	 * Gets the tx investigator.
	 *
	 * @return the tx investigator
	 */
	private JTextField getTxInvestigator() {
		if (txInvestigator == null) {
			txInvestigator = new JTextField();
			txInvestigator.setColumns(10);
		}
		return txInvestigator;
	}
	
	/**
	 * Gets the lbl organization.
	 *
	 * @return the lbl organization
	 */
	private JLabel getLblOrganization() {
		if (lblOrganization == null) {
			lblOrganization = new JLabel(Messages.getString("LoadCasesDialog.lblOrganization")); //$NON-NLS-1$
		}
		return lblOrganization;
	}
	
	/**
	 * Gets the tx organization.
	 *
	 * @return the tx organization
	 */
	private JTextField getTxOrganization() {
		if (txOrganization == null) {
			txOrganization = new JTextField();
			txOrganization.setColumns(10);
		}
		return txOrganization;
	}
	
	/**
	 * Gets the lbl description.
	 *
	 * @return the lbl description
	 */
	private JLabel getLblDescription() {
		if (lblDescription == null) {
			lblDescription = new JLabel(Messages.getString("LoadCasesDialog.lblDescription.text")); //$NON-NLS-1$
		}
		return lblDescription;
	}
	
	/**
	 * Gets the tx description.
	 *
	 * @return the tx description
	 */
	private JTextField getTxDescription() {
		if (txDescription == null) {
			txDescription = new JTextField();
			txDescription.setColumns(10);
		}
		return txDescription;
	}
	
	/**
	 * Gets the lbl min date.
	 *
	 * @return the lbl min date
	 */
	private JLabel getLblMinDate() {
		if (lblMinDate == null) {
			lblMinDate = new JLabel(Messages.getString("LoadCasesDialog.lblMinDate.text")); //$NON-NLS-1$
		}
		return lblMinDate;
	}
	
	/**
	 * Gets the lbl max date.
	 *
	 * @return the lbl max date
	 */
	private JLabel getLblMaxDate() {
		if (lblMaxDate == null) {
			lblMaxDate = new JLabel(Messages.getString("LoadCasesDialog.lblMaxDate.text")); //$NON-NLS-1$
		}
		return lblMaxDate;
	}
	
	/**
	 * Gets the lbl logic mask.
	 *
	 * @return the lbl logic mask
	 */
	private JLabel getLblLogicMask() {
		if (lblLogicMask == null) {
			lblLogicMask = new JLabel(Messages.getString("LoadCasesDialog.lblLogicMask.text")); //$NON-NLS-1$
		}
		return lblLogicMask;
	}
	
	/**
	 * Gets the tx min date.
	 *
	 * @return the tx min date
	 */
	private JDateChooser getTxMinDate() {
		if (txMinDate == null) {
			txMinDate = new JDateChooser();			
		}
		return txMinDate;
	}
	
	/**
	 * Gets the tx maxdate.
	 *
	 * @return the tx maxdate
	 */
	private JDateChooser getTxMaxdate() {
		if (txMaxdate == null) {
			txMaxdate = new JDateChooser();			
		}
		return txMaxdate;
	}
	
	/**
	 * Gets the cb logic mask.
	 *
	 * @return the cb logic mask
	 */
	private JComboBox<String> getCbLogicMask() {
		if (cbLogicMask == null) {
			cbLogicMask = new JComboBox<String>();
			cbLogicMask.setModel(new DefaultComboBoxModel<String>(new String[] {"And", "Or"}));
		}
		return cbLogicMask;
	}
	
	/**
	 * Gets the btn search.
	 *
	 * @return the btn search
	 */
	private JButton getBtnSearch() {
		if (btnSearch == null) {
			btnSearch = new JButton(Messages.getString("LoadCasesDialog.btnSearch.text")); //$NON-NLS-1$
			btnSearch.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					filterCases();
					resetMlList();
					checkOpenButtonEnabled();
				}
			});
		}
		return btnSearch;
	}
	
	/**
	 * Check open button enabled.
	 */
	private void checkOpenButtonEnabled() {
		if(listCases.getSelectedIndex() >= 0) {
			btnOpen.setEnabled(true);
		}
	}
}
