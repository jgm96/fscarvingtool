package ui.gui.window;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import logic.Application;
import model.FsExtension;
import model.exception.PresentationException;
import model.factory.FsExtensionsFactory;
import ui.gui.Messages;
import util.HexUtil;


/**
 * The Class NewFsExtensionDialog.
 */
public class NewFsExtensionDialog extends JDialog {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The pn central. */
	private JPanel pnCentral;
	
	/** The lbl extension. */
	private JLabel lblExtension;
	
	/** The lbl description. */
	private JLabel lblDescription;
	
	/** The lbl file signature. */
	private JLabel lblFileSignature;
	
	/** The lbl offset. */
	private JLabel lblOffset;
	
	/** The tx extension. */
	private JTextField txExtension;
	
	/** The tx description. */
	private JTextField txDescription;
	
	/** The tx file signature. */
	private JTextField txFileSignature;
	
	/** The tx offset. */
	private JTextField txOffset;
	
	/** The lbl trailer. */
	private JLabel lblTrailer;
	
	/** The tx trailer. */
	private JTextField txTrailer;
	
	/** The lbl trailer offset. */
	private JLabel lblTrailerOffset;
	
	/** The tx trailer offset. */
	private JTextField txTrailerOffset;
	
	/** The ok button. */
	private JButton okButton;	
	
	/** The chars. */
	private final String chars = "ABCDEFabcdef0123456789";	

	/** The creation listener. */
	private KeyListener creationListener = new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent e) {
			checkOkButtonEnabled();
		}
	};

	/** The not digit consumer. */
	private KeyListener notDigitConsumer = new KeyAdapter() {
		@Override
		public void keyTyped(KeyEvent e) {
			char ch = e.getKeyChar();
			if (!Character.isDigit(ch)) {
				e.consume();
			}
		}
	};
	
	/** The not hexadecimal consumer. */
	private KeyListener notHexadecimalConsumer = new KeyAdapter() {
		@Override
		public void keyTyped(KeyEvent e) {
			char ch = e.getKeyChar();
			if (chars.indexOf(ch) == -1) {
				e.consume();
			}
		}
	};

	/** The app. */
	@SuppressWarnings("unused")
	private Application app;
	
	/** The lbl category. */
	private JLabel lblCategory;
	
	/** The tx category. */
	private JTextField txCategory;

	/**
	 * Create the dialog.
	 *
	 * @param app the app
	 * @param mainWindow the main window
	 * @param extensionsWindow the extensions window
	 */
	public NewFsExtensionDialog(Application app, MainWindow mainWindow, ExtensionsWindow extensionsWindow) {
		setModal(true);
		this.app = app;
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(NewFsExtensionDialog.class.getResource("/ui/gui/img/fingerprint_search.png")));
		setTitle(Messages.getString("NewFsExtensionWindow.Title")); //$NON-NLS-1$
		setBounds(100, 100, 579, 340);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(getPnCentral(), BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("Ok");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							FsExtension oldExtension = app.getFsExtensionManager().getFsExtensionByNameAndFileSignature(
									txExtension.getText(), txFileSignature.getText());
							if (oldExtension != null) {
								int reply = JOptionPane.showConfirmDialog(getNewFsExtensionDialog(),
										Messages.getString("NewFsExtensionWindow.UpdateQuestion.text"),
										Messages.getString("Shared.Cofirmation.text"), JOptionPane.YES_NO_OPTION,
										JOptionPane.QUESTION_MESSAGE);
								if (reply == JOptionPane.YES_OPTION) {
									FsExtension newExtension = FsExtensionsFactory.getInstance().createFsExtension(
											txExtension.getText(), txDescription.getText(),
											txFileSignature.getText(), txOffset.getText(), txTrailer.getText(),
											txTrailerOffset.getText(), txCategory.getText());
									newExtension.setId(oldExtension.getId());
									app.getFsExtensionManager().updateFsExtension(oldExtension, newExtension);
								}								
							} else {
								app.getFsExtensionManager()
										.newFsExtension(FsExtensionsFactory.getInstance().createFsExtension(
												txExtension.getText(), txDescription.getText(),
												txFileSignature.getText(), txOffset.getText(), txTrailer.getText(),
												txTrailerOffset.getText(), txCategory.getText()));
								JOptionPane.showMessageDialog(getNewFsExtensionDialog(),
										Messages.getString("NewFsExtensionWindow.MessageSuccesfulCreation.text"),
										Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
							}
						} catch (PresentationException e1) {
							JOptionPane.showMessageDialog(getNewFsExtensionDialog(),
									Messages.getString("NewFsExtensionWindow.MessageExceptionCreation.text"),
									Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
							e1.printStackTrace();
						}

						dispose();
					}
				});
				okButton.setActionCommand("OK");
				okButton.setEnabled(false);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton(Messages.getString("Shared.Cancel")); //$NON-NLS-1$
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		this.setLocationRelativeTo(mainWindow.getFrmFsForensics());
		this.pack();
	}
	
	/**
	 * Gets the new fs extension dialog.
	 *
	 * @return the new fs extension dialog
	 */
	private NewFsExtensionDialog getNewFsExtensionDialog() {
		return this;
	}

	/**
	 * Gets the pn central.
	 *
	 * @return the pn central
	 */
	private JPanel getPnCentral() {
		if (pnCentral == null) {
			pnCentral = new JPanel();
			pnCentral.setBorder(null);
			GroupLayout gl_pnCentral = new GroupLayout(pnCentral);
			gl_pnCentral.setHorizontalGroup(
				gl_pnCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnCentral.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblDescription())
							.addComponent(getLblExtension())
							.addComponent(getLblOffset())
							.addComponent(getLblFileSignature())
							.addComponent(getLblCategory()))
						.addGap(26)
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnCentral.createSequentialGroup()
								.addGroup(gl_pnCentral.createParallelGroup(Alignment.TRAILING)
									.addComponent(getTxFileSignature(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
									.addGroup(Alignment.LEADING, gl_pnCentral.createSequentialGroup()
										.addComponent(getTxOffset(), GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(getLblTrailerOffset())
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(getTxTrailerOffset(), GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_pnCentral.createSequentialGroup()
										.addGroup(gl_pnCentral.createParallelGroup(Alignment.TRAILING)
											.addComponent(getTxDescription(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
											.addGroup(Alignment.LEADING, gl_pnCentral.createSequentialGroup()
												.addComponent(getTxExtension(), GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.UNRELATED)
												.addComponent(getLblTrailer())
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(getTxTrailer(), GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)))
										.addPreferredGap(ComponentPlacement.RELATED)))
								.addGap(32))
							.addGroup(gl_pnCentral.createSequentialGroup()
								.addComponent(getTxCategory(), 175, 175, 175)
								.addContainerGap())))
			);
			gl_pnCentral.setVerticalGroup(
				gl_pnCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnCentral.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblExtension())
							.addComponent(getLblTrailer())
							.addComponent(getTxExtension(), GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
							.addComponent(getTxTrailer(), GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblDescription())
							.addComponent(getTxDescription(), GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
						.addGap(13)
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblFileSignature())
							.addComponent(getTxFileSignature(), GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblOffset())
							.addComponent(getTxOffset(), GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblTrailerOffset())
							.addComponent(getTxTrailerOffset(), GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
						.addGap(14)
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblCategory())
							.addComponent(getTxCategory(), GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
						.addContainerGap())
			);
			pnCentral.setLayout(gl_pnCentral);
		}
		return pnCentral;
	}

	/**
	 * Gets the lbl extension.
	 *
	 * @return the lbl extension
	 */
	private JLabel getLblExtension() {
		if (lblExtension == null) {
			lblExtension = new JLabel(Messages.getString("ExtensionsWindow.lblExtension.text")); //$NON-NLS-1$
		}
		return lblExtension;
	}

	/**
	 * Gets the lbl description.
	 *
	 * @return the lbl description
	 */
	private JLabel getLblDescription() {
		if (lblDescription == null) {
			lblDescription = new JLabel(Messages.getString("ExtensionsWindow.lblDescription.text")); //$NON-NLS-1$
		}
		return lblDescription;
	}

	/**
	 * Gets the lbl file signature.
	 *
	 * @return the lbl file signature
	 */
	private JLabel getLblFileSignature() {
		if (lblFileSignature == null) {
			lblFileSignature = new JLabel(Messages.getString("ExtensionsWindow.lblFileSignature.text")); //$NON-NLS-1$
		}
		return lblFileSignature;
	}

	/**
	 * Gets the lbl offset.
	 *
	 * @return the lbl offset
	 */
	private JLabel getLblOffset() {
		if (lblOffset == null) {
			lblOffset = new JLabel(Messages.getString("ExtensionsWindow.lblOffset.text")); //$NON-NLS-1$
		}
		return lblOffset;
	}

	/**
	 * Gets the tx extension.
	 *
	 * @return the tx extension
	 */
	private JTextField getTxExtension() {
		if (txExtension == null) {
			txExtension = new JTextField();
			txExtension.setText("");
			txExtension.setColumns(10);
			txExtension.addKeyListener(creationListener);
		}
		return txExtension;
	}

	/**
	 * Gets the tx description.
	 *
	 * @return the tx description
	 */
	private JTextField getTxDescription() {
		if (txDescription == null) {
			txDescription = new JTextField();
			txDescription.setText("");
			txDescription.setColumns(10);
			txDescription.addKeyListener(creationListener);
		}
		return txDescription;
	}

	/**
	 * Gets the tx file signature.
	 *
	 * @return the tx file signature
	 */
	private JTextField getTxFileSignature() {
		if (txFileSignature == null) {
			txFileSignature = new JTextField();
			txFileSignature.setText("");
			txFileSignature.setColumns(10);
			txFileSignature.addKeyListener(creationListener);
			txFileSignature.addKeyListener(notHexadecimalConsumer);
		}
		return txFileSignature;
	}

	/**
	 * Gets the tx offset.
	 *
	 * @return the tx offset
	 */
	private JTextField getTxOffset() {
		if (txOffset == null) {
			txOffset = new JTextField();
			txOffset.setText("0");
			txOffset.setColumns(10);
			txOffset.addKeyListener(notDigitConsumer);
			txOffset.addKeyListener(creationListener);
		}
		return txOffset;
	}

	/**
	 * Gets the lbl trailer.
	 *
	 * @return the lbl trailer
	 */
	private JLabel getLblTrailer() {
		if (lblTrailer == null) {
			lblTrailer = new JLabel(Messages.getString("ExtensionsWindow.lblTraile.text")); //$NON-NLS-1$
		}
		return lblTrailer;
	}

	/**
	 * Gets the tx trailer.
	 *
	 * @return the tx trailer
	 */
	private JTextField getTxTrailer() {
		if (txTrailer == null) {
			txTrailer = new JTextField();
			txTrailer.setText("");
			txTrailer.setColumns(10);
			txTrailer.addKeyListener(creationListener);
			txTrailer.addKeyListener(notHexadecimalConsumer);
		}
		return txTrailer;
	}

	/**
	 * Gets the lbl trailer offset.
	 *
	 * @return the lbl trailer offset
	 */
	private JLabel getLblTrailerOffset() {
		if (lblTrailerOffset == null) {
			lblTrailerOffset = new JLabel(Messages.getString("NewFsExtensionDialog.lblTrailerOffset.text")); //$NON-NLS-1$
		}
		return lblTrailerOffset;
	}

	/**
	 * Gets the tx trailer offset.
	 *
	 * @return the tx trailer offset
	 */
	private JTextField getTxTrailerOffset() {
		if (txTrailerOffset == null) {
			txTrailerOffset = new JTextField();
			txTrailerOffset.setText("0");
			txTrailerOffset.setColumns(10);
			txTrailerOffset.addKeyListener(notDigitConsumer);
			txTrailerOffset.addKeyListener(creationListener);
		}
		return txTrailerOffset;
	}

	/**
	 * Check ok button enabled.
	 */
	private void checkOkButtonEnabled() {
		if (txExtension.getText().trim().isEmpty() || txDescription.getText().trim().isEmpty()
				|| txFileSignature.getText().isEmpty() || !HexUtil.checkHexFormatSize(txFileSignature.getText())
				|| !HexUtil.checkHexFormatSize(txTrailer.getText())) {
			okButton.setEnabled(false);
		} else {
			okButton.setEnabled(true);
		}
	}

	/**
	 * Gets the lbl category.
	 *
	 * @return the lbl category
	 */
	private JLabel getLblCategory() {
		if (lblCategory == null) {
			lblCategory = new JLabel(Messages.getString("NewFsExtensionDialog.lblCategory.text")); //$NON-NLS-1$
		}
		return lblCategory;
	}

	/**
	 * Gets the tx category.
	 *
	 * @return the tx category
	 */
	private JTextField getTxCategory() {
		if (txCategory == null) {
			txCategory = new JTextField();
			txCategory.setText("");
			txCategory.setColumns(10);
			txCategory.addKeyListener(creationListener);
		}
		return txCategory;
	}
}
