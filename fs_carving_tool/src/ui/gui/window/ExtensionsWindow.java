package ui.gui.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableModel;

import logic.Application;
import logic.io.writer.FsFileWriter;
import model.FsExtension;
import model.exception.PresentationException;
import model.factory.FsExtensionsFactory;
import ui.gui.Messages;
import ui.gui.swing.FsExtensionTableModel;
import ui.gui.swing.JTextFieldSearch;
import util.HexUtil;


/**
 * The Class ExtensionsWindow.
 */
public class ExtensionsWindow extends JDialog {

	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = 1L;
	
	/** The content pane. */
	private JPanel contentPane;
	
	/** The btn add new. */
	private JButton btnAddNew;
	
	/** The btn update. */
	private JButton btnUpdate;
	
	/** The btn delete. */
	private JButton btnDelete;
	
	/** The btn close. */
	private JButton btnClose;
	
	/** The button panel. */
	private JPanel buttonPanel;
	
	/** The pn central. */
	private JPanel pnCentral;
	
	/** The sp extensions. */
	private JScrollPane spExtensions;
	
	/** The tb extensions. */
	private JTable tbExtensions;
	
	/** The mt extensions. */
	private DefaultTableModel mtExtensions;

	/** The extension columns. */
	private String[] extensionColumns = { Messages.getString("ExtensionsWindow.Extension.text"),
			Messages.getString("ExtensionsWindow.Description.text"),
			Messages.getString("ExtensionsWindow.Signature.text"),
			Messages.getString("ExtensionsWindow.Category.text") };

	/** The cb filter pattern. */
	private JComboBox<String> cbFilterPattern;
	
	/** The lbl search pattern. */
	private JLabel lblSearchPattern;
	
	/** The tx search. */
	private JTextFieldSearch txSearch;
	
	/** The pn extension editor. */
	private JPanel pnExtensionEditor;
	
	/** The lbl extension. */
	private JLabel lblExtension;
	
	/** The lbl description. */
	private JLabel lblDescription;
	
	/** The lbl file signature. */
	private JLabel lblFileSignature;
	
	/** The lbl offset. */
	private JLabel lblOffset;
	
	/** The tx extension. */
	private JTextField txExtension;
	
	/** The tx description. */
	private JTextField txDescription;
	
	/** The tx file signature. */
	private JTextField txFileSignature;
	
	/** The tx offset. */
	private JTextField txOffset;
	
	/** The lbl traile. */
	private JLabel lblTraile;
	
	/** The tx trailer. */
	private JTextField txTrailer;
	
	/** The main window. */
	private MainWindow mainWindow;
	
	/** The chars. */
	private String chars = "ABCDEFabcdef0123456789";	

	/** The app. */
	private Application app;
	
	/** The filtered extensions list. */
	private List<FsExtension> filteredExtensionsList = new ArrayList<FsExtension>();

	/** The current extension. */
	private FsExtension currentExtension;
	
	/** The lbl trailer offset. */
	private JLabel lblTrailerOffset;
	
	/** The tx trailer offset. */
	private JTextField txTrailerOffset;

	/** The update listener. */
	private KeyListener updateListener = new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent e) {
			checkUpdateButttonEnabled();
		}
	};
	
	/** The not hexadecimal consumer. */
	private KeyListener notHexadecimalConsumer = new KeyAdapter() {
		@Override
		public void keyTyped(KeyEvent e) {
			char ch = e.getKeyChar();
			if (chars.indexOf(ch) == -1) {
				e.consume();
			}
		}
	};

	/** The not digit consumer. */
	private KeyListener notDigitConsumer = new KeyAdapter() {
		@Override
		public void keyTyped(KeyEvent e) {
			char ch = e.getKeyChar();
			if (!Character.isDigit(ch)) {
				e.consume();
			}
		}
	};
	
	/** The lbl category. */
	private JLabel lblCategory;
	
	/** The tx category. */
	private JTextField txCategory;
	
	/** The btn export extension. */
	private JButton btnExportExtension;

	/**
	 * Create the frame.
	 *
	 * @param app the app
	 * @param mainWindow the main window
	 */
	public ExtensionsWindow(Application app, MainWindow mainWindow) {
		this.app = app;
		this.mainWindow = mainWindow;
		filteredExtensionsList = app.getFsExtensionManager().getFsExtensions();		
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(ExtensionsWindow.class.getResource("/ui/gui/img/fingerprint_search.png")));
		setTitle(Messages.getString("Extensions.Window.Title.text")); //$NON-NLS-1$
		setModal(true);

		setBounds(100, 100, 754, 631);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getButtonPanel(), BorderLayout.SOUTH);
		contentPane.add(getPnCentral(), BorderLayout.CENTER);
		this.setLocationRelativeTo(mainWindow.getFrmFsForensics());
		this.pack();
	}

	/**
	 * Gets the btn add new.
	 *
	 * @return the btn add new
	 */
	private JButton getBtnAddNew() {
		if (btnAddNew == null) {
			btnAddNew = new JButton(Messages.getString("ExtensionsWindow.btnAddNew.text")); //$NON-NLS-1$
			btnAddNew.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JDialog dialog = new NewFsExtensionDialog(app, mainWindow,getExtensionsWindow());
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setModal(true);
					dialog.setVisible(true);
					btnDelete.setEnabled(false);
					updatePerformed();
				}
			});
		}
		return btnAddNew;
	}

	/**
	 * Gets the btn update.
	 *
	 * @return the btn update
	 */
	private JButton getBtnUpdate() {
		if (btnUpdate == null) {
			btnUpdate = new JButton(Messages.getString("ExtensionsWindow.btnUpdate.text")); //$NON-NLS-1$
			btnUpdate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						app.getFsExtensionManager()
								.updateFsExtension(currentExtension, FsExtensionsFactory.getInstance().updateFsExtension(currentExtension,
										txExtension.getText(), txDescription.getText(), txFileSignature.getText(),
										txOffset.getText(), txTrailer.getText(), txTrailerOffset.getText(),
										txCategory.getText()));
						JOptionPane.showMessageDialog(getExtensionsWindow(),
								Messages.getString("ExtensionsWindow.MessageSuccesfulUpdate.text"),
								Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
						updatePerformed();
					} catch (PresentationException e1) {
						JOptionPane.showMessageDialog(getExtensionsWindow(),
								Messages.getString("ExtensionsWindow.MessageExceptionUpdate.text"),
								Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
						e1.printStackTrace();
						e1.printStackTrace();
					}
				}
			});
			btnUpdate.setEnabled(false);
		}
		return btnUpdate;
	}
	
	/**
	 * Update performed.
	 */
	public void updatePerformed() {
		filteredExtensionsList = app.getFsExtensionManager().sortExtensions(cbFilterPattern.getSelectedIndex(), txSearch.getText());
		Collections.sort(filteredExtensionsList, (a,b)-> a.getExtension().compareToIgnoreCase(b.getExtension()));
		clearTable(tbExtensions);
		addExtensionsToTable();
		btnUpdate.setEnabled(false);
		btnDelete.setEnabled(false);
		btnExportExtension.setEnabled(false);
		currentExtension = null;
	}
	
	/**
	 * Gets the extensions window.
	 *
	 * @return the extensions window
	 */
	private ExtensionsWindow getExtensionsWindow() {
		return this;
	}

	/**
	 * Gets the btn delete.
	 *
	 * @return the btn delete
	 */
	private JButton getBtnDelete() {
		if (btnDelete == null) {
			btnDelete = new JButton(Messages.getString("ExtensionsWindow.btnDelete.text")); //$NON-NLS-1$
			btnDelete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int reply = JOptionPane.showConfirmDialog(getExtensionsWindow(),
							Messages.getString("ExtensionsWindow.MessageConfirmDeletion.text"),
							Messages.getString("Shared.Message.text"), JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);
					if (reply == JOptionPane.YES_OPTION) {
						try {
							app.getFsExtensionManager().deleteFsExtension(currentExtension);
							updatePerformed();
							btnDelete.setEnabled(false);
							btnExportExtension.setEnabled(false);
							enablePnExtensionEditor(false);							
						} catch (PresentationException e1) {
							e1.printStackTrace();
							JOptionPane.showMessageDialog(getExtensionsWindow(),
									Messages.getString("ExtensionsWindow.MessageExceptionDelete.text"),
									Messages.getString("Shared.Error.text"), JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			});
			btnDelete.setEnabled(false);
			btnUpdate.setEnabled(false);			
		}
		return btnDelete;
	}

	/**
	 * Gets the btn close.
	 *
	 * @return the btn close
	 */
	private JButton getBtnClose() {
		if (btnClose == null) {
			btnClose = new JButton(Messages.getString("ExtensionsWindow.btnClose.text")); //$NON-NLS-1$
			btnClose.setHorizontalAlignment(SwingConstants.RIGHT);
			btnClose.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
		}
		return btnClose;
	}

	/**
	 * Gets the button panel.
	 *
	 * @return the button panel
	 */
	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel();
			GroupLayout gl_buttonPanel = new GroupLayout(buttonPanel);
			gl_buttonPanel.setHorizontalGroup(gl_buttonPanel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_buttonPanel.createSequentialGroup().addComponent(getBtnAddNew())
							.addPreferredGap(ComponentPlacement.RELATED).addComponent(getBtnUpdate())
							.addPreferredGap(ComponentPlacement.RELATED).addComponent(getBtnDelete())
							.addPreferredGap(ComponentPlacement.RELATED, 148, Short.MAX_VALUE)
							.addComponent(getBtnClose())));
			gl_buttonPanel.setVerticalGroup(gl_buttonPanel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_buttonPanel.createSequentialGroup().addGap(5)
							.addGroup(gl_buttonPanel.createParallelGroup(Alignment.BASELINE).addComponent(getBtnClose())
									.addComponent(getBtnAddNew()).addComponent(getBtnUpdate())
									.addComponent(getBtnDelete()))));
			buttonPanel.setLayout(gl_buttonPanel);
		}
		return buttonPanel;
	}

	/**
	 * Gets the pn central.
	 *
	 * @return the pn central
	 */
	private JPanel getPnCentral() {
		if (pnCentral == null) {
			pnCentral = new JPanel();
			GroupLayout gl_pnCentral = new GroupLayout(pnCentral);
			gl_pnCentral.setHorizontalGroup(
				gl_pnCentral.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_pnCentral.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.LEADING)
							.addComponent(getPnExtensionEditor(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(getSpExtensions(), GroupLayout.DEFAULT_SIZE, 763, Short.MAX_VALUE)
							.addGroup(gl_pnCentral.createSequentialGroup()
								.addComponent(getTxSearch(), GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
								.addGap(47)
								.addComponent(getLblSearchPattern())
								.addGap(18)
								.addComponent(getCbFilterPattern(), GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap())
			);
			gl_pnCentral.setVerticalGroup(
				gl_pnCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnCentral.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_pnCentral.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblSearchPattern())
							.addComponent(getCbFilterPattern(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getTxSearch(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(9)
						.addComponent(getSpExtensions(), GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getPnExtensionEditor(), GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE)
						.addGap(1))
			);
			pnCentral.setLayout(gl_pnCentral);
		}
		return pnCentral;
	}

	/**
	 * Gets the sp extensions.
	 *
	 * @return the sp extensions
	 */
	private JScrollPane getSpExtensions() {
		if (spExtensions == null) {
			spExtensions = new JScrollPane();
			spExtensions.setMinimumSize(new Dimension(10, 23));
			spExtensions.setViewportView(getTbExtensions());
		}
		return spExtensions;
	}

	/**
	 * Gets the tb extensions.
	 *
	 * @return the tb extensions
	 */
	private JTable getTbExtensions() {
		if (tbExtensions == null) {
			mtExtensions = new FsExtensionTableModel(extensionColumns, 0);
			tbExtensions = new JTable(mtExtensions);
			tbExtensions.setFont(new Font("Tahoma", Font.PLAIN, 13));
			tbExtensions.setAlignmentY(6.0f);
			tbExtensions.setAlignmentX(2.0f);
			addExtensionsToTable();
			tbExtensions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tbExtensions.requestFocus();
			ListSelectionModel extensionsSelectionModel = tbExtensions.getSelectionModel();
			extensionsSelectionModel.addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					if (tbExtensions.getSelectedRow() >= 0
							&& tbExtensions.getSelectedRow() < filteredExtensionsList.size()) {
						currentExtension = filteredExtensionsList.get(tbExtensions.getSelectedRow());
						fillTextFieldsWithFsExtension();
					}
					if (currentExtension != null) {
						btnDelete.setEnabled(true);
						btnExportExtension.setEnabled(true);
					}
					enablePnExtensionEditor(currentExtension != null);
				}
			});
		}

		return tbExtensions;
	}

	/**
	 * Clear table.
	 *
	 * @param table the table
	 */
	private void clearTable(JTable table) {
		try {
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			int rowCount = model.getRowCount();
			for (int i = rowCount - 1; i >= 0; i--) {
				model.removeRow(i);
			}
			clearTextFields();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds the extensions to table.
	 */
	private void addExtensionsToTable() {
		Object[] row = new Object[extensionColumns.length];
		for (FsExtension e : filteredExtensionsList) {
			row[0] = e.getExtension();
			row[1] = e.getDescription();
			try {
				row[2] = e.getFileSignature() != null ? HexUtil.bytesToHex(e.getFileSignature()) : null;
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			row[3] = e.getCategory();
			mtExtensions.addRow(row);
		}
	}

	/**
	 * Fill text fields with fs extension.
	 */
	private void fillTextFieldsWithFsExtension() {
		txDescription.setText(currentExtension.getDescription());
		txExtension.setText(currentExtension.getExtension());
		try {
			txFileSignature.setText(
					currentExtension.getFileSignature() != null ? HexUtil.bytesToHex(currentExtension.getFileSignature())
							: null);
			txTrailer.setText(
					currentExtension.getTrailer() != null ? HexUtil.bytesToHex(currentExtension.getTrailer()) : "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		txOffset.setText(String.valueOf(currentExtension.getOffsetLeading()));
		txTrailerOffset.setText(String.valueOf(currentExtension.getOffsetTrailer()));
		txCategory.setText(currentExtension.getCategory());	
	}

	/**
	 * Clear text fields.
	 */
	private void clearTextFields() {
		txDescription.setText("");
		txExtension.setText("");
		txFileSignature.setText("");
		txOffset.setText("");
		txTrailerOffset.setText("");
		txCategory.setText("");
		txTrailer.setText("");
	}

	/**
	 * Gets the cb filter pattern.
	 *
	 * @return the cb filter pattern
	 */
	private JComboBox<String> getCbFilterPattern() {
		if (cbFilterPattern == null) {
			cbFilterPattern = new JComboBox<String>();
			for (int i = 0; i < extensionColumns.length; i++) {
				cbFilterPattern.addItem(extensionColumns[i]);
			}
		}
		return cbFilterPattern;
	}

	/**
	 * Gets the lbl search pattern.
	 *
	 * @return the lbl search pattern
	 */
	private JLabel getLblSearchPattern() {
		if (lblSearchPattern == null) {
			lblSearchPattern = new JLabel(Messages.getString("ExtensionsWindow.lblSearchPattern.text")); //$NON-NLS-1$
		}
		return lblSearchPattern;
	}

	/**
	 * Gets the tx search.
	 *
	 * @return the tx search
	 */
	private JTextFieldSearch getTxSearch() {
		if (txSearch == null) {
			txSearch = new JTextFieldSearch("search_icon", Messages.getString("ExtensionsWindow.SearchHint.text"));
			txSearch.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					updatePerformed();
				}
			});
			txSearch.setSelectionColor(Color.BLACK);
			txSearch.setText("");
			txSearch.setColumns(10);
		}
		return txSearch;
	}

	/**
	 * Gets the pn extension editor.
	 *
	 * @return the pn extension editor
	 */
	private JPanel getPnExtensionEditor() {
		if (pnExtensionEditor == null) {
			pnExtensionEditor = new JPanel();
			pnExtensionEditor.setVisible(true);
			pnExtensionEditor.setBorder(new TitledBorder(
					new TitledBorder(UIManager.getBorder("TitledBorder.border"), Messages.getString("ExtensionsWindow.FileExtensionEditorTitle.text"),
							TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)),
					"", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
			GroupLayout gl_pnExtensionEditor = new GroupLayout(pnExtensionEditor);
			gl_pnExtensionEditor.setHorizontalGroup(
				gl_pnExtensionEditor.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnExtensionEditor.createSequentialGroup()
						.addGap(20)
						.addGroup(gl_pnExtensionEditor.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblExtension())
							.addComponent(getLblDescription())
							.addComponent(getLblFileSignature())
							.addComponent(getLblCategory())
							.addComponent(getLblOffset()))
						.addGap(30)
						.addGroup(gl_pnExtensionEditor.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_pnExtensionEditor.createSequentialGroup()
								.addComponent(getTxFileSignature(), GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
								.addContainerGap())
							.addGroup(gl_pnExtensionEditor.createSequentialGroup()
								.addComponent(getTxDescription(), GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
								.addContainerGap())
							.addGroup(gl_pnExtensionEditor.createSequentialGroup()
								.addComponent(getTxOffset(), GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addComponent(getLblTrailerOffset(), GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getTxTrailerOffset(), GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
								.addContainerGap())
							.addGroup(gl_pnExtensionEditor.createSequentialGroup()
								.addGroup(gl_pnExtensionEditor.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_pnExtensionEditor.createSequentialGroup()
										.addComponent(getTxExtension(), GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(getLblTraile())
										.addGap(26)
										.addComponent(getTxTrailer(), GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE))
									.addGroup(Alignment.TRAILING, gl_pnExtensionEditor.createSequentialGroup()
										.addComponent(getTxCategory(), GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED, 297, Short.MAX_VALUE)
										.addComponent(getBtnExportExtension())))
								.addGap(60))))
			);
			gl_pnExtensionEditor.setVerticalGroup(
				gl_pnExtensionEditor.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnExtensionEditor.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_pnExtensionEditor.createParallelGroup(Alignment.BASELINE)
							.addComponent(getTxExtension(), GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblTraile())
							.addComponent(getTxTrailer(), GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblExtension()))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_pnExtensionEditor.createParallelGroup(Alignment.BASELINE)
							.addComponent(getTxDescription(), GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblDescription()))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_pnExtensionEditor.createParallelGroup(Alignment.BASELINE)
							.addComponent(getTxFileSignature(), GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblFileSignature()))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_pnExtensionEditor.createParallelGroup(Alignment.BASELINE)
							.addComponent(getTxOffset(), GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
							.addComponent(getTxTrailerOffset(), GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblTrailerOffset())
							.addComponent(getLblOffset()))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_pnExtensionEditor.createParallelGroup(Alignment.TRAILING)
							.addComponent(getBtnExportExtension())
							.addGroup(gl_pnExtensionEditor.createSequentialGroup()
								.addGroup(gl_pnExtensionEditor.createParallelGroup(Alignment.BASELINE)
									.addComponent(getTxCategory(), GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblCategory(), GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
								.addGap(7)))
						.addGap(16))
			);
			pnExtensionEditor.setLayout(gl_pnExtensionEditor);
			enablePnExtensionEditor(false);
		}
		return pnExtensionEditor;
	}

	/**
	 * Gets the lbl extension.
	 *
	 * @return the lbl extension
	 */
	private JLabel getLblExtension() {
		if (lblExtension == null) {
			lblExtension = new JLabel(Messages.getString("ExtensionsWindow.lblExtension.text")); //$NON-NLS-1$
		}
		return lblExtension;
	}

	/**
	 * Gets the lbl description.
	 *
	 * @return the lbl description
	 */
	private JLabel getLblDescription() {
		if (lblDescription == null) {
			lblDescription = new JLabel(Messages.getString("ExtensionsWindow.lblDescription.text")); //$NON-NLS-1$
		}
		return lblDescription;
	}

	/**
	 * Gets the lbl file signature.
	 *
	 * @return the lbl file signature
	 */
	private JLabel getLblFileSignature() {
		if (lblFileSignature == null) {
			lblFileSignature = new JLabel(Messages.getString("ExtensionsWindow.lblFileSignature.text")); //$NON-NLS-1$
		}
		return lblFileSignature;
	}

	/**
	 * Gets the lbl offset.
	 *
	 * @return the lbl offset
	 */
	private JLabel getLblOffset() {
		if (lblOffset == null) {
			lblOffset = new JLabel(Messages.getString("ExtensionsWindow.lblOffset.text")); //$NON-NLS-1$
		}
		return lblOffset;
	}

	/**
	 * Gets the tx extension.
	 *
	 * @return the tx extension
	 */
	private JTextField getTxExtension() {
		if (txExtension == null) {
			txExtension = new JTextField();
			txExtension.setText("");
			txExtension.setColumns(10);
			txExtension.addKeyListener(updateListener);
		}
		return txExtension;
	}

	/**
	 * Gets the tx description.
	 *
	 * @return the tx description
	 */
	private JTextField getTxDescription() {
		if (txDescription == null) {
			txDescription = new JTextField();
			txDescription.setText("");
			txDescription.setColumns(10);
			txDescription.addKeyListener(updateListener);
		}
		return txDescription;
	}

	/**
	 * Gets the tx file signature.
	 *
	 * @return the tx file signature
	 */
	private JTextField getTxFileSignature() {
		if (txFileSignature == null) {
			txFileSignature = new JTextField();
			txFileSignature.setText("");
			txFileSignature.setColumns(10);
			txFileSignature.addKeyListener(updateListener);
			txFileSignature.addKeyListener(notHexadecimalConsumer);
		}
		return txFileSignature;
	}

	/**
	 * Gets the tx offset.
	 *
	 * @return the tx offset
	 */
	private JTextField getTxOffset() {
		if (txOffset == null) {
			txOffset = new JTextField();
			txOffset.setText("");
			txOffset.setColumns(10);
			txOffset.addKeyListener(notDigitConsumer);
			txOffset.addKeyListener(updateListener);
		}
		return txOffset;
	}

	/**
	 * Gets the lbl traile.
	 *
	 * @return the lbl traile
	 */
	private JLabel getLblTraile() {
		if (lblTraile == null) {
			lblTraile = new JLabel(Messages.getString("ExtensionsWindow.lblTraile.text")); //$NON-NLS-1$
		}
		return lblTraile;
	}

	/**
	 * Gets the tx trailer.
	 *
	 * @return the tx trailer
	 */
	private JTextField getTxTrailer() {
		if (txTrailer == null) {
			txTrailer = new JTextField();
			txTrailer.setText("");
			txTrailer.setColumns(10);
			txTrailer.addKeyListener(updateListener);
		}
		return txTrailer;
	}

	/**
	 * Gets the lbl trailer offset.
	 *
	 * @return the lbl trailer offset
	 */
	private JLabel getLblTrailerOffset() {
		if (lblTrailerOffset == null) {
			lblTrailerOffset = new JLabel(Messages.getString("NewFsExtensionDialog.lblTrailerOffset.text")); //$NON-NLS-1$
		}
		return lblTrailerOffset;
	}

	/**
	 * Gets the tx trailer offset.
	 *
	 * @return the tx trailer offset
	 */
	private JTextField getTxTrailerOffset() {
		if (txTrailerOffset == null) {
			txTrailerOffset = new JTextField();
			txTrailerOffset.setText("");
			txTrailerOffset.setColumns(10);
			txTrailerOffset.addKeyListener(notHexadecimalConsumer);
			txTrailerOffset.addKeyListener(updateListener);
		}
		return txTrailerOffset;
	}

	/**
	 * Check update buttton enabled.
	 */
	private void checkUpdateButttonEnabled() {
		if (currentExtension != null) {
			try {
				if ((currentExtension.getDescription().equals(txDescription.getText())
						&& currentExtension.getExtension().equals(txExtension.getText())
						&& currentExtension.getCategory().equals(txCategory.getText())
						&& HexUtil.bytesToHex(currentExtension.getFileSignature()).equals(txFileSignature.getText())
						&& (txOffset.getText().equals("") ? true
								: currentExtension.getOffsetLeading() == Integer.valueOf(txOffset.getText()))
						&& (txTrailerOffset.getText().contentEquals("") ? true
								: currentExtension.getOffsetTrailer() == Integer.valueOf(txTrailerOffset.getText()))
						&& (currentExtension.getTrailer() == null ? txTrailer.getText().equals("")
								: HexUtil.bytesToHex(currentExtension.getTrailer()).equals(txTrailer.getText())))
						|| txFileSignature.getText().isEmpty() || !HexUtil.checkHexFormatSize(txFileSignature.getText())
						|| !HexUtil.checkHexFormatSize(txTrailer.getText())) {
					btnUpdate.setEnabled(false);

				} else {
					btnUpdate.setEnabled(true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	/**
	 * Gets the lbl category.
	 *
	 * @return the lbl category
	 */
	private JLabel getLblCategory() {
		if (lblCategory == null) {
			lblCategory = new JLabel(Messages.getString("ExtensionsWindow.lblCategory.text")); //$NON-NLS-1$
		}
		return lblCategory;
	}

	/**
	 * Gets the tx category.
	 *
	 * @return the tx category
	 */
	private JTextField getTxCategory() {
		if (txCategory == null) {
			txCategory = new JTextField();
			txCategory.setText("");
			txCategory.setColumns(10);
			txCategory.addKeyListener(updateListener);
		}
		return txCategory;
	}

	/**
	 * Enable pn extension editor.
	 *
	 * @param enable the enable
	 */
	private void enablePnExtensionEditor(boolean enable) {
		pnExtensionEditor.setEnabled(enable);
		txExtension.setEnabled(enable);
		txDescription.setEnabled(enable);
		txOffset.setEnabled(enable);
		txTrailer.setEnabled(enable);
		txTrailerOffset.setEnabled(enable);
		txFileSignature.setEnabled(enable);
		txTrailerOffset.setEnabled(enable);
		txCategory.setEnabled(enable);
	}
	
	/**
	 * Gets the btn export extension.
	 *
	 * @return the btn export extension
	 */
	private JButton getBtnExportExtension() {
		if (btnExportExtension == null) {
			btnExportExtension = new JButton(Messages.getString("ExtensionsWindow.btnExportExtension.text")); //$NON-NLS-1$
			btnExportExtension.setEnabled(false);
			btnExportExtension.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
					jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int returnValue = jfc.showOpenDialog(getExtensionsWindow());
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						File selectedFile = jfc.getSelectedFile();
						try {
							FsFileWriter.getInstance().exportGCKFileExtension(selectedFile, currentExtension);
							JOptionPane.showMessageDialog(getExtensionsWindow(), Messages.getString("ExtensionsWindow.FileExportSuccess.text"),
									Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
						} catch (IOException e1) {						
							e1.printStackTrace();
							JOptionPane.showMessageDialog(getExtensionsWindow(), Messages.getString("ExtensionsWindow.FileExportError.text"),
									Messages.getString("Shared.Error.text"), JOptionPane.INFORMATION_MESSAGE);
						}
					}
				}
			});
		}
		return btnExportExtension;
	}

}
