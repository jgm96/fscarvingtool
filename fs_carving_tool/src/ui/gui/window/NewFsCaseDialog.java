package ui.gui.window;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Calendar;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.Logger;

import conf.FsLogger;
import logic.Application;
import model.FsCase;
import model.exception.PresentationException;
import model.factory.FsCasesFactory;
import ui.gui.Messages;
import javax.swing.SwingConstants;


/**
 * The Class NewFsCaseDialog.
 */
public class NewFsCaseDialog extends JDialog {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The content panel. */
	private final JPanel contentPanel = new JPanel();

	/** The app. */
	@SuppressWarnings("unused")
	private Application app;
	
	/** The main window. */
	@SuppressWarnings("unused")
	private MainWindow mainWindow;

	/** The lbl case name. */
	private JLabel lblCaseName;
	
	/** The tx case name. */
	private JTextField txCaseName;
	
	/** The lbl investigator. */
	private JLabel lblInvestigator;
	
	/** The tx investigator. */
	private JTextField txInvestigator;
	
	/** The lbl organization. */
	private JLabel lblOrganization;
	
	/** The tx organization. */
	private JTextField txOrganization;
	
	/** The lbl contact details. */
	private JLabel lblContactDetails;
	
	/** The tx contact details. */
	private JTextField txContactDetails;
	
	/** The lbl case folder. */
	private JLabel lblCaseFolder;
	
	/** The rdbtn default location. */
	private JRadioButton rdbtnDefaultLocation;
	
	/** The rdbtn custom location. */
	private JRadioButton rdbtnCustomLocation;
	
	/** The tx case folder. */
	private JTextField txCaseFolder;
	
	/** The btn browse. */
	private JButton btnBrowse;
	
	/** The chckbx log case activity. */
	private JCheckBox chckbxLogCaseActivity;
	
	/** The ok button. */
	private JButton okButton;
	
	/** The cancel button. */
	private JButton cancelButton;	
	
	/** The load cases dialog. */
	@SuppressWarnings("unused")
	private LoadCasesDialog loadCasesDialog;

	/** The current dir. */
	File currentDir = new File(".");
	
	/** The logger. */
	Logger logger = FsLogger.getInstance();

	/** The chooser. */
	JFileChooser chooser;
	
	/** The lbl description. */
	private JLabel lblDescription;
	
	/** The tx description. */
	private JTextField txDescription;

	/**
	 * Create the dialog.
	 *
	 * @param app the app
	 * @param mainWindow the main window
	 * @param loadCasesDialog the load cases dialog
	 */
	public NewFsCaseDialog(Application app, MainWindow mainWindow, LoadCasesDialog loadCasesDialog) {
		this.app = app;
		this.mainWindow = mainWindow;		
		setModal(true);
		setTitle(Messages.getString("NewFsCaseDialog.Title.text")); //$NON-NLS-1$
		setIconImage(Toolkit.getDefaultToolkit().getImage(NewFsCaseDialog.class.getResource("/ui/gui/img/fingerprint_search.png"))); //$NON-NLS-1$
		setBounds(100, 100, 662, 354);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.WEST);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(getLblContactDetails())
						.addComponent(getLblOrganization())
						.addComponent(getLblCaseName())
						.addComponent(getLblInvestigator())
						.addComponent(getLblDescription(), GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE))
					.addGap(16)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(getTxDescription(), GroupLayout.PREFERRED_SIZE, 313, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(getTxCaseName(), GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
								.addComponent(getTxInvestigator(), GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
								.addComponent(getTxOrganization(), GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
								.addComponent(getTxContactDetails(), GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE))
							.addGap(205))))
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(47)
					.addComponent(getLblCaseFolder())
					.addGap(16)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(getChckbxLogCaseActivity())
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(getTxCaseFolder(), GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getBtnBrowse(), GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(getRdbtnDefaultLocation(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap(18)
							.addComponent(getRdbtnCustomLocation(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap(122)))
					.addGap(132))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getTxCaseName(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblCaseName()))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getTxInvestigator(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblInvestigator()))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblOrganization())
						.addComponent(getTxOrganization(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblContactDetails())
						.addComponent(getTxContactDetails(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getTxDescription(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblDescription()))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getRdbtnDefaultLocation())
						.addComponent(getRdbtnCustomLocation())
						.addComponent(getLblCaseFolder()))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getTxCaseFolder(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getBtnBrowse()))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(getChckbxLogCaseActivity())
					.addContainerGap(72, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton(Messages.getString("NewFsCaseDialog.OK")); //$NON-NLS-1$
				okButton.setEnabled(false);
				okButton.setActionCommand(Messages.getString("")); //$NON-NLS-1$
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							FsCase fsCase = FsCasesFactory.getInstance().createFsCase(txCaseName.getText(),
									txInvestigator.getText(), txOrganization.getText(), txContactDetails.getText(),
									Calendar.getInstance().getTime(), txDescription.getText(), txCaseFolder.getText(), chckbxLogCaseActivity.isSelected());
							app.getFsCaseManager().newFsCase(fsCase);							
							JOptionPane.showMessageDialog(getNewFsCaseWindow(),
									Messages.getString("NewFsCaseWindow.MessageSuccesfulCreation.text"),
									Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
							app.setCurrentFsCase(fsCase);
							mainWindow.enableCaseButtons(true);
							mainWindow.showPanels(false, true, false, false, false);
							mainWindow.fillManageCasesPanel();
							if(loadCasesDialog != null) {
								loadCasesDialog.dispose();
							}
							dispose();							
						} catch (PresentationException e1) {
							e1.printStackTrace();
							logger.error(e1.getMessage());
						}
					}
				});
			}
			{
				cancelButton = new JButton(Messages.getString("NewFsCaseDialog.Cancel.text")); //$NON-NLS-1$
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand(Messages.getString("NewFsCaseDialog.Cancel.text")); //$NON-NLS-1$
				buttonPane.add(cancelButton);
			}
		}
		addRbGroup();
		this.setLocationRelativeTo(mainWindow.getFrmFsForensics());
	}
	
	/**
	 * Gets the new fs case window.
	 *
	 * @return the new fs case window
	 */
	private NewFsCaseDialog getNewFsCaseWindow() {
		return this;
	}

	/**
	 * Adds the rb group.
	 */
	private void addRbGroup() {
		ButtonGroup rbg = new ButtonGroup();
		rbg.add(rdbtnDefaultLocation);
		rbg.add(rdbtnCustomLocation);
	}

	/**
	 * Gets the lbl case name.
	 *
	 * @return the lbl case name
	 */
	private JLabel getLblCaseName() {
		if (lblCaseName == null) {
			lblCaseName = new JLabel(Messages.getString("NewFsCaseDialog.CaseName.text")); //$NON-NLS-1$
		}
		return lblCaseName;
	}

	/**
	 * Gets the tx case name.
	 *
	 * @return the tx case name
	 */
	private JTextField getTxCaseName() {
		if (txCaseName == null) {
			txCaseName = new JTextField();
			txCaseName.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					if (txCaseName.getText().isEmpty()) {
						okButton.setEnabled(false);
					} else {
						okButton.setEnabled(true);
					}
				}
			});
			txCaseName.setColumns(10);
		}
		return txCaseName;
	}

	/**
	 * Gets the lbl investigator.
	 *
	 * @return the lbl investigator
	 */
	private JLabel getLblInvestigator() {
		if (lblInvestigator == null) {
			lblInvestigator = new JLabel(Messages.getString("NewFsCaseDialog.Investigator.text")); //$NON-NLS-1$
		}
		return lblInvestigator;
	}

	/**
	 * Gets the tx investigator.
	 *
	 * @return the tx investigator
	 */
	private JTextField getTxInvestigator() {
		if (txInvestigator == null) {
			txInvestigator = new JTextField();
			txInvestigator.setColumns(10);
		}
		return txInvestigator;
	}

	/**
	 * Gets the lbl organization.
	 *
	 * @return the lbl organization
	 */
	private JLabel getLblOrganization() {
		if (lblOrganization == null) {
			lblOrganization = new JLabel(Messages.getString("NewFsCaseDialog.Organization.text")); //$NON-NLS-1$
		}
		return lblOrganization;
	}

	/**
	 * Gets the tx organization.
	 *
	 * @return the tx organization
	 */
	private JTextField getTxOrganization() {
		if (txOrganization == null) {
			txOrganization = new JTextField();
			txOrganization.setColumns(10);
		}
		return txOrganization;
	}

	/**
	 * Gets the lbl contact details.
	 *
	 * @return the lbl contact details
	 */
	private JLabel getLblContactDetails() {
		if (lblContactDetails == null) {
			lblContactDetails = new JLabel(Messages.getString("NewFsCaseDialog.ContactDetails.text")); //$NON-NLS-1$
		}
		return lblContactDetails;
	}

	/**
	 * Gets the tx contact details.
	 *
	 * @return the tx contact details
	 */
	private JTextField getTxContactDetails() {
		if (txContactDetails == null) {
			txContactDetails = new JTextField();
			txContactDetails.setColumns(10);
		}
		return txContactDetails;
	}

	/**
	 * Gets the lbl case folder.
	 *
	 * @return the lbl case folder
	 */
	private JLabel getLblCaseFolder() {
		if (lblCaseFolder == null) {
			lblCaseFolder = new JLabel(Messages.getString("NewFsCaseDialog.CaseFolder.text")); //$NON-NLS-1$
		}
		return lblCaseFolder;
	}

	/**
	 * Gets the rdbtn default location.
	 *
	 * @return the rdbtn default location
	 */
	private JRadioButton getRdbtnDefaultLocation() {
		if (rdbtnDefaultLocation == null) {
			rdbtnDefaultLocation = new JRadioButton(Messages.getString("NewFsCaseDialog.DefaultLocation.text")); //$NON-NLS-1$
			rdbtnDefaultLocation.setSelected(true);
		}
		return rdbtnDefaultLocation;
	}

	/**
	 * Gets the rdbtn custom location.
	 *
	 * @return the rdbtn custom location
	 */
	private JRadioButton getRdbtnCustomLocation() {
		if (rdbtnCustomLocation == null) {
			rdbtnCustomLocation = new JRadioButton(Messages.getString("NewFsCaseDialog.CustomLocation.text")); //$NON-NLS-1$
			rdbtnCustomLocation.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					if (rdbtnCustomLocation.isSelected()) {
						btnBrowse.setEnabled(true);
					} else {
						btnBrowse.setEnabled(false);
					}
				}
			});
		}
		return rdbtnCustomLocation;
	}

	/**
	 * Gets the tx case folder.
	 *
	 * @return the tx case folder
	 */
	private JTextField getTxCaseFolder() {
		if (txCaseFolder == null) {
			txCaseFolder = new JTextField();
			txCaseFolder.setEnabled(false);
			txCaseFolder.setColumns(10);
			txCaseFolder.setText(currentDir.getAbsolutePath());
		}
		return txCaseFolder;
	}

	/**
	 * Gets the btn browse.
	 *
	 * @return the btn browse
	 */
	private JButton getBtnBrowse() {
		if (btnBrowse == null) {
			btnBrowse = new JButton(Messages.getString("NewFsCaseDialog.Browse.text")); //$NON-NLS-1$
			btnBrowse.setEnabled(false);
			btnBrowse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					chooser = new JFileChooser();
					chooser.setCurrentDirectory(currentDir); // $NON-NLS-1$
					chooser.setDialogTitle(Messages.getString("NewFsCaseDialog.SelectDirectory.text"));
					chooser.setAcceptAllFileFilterUsed(false);
					chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
						txCaseFolder.setText(chooser.getSelectedFile().getAbsolutePath());
					}
				}

			});
		}
		return btnBrowse;
	}

	/**
	 * Gets the chckbx log case activity.
	 *
	 * @return the chckbx log case activity
	 */
	private JCheckBox getChckbxLogCaseActivity() {
		if (chckbxLogCaseActivity == null) {
			chckbxLogCaseActivity = new JCheckBox(Messages.getString("NewFsCaseDialog.Chckbox.LogCaseActivity.text")); //$NON-NLS-1$
		}
		return chckbxLogCaseActivity;
	}
	
	/**
	 * Gets the lbl description.
	 *
	 * @return the lbl description
	 */
	private JLabel getLblDescription() {
		if (lblDescription == null) {
			lblDescription = new JLabel(Messages.getString("NewFsCaseDialog.lblDescription.text")); //$NON-NLS-1$
			lblDescription.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblDescription;
	}
	
	/**
	 * Gets the tx description.
	 *
	 * @return the tx description
	 */
	private JTextField getTxDescription() {
		if (txDescription == null) {
			txDescription = new JTextField();
			txDescription.setColumns(10);
		}
		return txDescription;
	}
}
