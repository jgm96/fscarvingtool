package ui.gui.window;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

import logic.Application;
import model.exception.PresentationException;
import ui.gui.Messages;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


/**
 * The Class PreferencesDialog.
 */
public class PreferencesDialog extends JDialog {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The content panel. */
	private final JPanel contentPanel = new JPanel();
	
	/** The lbl language. */
	private JLabel lblLanguage;
	
	/** The rdbtn spanish. */
	private JRadioButton rdbtnSpanish;
	
	/** The rdbtn englishuk. */
	private JRadioButton rdbtnEnglishuk;
	
	/** The rbtn group language. */
	private ButtonGroup rbtnGroupLanguage;
	
	/** The app. */
	private Application app;
	
	/** The locale. */
	private String locale;
	
	/** The ok button. */
	private JButton okButton;
	
	/** The lbl rds server. */
	private JLabel lblRdsServer;
	
	/** The tx RDS server. */
	private JTextField txRDSServer;
	
	/** The lbl rds port. */
	private JLabel lblRdsPort;
	
	/** The tx RDS port. */
	private JTextField txRDSPort;
	
	/** The server. */
	private String server;
	
	/** The port. */
	private String port;

	/**
	 * Create the dialog.
	 *
	 * @param app the app
	 * @param mainWindow the main window
	 */
	public PreferencesDialog(Application app, MainWindow mainWindow) {
		this.app = app;
		setTitle(Messages.getString("MainWindow.btnPreferences.text")); //$NON-NLS-1$
		loadRDSSettings();
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(PreferencesDialog.class.getResource("/ui/gui/img/fingerprint_search.png")));
		setBounds(100, 100, 412, 311);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		rbtnGroupLanguage = new ButtonGroup();
		contentPanel.setLayout(null);
		contentPanel.add(getLblLanguage());
		contentPanel.add(getRdbtnSpanish());
		contentPanel.add(getRdbtnEnglishuk());
		contentPanel.add(getLblRdsServer());
		contentPanel.add(getTxRDSServer());
		contentPanel.add(getLblRdsPort());
		contentPanel.add(getTxRDSPort());		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton(Messages.getString("NewFsCaseDialog.OK")); //$NON-NLS-1$
				okButton.setEnabled(false);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							if (rdbtnSpanish.isSelected()) {
								app.getFsSettingsManager().updateFsSetting("locale", "es");
							} else {
								app.getFsSettingsManager().updateFsSetting("locale", "en_GB");
							}
							app.getFsSettingsManager().updateFsSetting("rdsServer", txRDSServer.getText().trim());
							app.getFsSettingsManager().updateFsSetting("rdsPort", txRDSPort.getText().trim());
							JOptionPane.showMessageDialog(mainWindow.getFrmFsForensics(),
									Messages.getString("PreferencesDialog.MessageSuccesfulUpdate.text"),
									Messages.getString("Shared.Message.text"), JOptionPane.INFORMATION_MESSAGE);
							dispose();
						} catch (PresentationException e1) {
							e1.printStackTrace();
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton(Messages.getString("Shared.Cancel")); //$NON-NLS-1$
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		loadLocale();		
		this.setLocationRelativeTo(mainWindow.getFrmFsForensics());
	}

	/**
	 * Gets the lbl language.
	 *
	 * @return the lbl language
	 */
	private JLabel getLblLanguage() {
		if (lblLanguage == null) {
			lblLanguage = new JLabel(Messages.getString("SettingsDialog.lblLanguage.text")); //$NON-NLS-1$
			lblLanguage.setFont(new Font("Tahoma", Font.BOLD, 11));
			lblLanguage.setBounds(20, 22, 80, 14);
		}
		return lblLanguage;
	}

	/**
	 * Gets the rdbtn spanish.
	 *
	 * @return the rdbtn spanish
	 */
	private JRadioButton getRdbtnSpanish() {
		if (rdbtnSpanish == null) {
			rdbtnSpanish = new JRadioButton(Messages.getString("SettingsDialog.rdbtnSpanish.text")); //$NON-NLS-1$
			rdbtnSpanish.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {					
					checkOkButtonEnabled();					
				}
			});
			rdbtnSpanish.setBounds(143, 56, 109, 23);
			rbtnGroupLanguage.add(rdbtnSpanish);
		}
		return rdbtnSpanish;
	}

	/**
	 * Gets the rdbtn englishuk.
	 *
	 * @return the rdbtn englishuk
	 */
	private JRadioButton getRdbtnEnglishuk() {
		if (rdbtnEnglishuk == null) {
			rdbtnEnglishuk = new JRadioButton(Messages.getString("SettingsDialog.rdbtnEnglishuk.text")); //$NON-NLS-1$
			rdbtnEnglishuk.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {				
					checkOkButtonEnabled();					
				}
			});
			rdbtnEnglishuk.setBounds(20, 56, 109, 23);
			rbtnGroupLanguage.add(rdbtnEnglishuk);
		}
		return rdbtnEnglishuk;
	}

	/**
	 * Load locale.
	 */
	private void loadLocale() {
		locale = app.getFsSettingsManager().getSettings().get("locale");
		if (locale.equalsIgnoreCase("es")) {
			rdbtnSpanish.setSelected(true);
		} else {
			rdbtnEnglishuk.setSelected(true);
		}
	}
	
	/**
	 * Check ok button enabled.
	 */
	private void checkOkButtonEnabled() {
		if((locale.equalsIgnoreCase("es") && rdbtnSpanish.isSelected() || locale.equalsIgnoreCase("en_GB") && rdbtnEnglishuk.isSelected())
				&& server.equalsIgnoreCase(txRDSServer.getText().trim()) && port.equalsIgnoreCase(txRDSPort.getText().trim())) {
			okButton.setEnabled(false);
		}
		else {
			okButton.setEnabled(true);
		}
	}
	
	/**
	 * Load RDS settings.
	 */
	private void loadRDSSettings() {
		server = app.getFsSettingsManager().getSettings().get("rdsServer");
		port = app.getFsSettingsManager().getSettings().get("rdsPort");		
	}
	
	/**
	 * Gets the lbl rds server.
	 *
	 * @return the lbl rds server
	 */
	private JLabel getLblRdsServer() {
		if (lblRdsServer == null) {
			lblRdsServer = new JLabel(Messages.getString("PreferencesDialog.lblRdsServer.text")); //$NON-NLS-1$
			lblRdsServer.setFont(new Font("Tahoma", Font.BOLD, 11));
			lblRdsServer.setBounds(20, 97, 80, 14);			
		}
		return lblRdsServer;
	}
	
	/**
	 * Gets the tx RDS server.
	 *
	 * @return the tx RDS server
	 */
	private JTextField getTxRDSServer() {
		if (txRDSServer == null) {
			txRDSServer = new JTextField();
			txRDSServer.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					checkOkButtonEnabled();
				}
			});
			txRDSServer.setText("");
			txRDSServer.setBounds(20, 125, 158, 20);
			txRDSServer.setColumns(10);
			txRDSServer.setText(server);
		}
		return txRDSServer;
	}
	
	/**
	 * Gets the lbl rds port.
	 *
	 * @return the lbl rds port
	 */
	private JLabel getLblRdsPort() {
		if (lblRdsPort == null) {
			lblRdsPort = new JLabel(Messages.getString("PreferencesDialog.lblRdsPort.text")); //$NON-NLS-1$
			lblRdsPort.setFont(new Font("Tahoma", Font.BOLD, 11));
			lblRdsPort.setBounds(20, 167, 80, 14);
		}
		return lblRdsPort;
	}
	
	/**
	 * Gets the tx RDS port.
	 *
	 * @return the tx RDS port
	 */
	private JTextField getTxRDSPort() {
		if (txRDSPort == null) {
			txRDSPort = new JTextField();
			txRDSPort.setText("");
			txRDSPort.setBounds(20, 192, 86, 20);
			txRDSPort.setColumns(10);
			txRDSPort.setText(port);
			txRDSPort.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					checkOkButtonEnabled();
				}
			});
		}
		return txRDSPort;
	}
}
