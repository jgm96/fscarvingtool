package ui.gui;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Messages {
	////////////////////////////////////////////////////////////////////////////
	//
	// Constructor
	//
	////////////////////////////////////////////////////////////////////////////
	private Messages() {
		// do not instantiate
	}
	////////////////////////////////////////////////////////////////////////////
	//
	// Bundle access
	//
	////////////////////////////////////////////////////////////////////////////
	private static final String BUNDLE_NAME = "ui.gui.messages"; //$NON-NLS-1$
	private static final ResourceBundle RESOURCE_BUNDLE = null;
	private static ResourceBundle getBundle() {
		return RESOURCE_BUNDLE == null ? 
				ResourceBundle.getBundle(BUNDLE_NAME, Locale.getDefault()) : RESOURCE_BUNDLE;
	}
	////////////////////////////////////////////////////////////////////////////
	//
	// Strings access
	//
	////////////////////////////////////////////////////////////////////////////
	public static String getString(String key) {
		try {			
			ResourceBundle bundle = getBundle();			
			return bundle.getString(key);
		} catch (MissingResourceException e) {
			return "!" + key + "!";
		}
	}
}
