package persistence;

import java.util.List;

import model.FsExtension;
import persistence.util.Jpa;


/**
 * The Class FsExtensionFinder.
 */
public class FsExtensionFinder {

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	public static List<FsExtension> findAll() {

		return Jpa.getManager().createNamedQuery("FsExtension.findAll", FsExtension.class).getResultList();
	}

	/**
	 * Delete all.
	 *
	 * @return the int
	 */
	public static int deleteAll() {

		return Jpa.getManager().createNamedQuery("FsExtension.deleteAll", FsExtension.class).executeUpdate();
	}
	
	/**
	 * Find repeated extensions.
	 *
	 * @return the list
	 */
	public static List<Object[]> findRepeatedExtensions(){
		
		return Jpa.getManager().createNamedQuery("FsExtension.findRepeatedExtensions", Object[].class).getResultList();				
	}	

}