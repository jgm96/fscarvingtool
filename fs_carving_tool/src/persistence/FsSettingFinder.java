package persistence;

import java.util.List;

import model.FsSetting;
import persistence.util.Jpa;


/**
 * The Class FsSettingFinder.
 */
public class FsSettingFinder {

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	public static List<FsSetting> findAll() {

		return Jpa.getManager().createNamedQuery("FsSetting.findAll", FsSetting.class).getResultList();			
	}
	
	/**
	 * Update fs setting.
	 *
	 * @param name the name
	 * @param value the value
	 * @return the int
	 */
	public static int updateFsSetting(String name, String value) {
		return Jpa.getManager().createNamedQuery("FsSetting.updateFsSetting").setParameter(1, name).setParameter(2, value).executeUpdate();
	}
		

}