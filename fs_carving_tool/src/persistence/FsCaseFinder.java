package persistence;

import java.util.List;

import model.FsCase;
import persistence.util.Jpa;


/**
 * The Class FsCaseFinder.
 */
public class FsCaseFinder {

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	public static List<FsCase> findAll() {

		return Jpa.getManager().createNamedQuery("FsCase.findAll", FsCase.class).getResultList();
	}

	/**
	 * Delete all.
	 *
	 * @return the int
	 */
	public static int deleteAll() {

		return Jpa.getManager().createNamedQuery("FsCase.deleteAll", FsCase.class).executeUpdate();
	}

}