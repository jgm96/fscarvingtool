package business;

import java.util.List;

import model.FsSetting;
import model.exception.BusinessException;

/**
 * The Interface FsSettingService.
 */
public interface FsSettingService {
	
	/**
	 * Update fs setting.
	 *
	 * @param name the name
	 * @param value the value
	 * @throws BusinessException the business exception
	 */
	void updateFsSetting(String name, String value) throws BusinessException;
	
	/**
	 * Find all fs settings.
	 *
	 * @return the list
	 * @throws BusinessException the business exception
	 */
	List<FsSetting> findAllFsSettings() throws BusinessException;

}
