package business;

import java.util.List;

import model.FsExtension;
import model.exception.BusinessException;

/**
 * The Interface FsExtensionService.
 */
public interface FsExtensionService {

	/**
	 * New fs extension.
	 *
	 * @param fsExtension the fs extension
	 * @throws BusinessException the business exception
	 */
	void newFsExtension(FsExtension fsExtension) throws BusinessException;

	/**
	 * Delete fs extension.
	 *
	 * @param idFsExtension the id fs extension
	 * @throws BusinessException the business exception
	 */
	void deleteFsExtension(Long idFsExtension) throws BusinessException;

	/**
	 * Update fs extension.
	 *
	 * @param fsExtension the fs extension
	 * @throws BusinessException the business exception
	 */
	void updateFsExtension(FsExtension fsExtension) throws BusinessException;

	/**
	 * Delete all fs extensions.
	 *
	 * @return the int
	 * @throws BusinessException the business exception
	 */
	int deleteAllFsExtensions() throws BusinessException;

	/**
	 * Find repeated extensions.
	 *
	 * @return the list
	 * @throws BusinessException the business exception
	 */
	List<Object[]> findRepeatedExtensions() throws BusinessException;

	/**
	 * Find all fs extensions.
	 *
	 * @return the list
	 * @throws BusinessException the business exception
	 */
	List<FsExtension> findAllFsExtensions() throws BusinessException;	

}
