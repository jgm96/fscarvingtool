package business.impl;

import model.exception.BusinessException;

/**
 * The Interface Command.
 */
public interface Command {

	/**
	 * Execute.
	 *
	 * @return the object
	 * @throws BusinessException the business exception
	 */
	Object execute() throws BusinessException;

}
