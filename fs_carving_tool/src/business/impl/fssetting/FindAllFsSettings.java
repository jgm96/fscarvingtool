package business.impl.fssetting;

import java.util.List;

import business.impl.Command;
import model.FsSetting;
import model.exception.BusinessException;
import persistence.FsSettingFinder;


/**
 * The Class FindAllFsSettings.
 */
public class FindAllFsSettings implements Command {

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		List<FsSetting> fsSettings = FsSettingFinder.findAll();
		checkNotNull(fsSettings);
		return fsSettings;
	}
	
	/**
	 * Check not null.
	 *
	 * @param fsSettings the fs settings
	 * @throws BusinessException the business exception
	 */
	private void checkNotNull(List<FsSetting> fsSettings) throws BusinessException {
		if (fsSettings == null) {
			throw new BusinessException("FsExtension can't be deleted because it does not exist.");
		}
	}

}
