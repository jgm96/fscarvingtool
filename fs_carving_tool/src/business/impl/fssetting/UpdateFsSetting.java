package business.impl.fssetting;

import business.impl.Command;
import model.exception.BusinessException;
import persistence.FsSettingFinder;


/**
 * The Class UpdateFsSetting.
 */
public class UpdateFsSetting implements Command {

	/** The name. */
	private String name;
	
	/** The value. */
	private String value;

	/**
	 * Instantiates a new update fs setting.
	 *
	 * @param name the name
	 * @param value the value
	 */
	public UpdateFsSetting(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		return FsSettingFinder.updateFsSetting(name, value);
	}

}
