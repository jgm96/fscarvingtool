package business.impl.fsextension;

import java.util.List;

import business.impl.Command;
import model.exception.BusinessException;
import persistence.FsExtensionFinder;


/**
 * The Class FindRepeatedExtensions.
 */
public class FindRepeatedExtensions implements Command {

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		List<Object[]> fsExtensions = FsExtensionFinder.findRepeatedExtensions();		
		return fsExtensions;
	}	

}	
