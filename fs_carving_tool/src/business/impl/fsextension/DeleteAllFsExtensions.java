package business.impl.fsextension;

import business.impl.Command;
import model.exception.BusinessException;
import persistence.FsExtensionFinder;


/**
 * The Class DeleteAllFsExtensions.
 */
public class DeleteAllFsExtensions implements Command {

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		int rowsDeleted = FsExtensionFinder.deleteAll();
		return rowsDeleted;
	}

}
