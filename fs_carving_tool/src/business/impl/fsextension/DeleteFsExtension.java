package business.impl.fsextension;

import business.impl.Command;
import model.FsExtension;
import model.exception.BusinessException;
import persistence.util.Jpa;


/**
 * The Class DeleteFsExtension.
 */
public class DeleteFsExtension implements Command {

	/** The id fs extension. */
	private Long idFsExtension;

	/**
	 * Instantiates a new delete fs extension.
	 *
	 * @param idFsExtension the id fs extension
	 */
	public DeleteFsExtension(Long idFsExtension) {
		this.idFsExtension = idFsExtension;
	}

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		FsExtension f = Jpa.getManager().find(FsExtension.class, idFsExtension);
		checkNotNull(f);

		Jpa.getManager().remove(f);
		return null;
	}

	/**
	 * Check not null.
	 *
	 * @param f the f
	 * @throws BusinessException the business exception
	 */
	private void checkNotNull(FsExtension f) throws BusinessException {
		if (f == null) {
			throw new BusinessException("FsExtension can't be deleted because it does not exist.");
		}
	}

}
