package business.impl.fsextension;

import business.impl.Command;
import model.FsExtension;
import model.exception.BusinessException;
import persistence.util.Jpa;


/**
 * The Class AddFsExtension.
 */
public class AddFsExtension implements Command {

	/** The fs extension. */
	private FsExtension fsExtension;

	/**
	 * Instantiates a new adds the fs extension.
	 *
	 * @param fsExtension the fs extension
	 */
	public AddFsExtension(FsExtension fsExtension) {
		this.fsExtension = fsExtension;
	}

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		Jpa.getManager().persist(fsExtension);
		return fsExtension;
	}

}
