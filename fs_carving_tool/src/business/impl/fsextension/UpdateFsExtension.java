package business.impl.fsextension;

import business.impl.Command;
import model.FsExtension;
import model.exception.BusinessException;
import persistence.util.Jpa;


/**
 * The Class UpdateFsExtension.
 */
public class UpdateFsExtension implements Command {

	/** The fs extension. */
	private FsExtension fsExtension;

	/**
	 * Instantiates a new update fs extension.
	 *
	 * @param fsExtension the fs extension
	 */
	public UpdateFsExtension(FsExtension fsExtension) {
		this.fsExtension = fsExtension;
	}

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		return Jpa.getManager().merge(fsExtension);
	}

}
