package business.impl.fsextension;

import java.util.List;

import business.impl.Command;
import model.FsExtension;
import model.exception.BusinessException;
import persistence.FsExtensionFinder;


/**
 * The Class FindAllFsExtensions.
 */
public class FindAllFsExtensions implements Command {

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		List<FsExtension> FsExtensiones = FsExtensionFinder.findAll();
		return FsExtensiones;
	}

}
