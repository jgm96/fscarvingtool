package business.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import model.exception.BusinessException;
import persistence.util.Jpa;

/**
 * The Class CommandExecutor.
 */
public class CommandExecutor {

	/**
	 * Execute.
	 *
	 * @param cmd the cmd
	 * @return the object
	 * @throws BusinessException the business exception
	 */
	public Object execute(Command cmd) throws BusinessException {
		EntityManager mapper = Jpa.createEntityManager();
		EntityTransaction trx = mapper.getTransaction();
		trx.begin();

		try {
			Object res = cmd.execute();

			trx.commit();

			return res;

		} catch (PersistenceException | BusinessException e) {
			if (trx.isActive())
				trx.rollback();
			throw e;
		} finally {
			if (mapper.isOpen())
				mapper.close();
		}
	}
	

}
