package business.impl.fscase;

import business.impl.Command;
import model.FsCase;
import model.exception.BusinessException;
import persistence.util.Jpa;


/**
 * The Class AddFsCase.
 */
public class AddFsCase implements Command {

	/** The fs case. */
	private FsCase fsCase;

	/**
	 * Instantiates a new adds the fs case.
	 *
	 * @param fsCase the fs case
	 */
	public AddFsCase(FsCase fsCase) {
		this.fsCase = fsCase;
	}

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		Jpa.getManager().persist(fsCase);
		return fsCase;
	}

}
