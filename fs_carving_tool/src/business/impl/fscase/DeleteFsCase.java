package business.impl.fscase;

import business.impl.Command;
import model.FsCase;
import model.exception.BusinessException;
import persistence.util.Jpa;


/**
 * The Class DeleteFsCase.
 */
public class DeleteFsCase implements Command {

	/** The id fs case. */
	private Long idFsCase;

	/**
	 * Instantiates a new delete fs case.
	 *
	 * @param idFsCase the id fs case
	 */
	public DeleteFsCase(Long idFsCase) {
		super();
		this.idFsCase = idFsCase;
	}

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		FsCase f = Jpa.getManager().find(FsCase.class, idFsCase);
		checkNotNull(f);

		Jpa.getManager().remove(f);
		return null;
	}

	/**
	 * Check not null.
	 *
	 * @param f the f
	 * @throws BusinessException the business exception
	 */
	private void checkNotNull(FsCase f) throws BusinessException {
		if (f == null) {
			throw new BusinessException("FsCase can't be deleted because it does not exist.");
		}
	}

}
