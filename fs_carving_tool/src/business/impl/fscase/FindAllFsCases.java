package business.impl.fscase;

import java.util.List;

import business.impl.Command;
import model.FsCase;
import model.exception.BusinessException;
import persistence.FsCaseFinder;


/**
 * The Class FindAllFsCases.
 */
public class FindAllFsCases implements Command {

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		List<FsCase> fsCases = FsCaseFinder.findAll();
		return fsCases;
	}

}
