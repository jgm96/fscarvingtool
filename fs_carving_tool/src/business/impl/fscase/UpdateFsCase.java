package business.impl.fscase;

import business.impl.Command;
import model.FsCase;
import model.exception.BusinessException;
import persistence.util.Jpa;


/**
 * The Class UpdateFsCase.
 */
public class UpdateFsCase implements Command {

	/** The fs case. */
	private FsCase fsCase;

	/**
	 * Instantiates a new update fs case.
	 *
	 * @param fsCase the fs case
	 */
	public UpdateFsCase(FsCase fsCase) {
		this.fsCase = fsCase;
	}

	/* (non-Javadoc)
	 * @see business.impl.Command#execute()
	 */
	@Override
	public Object execute() throws BusinessException {
		return Jpa.getManager().merge(fsCase);
	}

}
