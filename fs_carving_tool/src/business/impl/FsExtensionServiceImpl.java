package business.impl;

import java.util.List;

import business.FsExtensionService;
import business.impl.fsextension.AddFsExtension;
import business.impl.fsextension.DeleteAllFsExtensions;
import business.impl.fsextension.DeleteFsExtension;
import business.impl.fsextension.FindAllFsExtensions;
import business.impl.fsextension.FindRepeatedExtensions;
import business.impl.fsextension.UpdateFsExtension;
import model.FsExtension;
import model.exception.BusinessException;


/**
 * The Class FsExtensionServiceImpl.
 */
public class FsExtensionServiceImpl implements FsExtensionService {

	/** The executor. */
	private CommandExecutor executor = new CommandExecutor();

	/* (non-Javadoc)
	 * @see business.FsExtensionService#newFsExtension(model.FsExtension)
	 */
	@Override
	public void newFsExtension(FsExtension fsExtension) throws BusinessException {
		executor.execute(new AddFsExtension(fsExtension));
	}

	/* (non-Javadoc)
	 * @see business.FsExtensionService#deleteFsExtension(java.lang.Long)
	 */
	@Override
	public void deleteFsExtension(Long idFsExtension) throws BusinessException {
		executor.execute(new DeleteFsExtension(idFsExtension));
	}

	/* (non-Javadoc)
	 * @see business.FsExtensionService#updateFsExtension(model.FsExtension)
	 */
	@Override
	public void updateFsExtension(FsExtension fsExtension) throws BusinessException {
		executor.execute(new UpdateFsExtension(fsExtension));
	}

	/* (non-Javadoc)
	 * @see business.FsExtensionService#findAllFsExtensions()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FsExtension> findAllFsExtensions() throws BusinessException {
		return (List<FsExtension>) executor.execute(new FindAllFsExtensions());
	}

	/* (non-Javadoc)
	 * @see business.FsExtensionService#findRepeatedExtensions()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findRepeatedExtensions() throws BusinessException {
		return (List<Object[]>) executor.execute(new FindRepeatedExtensions());
	}

	/* (non-Javadoc)
	 * @see business.FsExtensionService#deleteAllFsExtensions()
	 */
	@Override
	public int deleteAllFsExtensions() throws BusinessException {
		return (int) executor.execute(new DeleteAllFsExtensions());
	}	

}
