package business.impl;

import java.util.List;

import business.FsCaseService;
import business.impl.fscase.AddFsCase;
import business.impl.fscase.DeleteFsCase;
import business.impl.fscase.FindAllFsCases;
import business.impl.fscase.UpdateFsCase;
import model.FsCase;
import model.exception.BusinessException;

public class FsCaseServiceImpl implements FsCaseService {

	private CommandExecutor executor = new CommandExecutor();

	@Override
	public void newFsCase(FsCase fsCase) throws BusinessException {
		executor.execute(new AddFsCase(fsCase));

	}
	

	@Override
	public void deleteFsCase(Long id) throws BusinessException {
		executor.execute(new DeleteFsCase(id));
	}

	@Override
	public void updateFsCase(FsCase fsCase) throws BusinessException {
		executor.execute(new UpdateFsCase(fsCase));
	}

	@Override
	public FsCase findFsCaseById(Long id) throws BusinessException {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FsCase> findAllFsCases() throws BusinessException {
		return (List<FsCase>) executor.execute(new FindAllFsCases());
	}

}
