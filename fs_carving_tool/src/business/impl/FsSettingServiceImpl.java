 package business.impl;

import java.util.List;

import business.FsSettingService;
import business.impl.fssetting.FindAllFsSettings;
import business.impl.fssetting.UpdateFsSetting;
import model.FsSetting;
import model.exception.BusinessException;

/**
 * The Class FsSettingServiceImpl.
 */
public class FsSettingServiceImpl implements FsSettingService{
	
	/** The executor. */
	private CommandExecutor executor = new CommandExecutor();
	
	/* (non-Javadoc)
	 * @see business.FsSettingService#updateFsSetting(java.lang.String, java.lang.String)
	 */
	@Override
	public void updateFsSetting(String name, String value) throws BusinessException {
		executor.execute(new UpdateFsSetting(name, value));
	}

	/* (non-Javadoc)
	 * @see business.FsSettingService#findAllFsSettings()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FsSetting> findAllFsSettings() throws BusinessException {
		return (List<FsSetting>) executor.execute(new FindAllFsSettings());
	}

}
