package business;

import java.util.List;

import model.FsCase;
import model.exception.BusinessException;

/**
 * The Interface FsCaseService.
 */
public interface FsCaseService {

	/**
	 * New fs case.
	 *
	 * @param fsCase the fs case
	 * @throws BusinessException the business exception
	 */
	void newFsCase(FsCase fsCase) throws BusinessException;

	/**
	 * Delete fs case.
	 *
	 * @param id the id
	 * @throws BusinessException the business exception
	 */
	void deleteFsCase(Long id) throws BusinessException;

	/**
	 * Update fs case.
	 *
	 * @param fsCase the fs case
	 * @throws BusinessException the business exception
	 */
	void updateFsCase(FsCase fsCase) throws BusinessException;

	/**
	 * Find fs case by id.
	 *
	 * @param id the id
	 * @return the fs case
	 * @throws BusinessException the business exception
	 */
	FsCase findFsCaseById(Long id) throws BusinessException;

	/**
	 * Find all fs cases.
	 *
	 * @return the list
	 * @throws BusinessException the business exception
	 */
	List<FsCase> findAllFsCases() throws BusinessException;

}
