package util;

import model.exception.PresentationException;


/**
 * The Class HexUtil.
 */
public class HexUtil {

	/**
	 * Hex string to byte array.
	 *
	 * @param s the s
	 * @return the byte[]
	 * @throws PresentationException the presentation exception
	 */
	public static byte[] hexStringToByteArray(String s) throws PresentationException {
		String processeds = s.replaceAll("\\s+", "").replaceAll("-", "");
		if ((processeds.length() & 1) != 0) {
			throw new PresentationException("Error. Incorrect Hex format.");
		}
		int len = processeds.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(processeds.charAt(i), 16) << 4)
					+ Character.digit(processeds.charAt(i + 1), 16));
		}
		return data;
	}

	/**
	 * Bytes to hex.
	 *
	 * @param byteArray the byte array
	 * @return the string
	 */
	public static String bytesToHex(byte[] byteArray) {
		StringBuilder sb = new StringBuilder();
		for (byte b : byteArray) {
			sb.append(String.format("%02X ", b));
		}
		return sb.toString().trim();
	}

	/**
	 * Check hex format size.
	 *
	 * @param hex the hex
	 * @return true, if successful
	 */
	public static boolean checkHexFormatSize(String hex) {
		return hex.replaceAll("\\s+", "").replaceAll("-", "").length() % 2 == 0;
	}

}
