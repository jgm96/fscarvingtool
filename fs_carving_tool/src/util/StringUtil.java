package util;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.FsCase;
import model.FsExtension;
import ui.gui.Messages;


/**
 * The Class StringUtil.
 */
public class StringUtil {
	
	/** The Constant separator. */
	public static final String separator = "===========================================\n";
	
	/** The Constant newLine. */
	public static final String newLine = "\n";
	
	/** The Constant sDots. */
	public static final String sDots = "...";
	
	/** The Constant htmlTag_open. */
	public static final String htmlTag_open = "<html>";
	
	/** The Constant htmlTag_open_style. */
	public static final String htmlTag_open_style = "<html style=\"";
	
	/** The Constant htmlTag_close. */
	public static final String htmlTag_close = "</html>";
	
	/** The Constant boldTag_close. */
	public static final String boldTag_close = "</b>";
	
	/** The Constant boldTag_open. */
	public static final String boldTag_open = "<b>";
	
	/** The Constant htmlTag_lineBreak. */
	public static final String htmlTag_lineBreak = "<br>";
	
	/**
	 * Generate case header.
	 *
	 * @param fsCase the fs case
	 * @return the string
	 */
	public static String generateCaseHeader(FsCase fsCase) {
		StringBuilder sb = new StringBuilder();
		sb.append("FS FORENSICS v1.0").append(newLine);
		sb.append("Current case: ").append(fsCase.getname()).append(newLine);
		sb.append("- Organization: ").append(fsCase.getOrganization()).append(newLine);
		sb.append("- Investigator: ").append(fsCase.getInvestigator()).append(newLine);
		sb.append("- Contact Details: ").append(fsCase.getContactDetails()).append(newLine);
		sb.append("- Case Date: ").append(fsCase.getCaseDate()).append(newLine);
		sb.append("- Case folder: ").append(fsCase.getCaseFolder()).append(newLine);		
		sb.append(separator);
		return sb.toString();
	}

	/**
	 * Generate file header.
	 *
	 * @param file the file
	 * @return the string
	 */
	public static String generateFileHeader(File file) {
		StringBuilder sb = new StringBuilder();
		sb.append("Current file: ").append(newLine);
		sb.append("- Filename: ").append(file.getName()).append(newLine);
		sb.append("- File size: ").append(file.length()/ 1024).append(" KB").append(newLine);
		sb.append("- File path: ").append(file.getAbsoluteFile()).append(newLine);
		return sb.toString();
	}	
	
	/**
	 * Generate extension header.
	 *
	 * @param extension the extension
	 * @return the string
	 */
	public static String generateExtensionHeader(FsExtension extension) {
		StringBuilder sb = new StringBuilder();
		sb.append("File extension: ").append(extension.getExtension()).append(newLine);
		sb.append("- Description: ").append(extension.getDescription()).append(newLine);
		sb.append("- Category: ").append(extension.getCategory()).append(newLine);
		sb.append(" - Offset leading: ").append(extension.getOffsetLeading()).append(newLine);
		sb.append(" - File signature: ").append(HexUtil.bytesToHex(extension.getFileSignature())).append(newLine);
		if (extension.getTrailer() != null) {
			sb.append(" - Offset trailer: ").append(extension.getOffsetTrailer()).append(newLine);
			sb.append(" - Trailer: ").append(HexUtil.bytesToHex(extension.getTrailer())).append(newLine);
		}
		return sb.toString();
	}	
	
	/**
	 * Convert date to fancy string.
	 *
	 * @param date the date
	 * @return the string
	 */
	public static String convertDateToFancyString(Date date) {
		String pattern = Messages.getString("LoadCasesWindow.dateFormat.text"); //$NON-NLS-1$
		DateFormat df = new SimpleDateFormat(pattern);
		return df.format(date);
	}
	
	/**
	 * HTM L to bold.
	 *
	 * @param s the s
	 * @return the string
	 */
	public static String HTML_ToBold(String s) {
		StringBuilder sb = new StringBuilder();
		sb.append(boldTag_open).append(newLine).append(s).append(boldTag_close);
		return sb.toString();
	}
	
	/**
	 * To HTML.
	 *
	 * @param s the s
	 * @param style the style
	 * @return the string
	 */
	public static String toHTML(String s, String style) {
		StringBuilder sb = new StringBuilder();
		sb.append(htmlTag_open).append("<head><style>").append("body {").append(style).append("} </style></head><body>").append(s).append("</body>").append(htmlTag_close);
		return sb.toString();
	}
	
	/**
	 * HTM L to red.
	 *
	 * @param s the s
	 * @return the string
	 */
	public static String HTML_ToRed(String s) {
		StringBuilder sb = new StringBuilder();
		sb.append("<span style=\"color:#FF0000\">").append(s).append("</span>");
		return sb.toString();
	}
	
	/**
	 * HTM L to green.
	 *
	 * @param s the s
	 * @return the string
	 */
	public static String HTML_ToGreen(String s) {
		StringBuilder sb = new StringBuilder();
		sb.append("<span style=\"color:#008000\">").append(s).append("</span>");
		return sb.toString();
	}

}
