package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The Class FsExtension.
 *
 * @author Julián García Murias
 * 
 */
@Entity
@Table(name = "FsExtensions")
public class FsExtension {

	/**
	 * Instantiates a new fs extension.
	 */
	FsExtension() {
	}

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** The extension. */
	@Column(nullable = false)
	private String extension;

	/** The description. */
	private String description;

	/** The file signature. */
	private byte[] fileSignature;

	/** The leading offsetLeading. */
	@Column(name = "OFFSET_LEADING")
	private int offsetLeading;

	/** The trailer. */
	private byte[] trailer;

	/** The trailer offsetLeading. */
	@Column(name = "OFFSET_TRAILER")
	private int offsetTrailer;

	/** The category. */
	private String category;

	/**
	 * Instantiates a new fs extension.
	 *
	 * @param extension     the extension
	 * @param description   the description
	 * @param fileSignature the file signature
	 * @param offsetLeading the offset leading
	 * @param trailer       the trailer
	 * @param offsetTrailer the offset trailer
	 * @param category      the category
	 */
	public FsExtension(String extension, String description, byte[] fileSignature, int offsetLeading, byte[] trailer,
			int offsetTrailer, String category) {
		super();
		this.extension = extension;
		this.description = description;
		this.fileSignature = fileSignature;
		this.offsetLeading = offsetLeading;
		this.trailer = trailer;
		this.offsetTrailer = offsetTrailer;
		this.category = category;
	}

	/**
	 * Instantiates a new fs extension.
	 *
	 * @param id            the id
	 * @param extension     the extension
	 * @param description   the description
	 * @param fileSignature the file signature
	 * @param offsetLeading the offset leading
	 */
	public FsExtension(Long id, String extension, String description, byte[] fileSignature, int offsetLeading) {
		super();
		this.id = id;
		this.extension = extension;
		this.description = description;
		this.fileSignature = fileSignature;
		this.offsetLeading = offsetLeading;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the extension.
	 *
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * Sets the extension.
	 *
	 * @param extension the new extension
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the file signature.
	 *
	 * @return the file signature
	 */
	public byte[] getFileSignature() {
		return fileSignature;
	}

	/**
	 * Sets the file signature.
	 *
	 * @param fileSignature the new file signature
	 */
	public void setFileSignature(byte[] fileSignature) {
		this.fileSignature = fileSignature;
	}

	/**
	 * Gets the trailer.
	 *
	 * @return the trailer
	 */
	public byte[] getTrailer() {
		return trailer;
	}

	/**
	 * Sets the trailer.
	 *
	 * @param trailer the new trailer
	 */
	public void setTrailer(byte[] trailer) {
		this.trailer = trailer;
	}
	
	/**
	 * Gets the offset leading.
	 *
	 * @return the offset leading
	 */
	public int getOffsetLeading() {
		return offsetLeading;
	}

	/**
	 * Sets the offset leading.
	 *
	 * @param offsetLeading the new offset leading
	 */
	public void setOffsetLeading(int offsetLeading) {
		this.offsetLeading = offsetLeading;
	}

	/**
	 * Gets the offset trailer.
	 *
	 * @return the offset trailer
	 */
	public int getOffsetTrailer() {
		return offsetTrailer;
	}

	/**
	 * Sets the offset trailer.
	 *
	 * @param offsetTrailer the new offset trailer
	 */
	public void setOffsetTrailer(int offsetTrailer) {
		this.offsetTrailer = offsetTrailer;
	}

	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 */
	public void setCategory(String category) {
		this.category = category;
	}	

}
