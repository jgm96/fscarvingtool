package model.factory;

import model.FsExtension;
import model.exception.PresentationException;
import util.HexUtil;


/**
 * A factory for creating FsExtensions objects.
 */
public class FsExtensionsFactory {

	/** The instance. */
	private static FsExtensionsFactory instance;

	/**
	 * Instantiates a new fs extensions factory.
	 */
	private FsExtensionsFactory() {

	}

	/**
	 * Gets the single instance of FsExtensionsFactory.
	 *
	 * @return single instance of FsExtensionsFactory
	 */
	public static FsExtensionsFactory getInstance() {
		if (instance == null) {
			instance = new FsExtensionsFactory();
		}
		return instance;
	}

	/**
	 * Creates a new FsExtensions object.
	 *
	 * @param extension the extension
	 * @param description the description
	 * @param fileSignature the file signature
	 * @param offset the offset
	 * @param trailer the trailer
	 * @param trailerOffset the trailer offset
	 * @param category the category
	 * @return the fs extension
	 * @throws PresentationException the presentation exception
	 */
	public FsExtension createFsExtension(String extension, String description, String fileSignature, String offset,
			String trailer, String trailerOffset, String category) throws PresentationException {
		extension = extension.trim();
		description = description.trim();
		fileSignature = fileSignature.replaceAll("\\s+", "");
		trailerOffset.trim();
		offset = offset.trim();
		trailer = trailer.replaceAll("\\s+", "");

		int intOffset = !offset.equals("") ? Integer.valueOf(offset) : 0;
		int intTrailerOffset = !trailerOffset.equals("") ? Integer.valueOf(trailerOffset) : 0;

		checkParameters(extension, description);

		FsExtension fsExtension;

		try {
			fsExtension = new FsExtension(extension, description,
					!fileSignature.isEmpty() ? HexUtil.hexStringToByteArray(fileSignature) : null, intOffset,
					!trailer.isEmpty() ? HexUtil.hexStringToByteArray(trailer) : null, intTrailerOffset, category);

		} catch (Exception e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}

		return fsExtension;
	}

	/**
	 * Update fs extension.
	 *
	 * @param fsExtension the fs extension
	 * @param extension the extension
	 * @param description the description
	 * @param fileSignature the file signature
	 * @param offset the offset
	 * @param trailer the trailer
	 * @param trailerOffset the trailer offset
	 * @param category the category
	 * @return the fs extension
	 * @throws PresentationException the presentation exception
	 */
	public FsExtension updateFsExtension(FsExtension fsExtension, String extension, String description,
			String fileSignature, String offset, String trailer, String trailerOffset, String category)
			throws PresentationException {
		extension = extension.trim();
		description = description.trim();
		fileSignature = fileSignature.replaceAll("\\s+", "");
		trailerOffset = trailerOffset.trim();
		offset = offset.trim();
		trailer = trailer.replaceAll("\\s+", "");

		int intOffset = !offset.equals("") ? Integer.valueOf(offset) : 0;
		int intTrailerOffset = !trailerOffset.equals("") ? Integer.valueOf(trailerOffset) : 0;

		checkParameters(extension, description);

		try {
			fsExtension.setExtension(extension);
			fsExtension.setDescription(description);
			fsExtension.setFileSignature(!fileSignature.isEmpty() ? HexUtil.hexStringToByteArray(fileSignature) : null);
			fsExtension.setTrailer(!trailer.isEmpty() ? HexUtil.hexStringToByteArray(trailer) : null);
			fsExtension.setOffsetLeading(intOffset);
			fsExtension.setOffsetTrailer(intTrailerOffset);
			fsExtension.setCategory(category);

		} catch (Exception e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}

		return fsExtension;

	}

	/**
	 * Check parameters.
	 *
	 * @param extension the extension
	 * @param description the description
	 * @throws PresentationException the presentation exception
	 */
	private void checkParameters(String extension, String description) throws PresentationException {
		if (extension.isEmpty()) {
			throw new PresentationException("Error. Extension must have value.");
		} else if (description.isEmpty()) {
			throw new PresentationException("Error. Description must have value.");
		}
	}

}
