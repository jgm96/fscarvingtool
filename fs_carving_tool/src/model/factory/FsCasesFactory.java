package model.factory;

import java.util.Date;

import model.FsCase;


/**
 * A factory for creating FsCases objects.
 */
public class FsCasesFactory {

	/** The instance. */
	private static FsCasesFactory instance;

	/**
	 * Instantiates a new fs cases factory.
	 */
	private FsCasesFactory() {

	}

	/**
	 * Gets the single instance of FsCasesFactory.
	 *
	 * @return single instance of FsCasesFactory
	 */
	public static FsCasesFactory getInstance() {
		if (instance == null) {
			instance = new FsCasesFactory();
		}
		return instance;
	}

	/**
	 * Creates a new FsCases object.
	 *
	 * @param name the name
	 * @param investigator the investigator
	 * @param organization the organization
	 * @param contactDetails the contact details
	 * @param caseDate the case date
	 * @param description the description
	 * @param caseFolder the case folder
	 * @param logCaseActivity the log case activity
	 * @return the fs case
	 */
	public FsCase createFsCase(String name, String investigator, String organization, String contactDetails,
			Date caseDate, String description, String caseFolder, boolean logCaseActivity) {
		return new FsCase(name, investigator, organization, contactDetails, caseDate, description, caseFolder, logCaseActivity);
	}

}
