package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The Class FsCase.
 *
 * @author Julián García Murias
 * 
 */
@Entity
@Table(name = "FsCases")
public class FsCase {

	/**
	 * Instantiates a new fs case.
	 */
	FsCase() {
	}

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** The name. */
	@Column(unique = true, nullable = false)
	private String name;

	/** The investigator. */
	private String investigator;

	/** The organization. */
	private String organization;
	
	/** The description. */
	private String description;

	/** The contact details. */
	@Column(name = "contact_details")
	private String contactDetails;

	/** The start date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "case_date")
	private Date caseDate;

	/** The case folder. */
	@Column(name = "folder")
	private String caseFolder;
	
	/** The log case activity. */
	@Column(name = "enabled_log")
	private boolean logCaseActivity;

	/**
	 * Instantiates a new fs case.
	 *
	 * @param name the name
	 * @param investigator the investigator
	 * @param organization the organization
	 * @param contactDetails the contact details
	 * @param caseDate the case date
	 * @param description case short description
	 * @param caseFolder the case folder
	 * @param logCaseActivity the log case activity
	 */
	public FsCase(String name, String investigator, String organization, String contactDetails, Date caseDate,
			String description, String caseFolder, boolean logCaseActivity) {
		super();
		this.name = name;
		this.investigator = investigator;
		this.organization = organization;
		this.contactDetails = contactDetails;
		this.caseDate = caseDate;
		this.description = description;
		this.caseFolder = caseFolder;
		this.logCaseActivity = logCaseActivity;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getname() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setname(String name) {
		this.name = name;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getCaseDate() {
		return caseDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param caseDate the new case date
	 */
	public void setCaseDate(Date caseDate) {
		this.caseDate = caseDate;
	}

	/**
	 * Gets the investigator.
	 *
	 * @return the investigator
	 */
	public String getInvestigator() {
		return investigator;
	}

	/**
	 * Sets the investigator.
	 *
	 * @param investigator the new investigator
	 */
	public void setInvestigator(String investigator) {
		this.investigator = investigator;
	}

	/**
	 * Gets the organization.
	 *
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * Sets the organization.
	 *
	 * @param organization the new organization
	 */
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/**
	 * Gets the contact details.
	 *
	 * @return the contact details
	 */
	public String getContactDetails() {
		return contactDetails;
	}

	/**
	 * Sets the contact details.
	 *
	 * @param contactDetails the new contact details
	 */
	public void setContactDetails(String contactDetails) {
		this.contactDetails = contactDetails;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the case folder.
	 *
	 * @return the case folder
	 */
	public String getCaseFolder() {
		return caseFolder;
	}

	/**
	 * Sets the case folder.
	 *
	 * @param caseFolder the new case folder
	 */
	public void setCaseFolder(String caseFolder) {
		this.caseFolder = caseFolder;
	}
	
	/**
	 * Checks if is log case activity.
	 *
	 * @return true, if is log case activity
	 */
	public boolean isLogCaseActivity() {
		return logCaseActivity;
	}

	/**
	 * Sets the log case activity.
	 *
	 * @param logCaseActivity the new log case activity
	 */
	public void setLogCaseActivity(boolean logCaseActivity) {
		this.logCaseActivity = logCaseActivity;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FsCase [id=" + id + ", name=" + name + ", investigator=" + investigator + ", organization="
				+ organization + ", contactDetails=" + contactDetails + ", caseDate=" + caseDate + ", caseFolder="
				+ caseFolder + ", logCaseActivity=" + logCaseActivity + "]";
	}	
	

}
