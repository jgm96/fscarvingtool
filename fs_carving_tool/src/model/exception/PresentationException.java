package model.exception;


/**
 * The Class PresentationException.
 */
public class PresentationException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6193644331843268598L;

	/**
	 * Instantiates a new presentation exception.
	 */
	public PresentationException() {
		super();
	}

	/**
	 * Instantiates a new presentation exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public PresentationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new presentation exception.
	 *
	 * @param message the message
	 */
	public PresentationException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new presentation exception.
	 *
	 * @param cause the cause
	 */
	public PresentationException(Throwable cause) {
		super(cause);
	}

}
