package logic.manager;

import java.util.ArrayList;
import java.util.List;

import business.FsExtensionService;
import conf.ServicesFactory;
import model.FsExtension;
import model.exception.BusinessException;
import model.exception.PresentationException;
import util.HexUtil;

/**
 * The Class FsExtensionManager.
 */
public class FsExtensionManager {

	/** The fs extensions. */
	private List<FsExtension> fsExtensions = new ArrayList<FsExtension>();

	/** The fs. */
	private FsExtensionService fs = ServicesFactory.getFsExtensionService();

	/**
	 * Instantiates a new fs extension manager.
	 */
	public FsExtensionManager() {
	}

	/**
	 * Gets the fs extensions.
	 *
	 * @return the fs extensions
	 */
	public List<FsExtension> getFsExtensions() {
		return fsExtensions;
	}

	/**
	 * Creates the fs extensions.
	 *
	 * @param fsExtensions the fs extensions
	 * @throws PresentationException the presentation exception
	 */
	public void createFsExtensions(List<FsExtension> fsExtensions) throws PresentationException {
		this.fsExtensions = fsExtensions;
		for (FsExtension ext : fsExtensions) {
			try {
				fs.newFsExtension(ext);
			} catch (BusinessException e) {
				e.printStackTrace();
				throw new PresentationException(e);
			}
		}
	}

	/**
	 * New fs extension.
	 *
	 * @param fsExtension the fs extension
	 * @throws PresentationException the presentation exception
	 */
	public void newFsExtension(FsExtension fsExtension) throws PresentationException {
		try {
			fs.newFsExtension(fsExtension);
			fsExtensions.add(fsExtension);
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Delete fs extension.
	 *
	 * @param fsExtension the fs extension
	 * @throws PresentationException the presentation exception
	 */
	public void deleteFsExtension(FsExtension fsExtension) throws PresentationException {
		try {
			fs.deleteFsExtension(fsExtension.getId());
			fsExtensions.remove(fsExtension);
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Delete all fs extensions.
	 *
	 * @return the int
	 * @throws PresentationException the presentation exception
	 */
	public int deleteAllFsExtensions() throws PresentationException {
		try {
			int rows = fs.deleteAllFsExtensions();
			fsExtensions.clear();
			return rows;
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Update fs extension.
	 *
	 * @param fsExtensionOld the fs extension old
	 * @param fsExtensionNew the fs extension new
	 * @throws PresentationException the presentation exception
	 */
	public void updateFsExtension(FsExtension fsExtensionOld, FsExtension fsExtensionNew) throws PresentationException {
		try {			
			fsExtensions.remove(fsExtensionOld);
			fs.updateFsExtension(fsExtensionNew);
			fsExtensions.add(fsExtensionNew);
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Find all fs extensions.
	 *
	 * @throws PresentationException the presentation exception
	 */
	public void findAllFsExtensions() throws PresentationException {
		try {
			fsExtensions = fs.findAllFsExtensions();
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}
	
	/**
	 * Gets the fs extension by name and file signature.
	 *
	 * @param name the name
	 * @param fileSignature the file signature
	 * @return the fs extension by name and file signature
	 * @throws PresentationException the presentation exception
	 */
	public FsExtension getFsExtensionByNameAndFileSignature(String name, String fileSignature) throws PresentationException {
		for(FsExtension fsExtension : fsExtensions) {
			if(fsExtension.getExtension().equalsIgnoreCase(name) && 
					HexUtil.bytesToHex(fsExtension.getFileSignature()).replaceAll("\\s","").equalsIgnoreCase(fileSignature.replaceAll("\\s",""))) {
				return fsExtension;
			}
		}
		return null;		
	}	
		
	/**
	 * Delete repeated extensions.
	 */
	public void deleteRepeatedExtensions() {
		try {			
			List<Object[]> repeatedFsExtensions = fs.findRepeatedExtensions();
			for(Object[] fsExtension : repeatedFsExtensions) {
				String name = (String)fsExtension[0];
				String fileSignature = HexUtil.bytesToHex((byte[])fsExtension[1]);				
				FsExtension ext = getFsExtensionByNameAndFileSignature(name, fileSignature);
				fs.deleteFsExtension(ext.getId());
				fsExtensions.remove(ext);
			}
		} catch (BusinessException | PresentationException e) {			
			e.printStackTrace();		
		}
	}
	
	/**
	 * Sort extensions.
	 *
	 * @param criteria the criteria
	 * @param pattern the pattern
	 * @return the list
	 */
	public List<FsExtension> sortExtensions(int criteria, String pattern){
		List<FsExtension> filteredExtensions = new ArrayList<FsExtension>();
		if(criteria == 0) {
			for(FsExtension ext : fsExtensions) {
				if(ext.getExtension().toLowerCase().startsWith(pattern.toLowerCase())) {
					filteredExtensions.add(ext);
				}
			}
		}
		else if(criteria == 1) {
			for(FsExtension ext : fsExtensions) {
				if(ext.getDescription().toLowerCase().startsWith(pattern.toLowerCase())) {
					filteredExtensions.add(ext);
				}
			}		
		}
		else if(criteria == 2){
			for(FsExtension ext : fsExtensions) {
				if(HexUtil.bytesToHex(ext.getFileSignature()).toLowerCase().startsWith(pattern.toLowerCase())) {
					filteredExtensions.add(ext);
				}
			}
		}
		else if(criteria == 3) {
			for(FsExtension ext : fsExtensions) {
				if(ext.getCategory().toLowerCase().startsWith(pattern.toLowerCase())) {
					filteredExtensions.add(ext);
				}
			}
		}
		return filteredExtensions;
	}

}
