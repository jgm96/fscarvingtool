package logic.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import business.FsCaseService;
import conf.ServicesFactory;
import model.FsCase;
import model.exception.BusinessException;
import model.exception.PresentationException;

/**
 * The Class FsCaseManager.
 */
public class FsCaseManager {

	/** The fs cases. */
	private List<FsCase> fsCases = new ArrayList<FsCase>();

	/** The fs. */
	private FsCaseService fs = ServicesFactory.getFsCaseService();

	/**
	 * Instantiates a new fs case manager.
	 */
	public FsCaseManager() {
	}

	/**
	 * Gets the fs cases.
	 *
	 * @return the fs cases
	 */
	public List<FsCase> getFsCases() {
		return fsCases;
	}

	/**
	 * Createfs cases.
	 *
	 * @param fsCases the fs cases
	 * @throws PresentationException the presentation exception
	 */
	public void createfsCases(List<FsCase> fsCases) throws PresentationException {
		this.fsCases = fsCases;
		for (FsCase ext : fsCases) {
			try {
				fs.newFsCase(ext);
			} catch (BusinessException e) {
				e.printStackTrace();
				throw new PresentationException(e);
			}
		}
	}

	/**
	 * New fs case.
	 *
	 * @param FsCase the fs case
	 * @throws PresentationException the presentation exception
	 */
	public void newFsCase(FsCase FsCase) throws PresentationException {
		try {
			fs.newFsCase(FsCase);
			fsCases.add(FsCase);
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Delete fs case.
	 *
	 * @param FsCase the fs case
	 * @throws PresentationException the presentation exception
	 */
	public void deleteFsCase(FsCase FsCase) throws PresentationException {
		try {
			fs.deleteFsCase(FsCase.getId());
			fsCases.remove(FsCase);
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Update fs case.
	 *
	 * @param FsCase the fs case
	 * @throws PresentationException the presentation exception
	 */
	public void updateFsCase(FsCase FsCase) throws PresentationException {
		try {
			fs.updateFsCase(FsCase);
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Find all fs cases.
	 *
	 * @throws PresentationException the presentation exception
	 */
	public void findAllFsCases() throws PresentationException {
		try {
			fsCases = fs.findAllFsCases();
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Filter cases.
	 *
	 * @param caseName the case name
	 * @param caseInvestigator the case investigator
	 * @param caseOrganization the case organization
	 * @param caseDescription the case description
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param logicMask the logic mask
	 * @return the list
	 */
	public List<FsCase> filterCases(String caseName, String caseInvestigator, String caseOrganization, 
			String caseDescription, Date minDate, Date maxDate, int logicMask) {
		List<FsCase> filtered = new ArrayList<FsCase>();		
		for(FsCase fsCase : fsCases) {
			if(logicMask == 0) {
				if((caseName.isEmpty() ? true : fsCase.getname().toLowerCase().contains(caseName.toLowerCase())) &&
						(caseInvestigator.isEmpty() ? true : fsCase.getInvestigator().toLowerCase().contains(caseInvestigator.toLowerCase())) &&
								(caseOrganization.isEmpty() ? true : fsCase.getOrganization().toLowerCase().contains(caseOrganization.toLowerCase())) &&
										(caseDescription.isEmpty() ? true : fsCase.getDescription().toLowerCase().contains(caseDescription.toLowerCase())) &&
												(minDate == null ? true : fsCase.getCaseDate().after(minDate)) &&
														(maxDate == null ? true : fsCase.getCaseDate().before(maxDate))) {
					filtered.add(fsCase);
				}
			}
			else if(logicMask == 1) {
				if((caseName.isEmpty() ? true : fsCase.getname().toLowerCase().contains(caseName.toLowerCase())) ||
						(caseInvestigator.isEmpty() ? true : fsCase.getInvestigator().toLowerCase().contains(caseInvestigator.toLowerCase())) ||
								(caseOrganization.isEmpty() ? true : fsCase.getOrganization().toLowerCase().contains(caseOrganization.toLowerCase())) ||
										(caseDescription.isEmpty() ? true : fsCase.getDescription().toLowerCase().contains(caseDescription.toLowerCase())) ||
												(minDate == null ? true : fsCase.getCaseDate().after(minDate)) ||
														(maxDate == null ? true : fsCase.getCaseDate().before(maxDate))) {
					filtered.add(fsCase);
				}
			}
			
		}
		return filtered;
	}

}
