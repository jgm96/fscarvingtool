package logic.manager;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;

import business.FsSettingService;
import business.impl.FsSettingServiceImpl;
import model.FsSetting;
import model.exception.BusinessException;
import model.exception.PresentationException;
import util.StringUtil;

/**
 * The Class FsSettingManager.
 */
public class FsSettingManager {

	/** The settings. */
	private Map<String, String> settings = new HashMap<String, String>();

	/** The fs. */
	FsSettingService fs = new FsSettingServiceImpl();

	/** The imported fs signatures. */
	private boolean importedFsSignatures;	

	/**
	 * Instantiates a new fs setting manager.
	 */
	public FsSettingManager() {
		findAllFsSettings();
	}

	/**
	 * Gets the settings.
	 *
	 * @return the settings
	 */
	public Map<String, String> getSettings() {
		return settings;
	}

	/**
	 * Find all fs settings.
	 */
	private void findAllFsSettings() {
		List<FsSetting> settingsList = null;
		try {
			settingsList = fs.findAllFsSettings();
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		for (FsSetting s : settingsList) {
			settings.put(s.getName(), s.getValue());
		}
	}

	/**
	 * Update fs setting.
	 *
	 * @param name the name
	 * @param value the value
	 * @throws PresentationException the presentation exception
	 */
	public void updateFsSetting(String name, String value) throws PresentationException {
		try {
			settings.put(name, value);
			fs.updateFsSetting(name, value);
		} catch (BusinessException e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Checks if is imported fs signatures.
	 *
	 * @return true, if is imported fs signatures
	 */
	public boolean isImportedFsSignatures() {
		return importedFsSignatures;
	}

	/**
	 * Sets the imported fs signatures.
	 *
	 * @param importedFsSignatures the new imported fs signatures
	 * @throws PresentationException the presentation exception
	 */
	public void setImportedFsSignatures(boolean importedFsSignatures) throws PresentationException {
		this.importedFsSignatures = importedFsSignatures;
		if (importedFsSignatures) {
			updateFsSetting("importedSignatures", "yes");
			String date = StringUtil.convertDateToFancyString(Calendar.getInstance().getTime());
			
			updateFsSetting("signaturesVersion", date);
			try {
				updateFsSetting("signaturesHash", makeSHA1Hash(RandomStringUtils.random(10)));
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {				
				e.printStackTrace();
			}
		} else {
			updateFsSetting("importedSignatures", "no");
			updateFsSetting("signaturesVersion", "none");
			updateFsSetting("signaturesHash", "none");
		}

	}

	/**
	 * Make SHA 1 hash.
	 *
	 * @param input the input
	 * @return the string
	 * @throws NoSuchAlgorithmException the no such algorithm exception
	 * @throws UnsupportedEncodingException the unsupported encoding exception
	 */
	public String makeSHA1Hash(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.reset();
		byte[] buffer = input.getBytes("UTF-8");
		md.update(buffer);
		byte[] digest = md.digest();

		String hexStr = "";
		for (int i = 0; i < digest.length; i++) {
			hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
		}
		return hexStr;
	}

}
