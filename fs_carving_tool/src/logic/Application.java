package logic;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.SAXException;

import conf.FsLogger;
import logic.io.writer.FsFileWriter;
import logic.manager.FsCaseManager;
import logic.manager.FsExtensionManager;
import logic.manager.FsSettingManager;
import model.FsCase;
import model.exception.PresentationException;
import util.StringUtil;

/**
 * The Class Application.
 */
public class Application {

	/** The fs extension manager. */
	private FsExtensionManager fsExtensionManager;
	
	/** The fs case manager. */
	private FsCaseManager fsCaseManager;
	
	/** The fs setting manager. */
	private FsSettingManager fsSettingManager;
	
	/** The fs analyzer. */
	private FileSignatureAnalyzer fsAnalyzer;
	
	/** The current fs case. */
	private FsCase currentFsCase;
	
	/** The metadata current file. */
	private File metadataCurrentFile;
	
	/** The current metadata. */
	private Metadata currentMetadata;
	
	/** The file sig analysis current directory. */
	private File fileSigAnalysisCurrentDirectory;
	
	/** The rds query tool current directory. */
	private File rdsQueryToolCurrentDirectory;
	
	/** The logger. */
	private Logger logger = FsLogger.getInstance();
	
	/** The current fs report. */
	private String currentFsReport;	
	
	/** The rds query tool. */
	private RDSQueryTool rdsQueryTool;

	/**
	 * Instantiates a new application.
	 */
	public Application() {		
		fsCaseManager = new FsCaseManager();
		fsExtensionManager = new FsExtensionManager();
		fsSettingManager = new FsSettingManager();
		fsAnalyzer = new FileSignatureAnalyzer();
		fileSigAnalysisCurrentDirectory = SystemUtils.IS_OS_LINUX ? new File("/home/") : new File("C:/");
		rdsQueryToolCurrentDirectory = SystemUtils.IS_OS_LINUX ? new File("/home/") : new File("C:/");
		rdsQueryTool = new RDSQueryTool(this);
		Locale.setDefault(new Locale(fsSettingManager.getSettings().get("locale")));
		
		initialize();
	}
	
	/**
	 * Initialize.
	 */
	private void initialize() {
		try {
			fsExtensionManager.findAllFsExtensions();
			fsCaseManager.findAllFsCases();
		} catch (PresentationException e) {		
			e.printStackTrace();
		}		
	}	

	/**
	 * Sets the current metadata.
	 *
	 * @param currentMetadata the new current metadata
	 */
	public void setCurrentMetadata(Metadata currentMetadata) {
		this.currentMetadata = currentMetadata;
	}

	/**
	 * Gets the current metadata.
	 *
	 * @return the current metadata
	 */
	public Metadata getCurrentMetadata() {
		return currentMetadata;
	}

	/**
	 * Gets the fs extension manager.
	 *
	 * @return the fs extension manager
	 */
	public FsExtensionManager getFsExtensionManager() {
		return fsExtensionManager;
	}

	/**
	 * Gets the fs case manager.
	 *
	 * @return the fs case manager
	 */
	public FsCaseManager getFsCaseManager() {
		return fsCaseManager;
	}

	/**
	 * Gets the current fs case.
	 *
	 * @return the current fs case
	 */
	public FsCase getCurrentFsCase() {
		return currentFsCase;
	}

	/**
	 * Sets the current fs case.
	 *
	 * @param currentFsCase the new current fs case
	 */
	public void setCurrentFsCase(FsCase currentFsCase) {
		this.currentFsCase = currentFsCase;
	}

	/**
	 * Gets the metadata current file.
	 *
	 * @return the metadata current file
	 */
	public File getMetadataCurrentFile() {
		return metadataCurrentFile;
	}	

	/**
	 * Gets the current fs report.
	 *
	 * @return the current fs report
	 */
	public String getCurrentFsReport() {
		return currentFsReport;
	}

	/**
	 * Sets the current fs report.
	 *
	 * @param currentFsReport the new current fs report
	 */
	public void setCurrentFsReport(String currentFsReport) {
		this.currentFsReport = currentFsReport;
	}

	/**
	 * Sets the metadata current file.
	 *
	 * @param metadataCurrentFile the new metadata current file
	 */
	public void setMetadataCurrentFile(File metadataCurrentFile) {
		this.metadataCurrentFile = metadataCurrentFile;
	}	

	/**
	 * Gets the file sig analysis current directory.
	 *
	 * @return the file sig analysis current directory
	 */
	public File getFileSigAnalysisCurrentDirectory() {
		return fileSigAnalysisCurrentDirectory;
	}

	/**
	 * Sets the file sig analysis current directory.
	 *
	 * @param fileSigAnalysisCurrentDirectory the new file sig analysis current directory
	 */
	public void setFileSigAnalysisCurrentDirectory(File fileSigAnalysisCurrentDirectory) {
		this.fileSigAnalysisCurrentDirectory = fileSigAnalysisCurrentDirectory;
	}
	
	/**
	 * Rds query to current directory.
	 *
	 * @return the string
	 * @throws PresentationException the presentation exception
	 */
	public String rdsQueryToCurrentDirectory() throws PresentationException {
		try {
			return rdsQueryTool.analyseDirectory(rdsQueryToolCurrentDirectory);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new PresentationException(e);
		}
	}
	
	
	/**
	 * Analyze current directory.
	 *
	 * @param recursiveSearch the recursive search
	 * @param extractMetadata the extract metadata
	 * @param showDetailedOutput the show detailed output
	 * @return the string
	 * @throws PresentationException the presentation exception
	 */
	public String analyzeCurrentDirectory(boolean recursiveSearch, boolean extractMetadata, boolean showDetailedOutput) throws PresentationException{
		try {
			String report = fsAnalyzer.analyzeDirectory(fileSigAnalysisCurrentDirectory, 
					fsExtensionManager.getFsExtensions(), recursiveSearch, extractMetadata,  showDetailedOutput, fsSettingManager.getSettings());
			currentFsReport = fsAnalyzer.getReport().toString();
			return report;
		} catch (IOException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new PresentationException(e);
		}		
	}

	/**
	 * Export metadata to text file.
	 *
	 * @throws PresentationException the presentation exception
	 */
	public void exportMetadataToTextFile() throws PresentationException {
		if (metadataCurrentFile != null && currentMetadata != null) {
			try {
				String metadata = "";
				String [] names = currentMetadata.names();
				for (String name : names) {
					if(!name.equalsIgnoreCase("x-parsed-by"))
						metadata+= name +": " + currentMetadata.get(name) + "\n";
				}
				FsFileWriter.getInstance().exportMetadataToText(metadataCurrentFile, currentFsCase, metadata);
			} catch (IOException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				throw new PresentationException(e);
			}
		}
	}	

	/**
	 * Gets the metadata of current file.
	 *
	 * @return the metadata of current file
	 * @throws PresentationException the presentation exception
	 */
	public String getMetadataOfCurrentFile() throws PresentationException {
		try {
			currentMetadata = MetadataExtractor.getMetadataFromImage(metadataCurrentFile);
			StringBuilder sb = new StringBuilder();
			StringBuilder sblog = new StringBuilder();
			String [] names = currentMetadata.names();			
			for (String name : names) {
				if(!name.equalsIgnoreCase("x-parsed-by")) {
					sb.append(StringUtil.HTML_ToBold(name)).append(": ").append(currentMetadata.get(name)).append(StringUtil.htmlTag_lineBreak);
					if(currentFsCase.isLogCaseActivity()) {
						sblog.append(name).append(": ").append(currentMetadata.get(name)).append("\n");
					}
				}
			}			
			if (getCurrentFsCase().isLogCaseActivity()) {
				logger.info("Metadata obtained:");
				logger.info(sblog.toString());
			}
			return StringUtil.toHTML(sb.toString(),"font-family:Tahoma; font-size: 9px");
		} catch (IOException | TikaException | SAXException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new PresentationException(e);
		}
	}
	
	/**
	 * Export file signature analysis to text file.
	 *
	 * @throws PresentationException the presentation exception
	 */
	public void exportFileSignatureAnalysisToTextFile() throws PresentationException{
		if(currentFsReport!=null) {
			try {
				FsFileWriter.getInstance().exportFileSignatureAnalysisToText(fileSigAnalysisCurrentDirectory, currentFsCase, currentFsReport);
			} catch (IOException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				throw new PresentationException(e);
			}
		}
	}
	
	/**
	 * Export RDS query to text file.
	 *
	 * @throws PresentationException the presentation exception
	 */
	public void exportRDSQueryToTextFile() throws PresentationException{
		if(rdsQueryTool.getHashes() != null) {
			try {
				FsFileWriter.getInstance().exportRDSQueryToText(rdsQueryToolCurrentDirectory, currentFsCase, rdsQueryTool.getReport(rdsQueryToolCurrentDirectory));
			} catch (IOException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				throw new PresentationException(e);
			}
		}
	}
	
	/**
	 * Gets the fs settings manager.
	 *
	 * @return the fs settings manager
	 */
	public FsSettingManager getFsSettingsManager() {
		return fsSettingManager;
	}

	/**
	 * Gets the rds query tool current directory.
	 *
	 * @return the rds query tool current directory
	 */
	public File getRdsQueryToolCurrentDirectory() {
		return rdsQueryToolCurrentDirectory;
	}

	/**
	 * Sets the rds query tool current directory.
	 *
	 * @param rdsQueryToolCurrentDirectory the new rds query tool current directory
	 */
	public void setRdsQueryToolCurrentDirectory(File rdsQueryToolCurrentDirectory) {
		this.rdsQueryToolCurrentDirectory = rdsQueryToolCurrentDirectory;
	}

	/**
	 * Gets the rds query tool.
	 *
	 * @return the rds query tool
	 */
	public RDSQueryTool getRdsQueryTool() {
		return rdsQueryTool;
	}
	

}
