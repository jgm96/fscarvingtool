package logic.io.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.FsExtension;
import model.exception.PresentationException;
import model.factory.FsExtensionsFactory;

/**
 * The Class FileSigsGCK_Reader.
 */
public class FileSigsGCK_Reader {

	/** The instance. */
	private static FileSigsGCK_Reader instance;

	/** The br. */
	private BufferedReader br;

	/** The file description. */
	private String fileDescription;
	
	/** The file extension. */
	private String fileExtension;
	
	/** The offset. */
	private String offset;
	
	/** The file signature. */
	private String fileSignature;
	
	/** The category. */
	private String category;
	
	/** The trailer. */
	private String trailer;
	
	/** The trail offset. */
	private String trailOffset;

	/**
	 * Instantiates a new file sigs GC K reader.
	 */
	private FileSigsGCK_Reader() {
	}

	/**
	 * Gets the single instance of FileSigsGCK_Reader.
	 *
	 * @return single instance of FileSigsGCK_Reader
	 */
	public static FileSigsGCK_Reader getInstance() {
		if (instance == null) {
			instance = new FileSigsGCK_Reader();
		}
		return instance;
	}

	/**
	 * Inits the variables.
	 */
	private void initVariables() {
		fileDescription = "";
		fileExtension = "";
		offset = "";
		fileSignature = "";
		category = "";
		trailer = "";
		trailOffset = "";
	}

	/**
	 * Check variables.
	 */
	private void checkVariables() {
		if (fileExtension == null || fileSignature == null) {
			throw new IllegalArgumentException("Extension must have file extension and file signature.");
		}
	}

	/**
	 * Load GTK fs extensions.
	 *
	 * @param file the file
	 * @return the list
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws PresentationException the presentation exception
	 */
	public List<FsExtension> loadGTKFsExtensions(File file) throws IOException, PresentationException {
		try {
			initVariables();
			List<FsExtension> fsExtensions = new ArrayList<FsExtension>();
			FsExtension fsExtension;
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				String[] gtkLine = line.split(",");
				fileDescription = gtkLine[0];
				fileSignature = gtkLine[1];
				category = gtkLine[5];
				String[] fileExtensions = gtkLine[4].split("\\|");
				for (String extension : fileExtensions) {
					fileExtension = extension;
					checkVariables();
					fsExtension = FsExtensionsFactory.getInstance().createFsExtension(fileExtension, fileDescription,
							fileSignature, offset, trailer, trailOffset, category);
					fsExtensions.add(fsExtension);
				}
				fileExtension = null;
			}
			return fsExtensions;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

	/**
	 * Load GTK fs extension.
	 *
	 * @param file the file
	 * @return the fs extension
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws PresentationException the presentation exception
	 */
	public FsExtension loadGTKFsExtension(File file) throws IOException, PresentationException {
		try {
			initVariables();
			FsExtension fsExtension;
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				if (!line.trim().isEmpty()) {
					String[] dotsLine = line.split(":");
					if (dotsLine.length != 2) {
						throw new IllegalArgumentException("Invalid format in file sigs file: " + dotsLine.toString());
					}
					switch (dotsLine[0]) {
					case "File Description":
						fileDescription = dotsLine[1];
						break;
					case "File Extension":
						fileExtension = dotsLine[1];
						break;
					case "Offset":
						offset = dotsLine[1];
						break;
					case "Signature":
						fileSignature = dotsLine[1];
						break;
					case "Category":
						category = dotsLine[1];
						break;
					case "Trailer":
						trailer = dotsLine[1];
						break;
					case "TrailOffset":
						trailOffset = dotsLine[1];
						break;
					}
				}
			}

			checkVariables();
			fsExtension = FsExtensionsFactory.getInstance().createFsExtension(fileExtension, fileDescription,
					fileSignature, offset, trailer, trailOffset, category);
			return fsExtension;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PresentationException(e);
		}
	}

}
