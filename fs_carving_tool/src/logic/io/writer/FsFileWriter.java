package logic.io.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import org.joda.time.LocalDateTime;

import model.FsCase;
import model.FsExtension;
import util.HexUtil;
import util.StringUtil;

/**
 * The Class FsFileWriter.
 */
public class FsFileWriter {

	/** The instance. */
	private static FsFileWriter instance;	

	/**
	 * Instantiates a new fs file writer.
	 */
	private FsFileWriter() {

	}

	/**
	 * Gets the single instance of FsFileWriter.
	 *
	 * @return single instance of FsFileWriter
	 */
	public static FsFileWriter getInstance() {
		if (instance == null) {
			instance = new FsFileWriter();
		}
		return instance;
	}	

	/**
	 * Export metadata to text.
	 *
	 * @param file the file
	 * @param fsCase the fs case
	 * @param metadata the metadata
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void exportMetadataToText(File file, FsCase fsCase, String metadata) throws IOException {
		String outputFile = fsCase.getCaseFolder().concat(File.separator).concat(file.getName()).concat("_metadata_")
				.concat(String.valueOf(Calendar.getInstance().getTimeInMillis()).concat(".txt"));		
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
		StringBuilder sb = new StringBuilder();
		sb.append("Current date: ").append(LocalDateTime.now()).append(StringUtil.newLine);
		sb.append(StringUtil.generateCaseHeader(fsCase));
		sb.append("METADATA EXTRACTION\n");
		sb.append(StringUtil.separator);
		sb.append(StringUtil.generateFileHeader(file));
		sb.append(StringUtil.newLine);
		sb.append(metadata);
		bw.write(sb.toString());
		bw.flush();
		bw.close();
	}
	
	/**
	 * Export file signature analysis to text.
	 *
	 * @param directory the directory
	 * @param fsCase the fs case
	 * @param report the report
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void exportFileSignatureAnalysisToText(File directory, FsCase fsCase, String report) throws IOException{
		String outputFile = fsCase.getCaseFolder().concat(File.separator).concat(directory.getName()).concat("_fileSigAnalysis_")
				.concat(String.valueOf(Calendar.getInstance().getTimeInMillis()).concat(".txt"));		
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
		StringBuilder sb = new StringBuilder();
		sb.append("Current date: ").append(LocalDateTime.now()).append(StringUtil.newLine);
		sb.append(StringUtil.generateCaseHeader(fsCase));
		sb.append("FILE SIGNATURE ANALYSIS\n");
		sb.append(StringUtil.separator);
		sb.append(report);
		bw.write(sb.toString());
		bw.flush();
		bw.close();
	}
	
	/**
	 * Export GCK file extension.
	 *
	 * @param directory the directory
	 * @param extension the extension
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void exportGCKFileExtension(File directory, FsExtension extension) throws IOException {
		String outputFile = directory.getAbsolutePath().concat(File.separator).concat(extension.getDescription()).concat(".txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
		StringBuilder sb = new StringBuilder();
		sb.append("File Description: ").append(extension.getDescription());
		sb.append(StringUtil.newLine).append(StringUtil.newLine);		
		sb.append("File Extension: ").append(extension.getExtension());
		sb.append(StringUtil.newLine).append(StringUtil.newLine);
		sb.append("Offset: ").append(extension.getOffsetLeading());
		sb.append(StringUtil.newLine).append(StringUtil.newLine);
		sb.append("Signature: ").append(HexUtil.bytesToHex(extension.getFileSignature()));
		if(extension.getTrailer() != null) {
			sb.append(StringUtil.newLine).append(StringUtil.newLine);
			sb.append("TrailOffset: ").append(extension.getOffsetTrailer());
			sb.append(StringUtil.newLine).append(StringUtil.newLine);
			sb.append("Trailer: ").append(HexUtil.bytesToHex(extension.getTrailer()));
		}
		bw.write(sb.toString());
		bw.flush();
		bw.close();
	}
	
	/**
	 * Export RDS query to text.
	 *
	 * @param directory the directory
	 * @param fsCase the fs case
	 * @param report the report
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void exportRDSQueryToText(File directory, FsCase fsCase, String report) throws IOException {
		String outputFile = fsCase.getCaseFolder().concat(File.separator).concat(directory.getName()).concat("_RDSReport_")
				.concat(String.valueOf(Calendar.getInstance().getTimeInMillis()).concat(".txt"));		
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
		StringBuilder sb = new StringBuilder();
		sb.append("Current date: ").append(LocalDateTime.now()).append(StringUtil.newLine);
		sb.append(StringUtil.generateCaseHeader(fsCase));
		sb.append("RDS QUERY TOOL\n");
		sb.append(StringUtil.separator);		
		sb.append(report);
		bw.write(sb.toString());
		bw.flush();
		bw.close();
	}
	


}
