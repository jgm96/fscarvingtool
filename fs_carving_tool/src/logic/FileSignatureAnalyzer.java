package logic;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.SAXException;

import model.FsExtension;
import ui.gui.Messages;
import util.HexUtil;
import util.StringUtil;

/**
 * The Class FileSignatureAnalyzer.
 */
public class FileSignatureAnalyzer {

	/** The report. */
	private StringBuilder report;
	
	/** The strings loaded. */
	private boolean stringsLoaded = false;
	
	/** The mismatches. */
	private int mismatches;
	
	/** The unrecognized. */
	private int unrecognized;
	
	/** The files. */
	private List<File> files;
	
	/** The analyzing dir. */
	private String analyzingDir;
	
	/** The analyzing file. */
	private String analyzingFile;
	
	/** The sigs version. */
	private String sigsVersion;
	
	/** The sigs hash. */
	private String sigsHash;
	
	/** The total analyzed. */
	private String totalAnalyzed;
	
	/** The number of mismatches. */
	private String numberOfMismatches;
	
	/** The number of unrecognized. */
	private String numberOfUnrecognized;
	
	/** The seconds. */
	private String seconds;
	
	/** The time elapsed. */
	private String timeElapsed;
	
	/** The expected file signature. */
	private String expectedFileSignature;
	
	/** The metadata extraction. */
	private String metadataExtraction;
	
	/** The matches signature. */
	private String matchesSignature;
	
	/** The matches trailer. */
	private String matchesTrailer;
	
	/** The not matches signature. */
	private String notMatchesSignature;
	
	/** The not matches trailer. */
	private String notMatchesTrailer;
	
	/** The unrecognised file signature. */
	private String unrecognisedFileSignature;	
				

	
	/**
	 * Gets the report.
	 *
	 * @return the report
	 */
	public StringBuilder getReport() {
		return report;
	}
	
	/**
	 * Load strings.
	 */
	private void loadStrings() {
		analyzingDir = Messages.getString("Reports.AnalyzingDir.text");
		analyzingFile = Messages.getString("Reports.AnalysingFile.text");
		sigsVersion = Messages.getString("Reports.SignaturesVersion.text");
		sigsHash = Messages.getString("Reports.SignaturesHash.text");
		totalAnalyzed = Messages.getString("Reports.TotalAnalysed.text");
		numberOfMismatches = Messages.getString("Reports.NumberOfMismatches.text");
		numberOfUnrecognized = Messages.getString("Reports.NumberOfUnRecognized.text");
		seconds = Messages.getString("Reports.Seconds.text");
		timeElapsed = Messages.getString("Reports.ElapsedTime.text");
		expectedFileSignature = "Reports.ExpectedFileSignature.text";
		metadataExtraction = Messages.getString("Reports.MetadataExtraction.text");
		matchesSignature = Messages.getString("Reports.FileExtensionMatches_1.text");
		matchesTrailer = Messages.getString("Reports.TrailerMatches_1.text");
		notMatchesSignature = Messages.getString("Reports.FileExtensionDoesNotMatch_1.text");
		notMatchesTrailer = Messages.getString("Reports.TrailerDoesNotMatch_1.text");
		unrecognisedFileSignature = Messages.getString("Reports.FileExtensionNotRecognised_1.text");
		stringsLoaded = true;
	}

	/**
	 * Analyze directory.
	 *
	 * @param directory the directory
	 * @param fsExtensions the fs extensions
	 * @param recursiveSearch the recursive search
	 * @param extractMetadata the extract metadata
	 * @param showDetailedOutput the show detailed output
	 * @param settings the settings
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String analyzeDirectory(File directory, List<FsExtension> fsExtensions, boolean recursiveSearch,
			boolean extractMetadata, boolean showDetailedOutput, Map<String,String> settings) throws IOException {
		Instant start = Instant.now();
		initialize();		
		StringBuilder summary = new StringBuilder();	
		String sigsVersion_value = settings.get("signaturesVersion");
		String sigsHash_value = settings.get("signaturesHash");				
		report.append(sigsVersion).append(sigsVersion_value).append(StringUtil.newLine).append(StringUtil.newLine);
		report.append(sigsHash).append(sigsHash_value).append(StringUtil.newLine).append(StringUtil.newLine);
		summary.append(StringUtil.HTML_ToBold(sigsVersion)).append(sigsVersion_value)
			.append(StringUtil.htmlTag_lineBreak).append(StringUtil.htmlTag_lineBreak);
		summary.append(StringUtil.HTML_ToBold(sigsHash)).append(sigsHash_value)
			.append(StringUtil.htmlTag_lineBreak).append(StringUtil.htmlTag_lineBreak);
		summary.append(StringUtil.HTML_ToBold(analyzingDir)).append(directory.getAbsolutePath())
				.append(StringUtil.sDots).append(StringUtil.htmlTag_lineBreak).append(StringUtil.htmlTag_lineBreak);
		report.append(analyzingDir).append(directory.getAbsolutePath()).append(StringUtil.sDots)
				.append(StringUtil.newLine).append(StringUtil.newLine);
		getFilesInDirectory(directory, recursiveSearch);
		for (File file : files) {			
			summary.append(analyzeFile(file, fsExtensions, extractMetadata, showDetailedOutput));
		}

		report.append(totalAnalyzed).append(files.size()).append(StringUtil.newLine);
		report.append(numberOfMismatches).append(mismatches).append(StringUtil.newLine);
		report.append(numberOfUnrecognized).append(unrecognized).append(StringUtil.newLine);

		summary.append(StringUtil.HTML_ToBold(totalAnalyzed)).append(files.size()).append(StringUtil.htmlTag_lineBreak);
		summary.append(StringUtil.HTML_ToBold(numberOfMismatches)).append(mismatches)
				.append(StringUtil.htmlTag_lineBreak);
		summary.append(StringUtil.HTML_ToBold(numberOfUnrecognized)).append(unrecognized)
				.append(StringUtil.htmlTag_lineBreak);
		Instant finish = Instant.now();
		long elapsed = Duration.between(start, finish).toMillis();
		float sec = elapsed / 1000F;		
		
		summary.append(StringUtil.newLine).append(StringUtil.newLine);;
		summary.append(StringUtil.HTML_ToBold(timeElapsed)).append(sec).append(seconds);

		report.append(StringUtil.newLine).append(StringUtil.newLine);
		report.append(timeElapsed).append(sec).append(seconds);
		
		return StringUtil.toHTML(summary.toString(), "font-family:Tahoma; font-size: 9px");

	}

	/**
	 * Gets the files in directory.
	 *
	 * @param directory the directory
	 * @param recursiveSearch the recursive search
	 * @return the files in directory
	 */
	public void getFilesInDirectory(File directory, boolean recursiveSearch) {
		for (File fileEntry : directory.listFiles()) {
			if (fileEntry.isDirectory()) {
				if (recursiveSearch) {
					getFilesInDirectory(fileEntry, true);
				}
			} else {
				files.add(fileEntry);
			}
		}
	}

	/**
	 * Analyze file.
	 *
	 * @param file the file
	 * @param fsExtensions the fs extensions
	 * @param extractMetadata the extract metadata
	 * @param showDetailedOutput the show detailed output
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String analyzeFile(File file, List<FsExtension> fsExtensions, boolean extractMetadata, boolean showDetailedOutput) throws IOException {
		String extensionName = FilenameUtils.getExtension(file.getAbsolutePath());
		StringBuilder summary = new StringBuilder();
		String s = file.getName() + StringUtil.sDots;		
		if(showDetailedOutput) {
			summary.append(StringUtil.HTML_ToBold(analyzingFile)).append(s).append(StringUtil.htmlTag_lineBreak);
		}
		report.append(analyzingFile).append(s).append(StringUtil.newLine);
		report.append(StringUtil.generateFileHeader(file));

		List<FsExtension> extensionsByFileName = getFileExtensionsByExtensionName(extensionName, fsExtensions);
		if (extensionsByFileName.isEmpty()) {
			s = MessageFormat.format(unrecognisedFileSignature, extensionName.toUpperCase());			
			report.append(s).append(StringUtil.newLine);
			if(showDetailedOutput) {
				summary.append(s).append(StringUtil.htmlTag_lineBreak);
			}
			unrecognized++;

			report.append(StringUtil.newLine);
			if(showDetailedOutput) {
				summary.append(StringUtil.htmlTag_lineBreak);
			}
			return summary.toString();
		}
		boolean match = false;	
		int i = 0;
		while(i<extensionsByFileName.size() && !match) {
			FsExtension extension = extensionsByFileName.get(i);
			if (extension.getFileSignature() != null) {
				if (extension.getFileSignature().length <= (file.length() + extension.getOffsetLeading())
						&& matchesFileSignature(file, extension.getOffsetLeading(), extension.getFileSignature())) {
					s = MessageFormat.format(matchesSignature, extension.getExtension()) + "("
							+ extension.getDescription() + ")";
					if(showDetailedOutput) {				
						summary.append(StringUtil.HTML_ToGreen(s)).append(StringUtil.htmlTag_lineBreak);
					}
					report.append(s);
					report.append(StringUtil.newLine).append(StringUtil.newLine);
					report.append(StringUtil.generateExtensionHeader(extension));
					match = true;
				} else {
					match = false;
					s = MessageFormat.format(notMatchesSignature, extension.getExtension()) + "("
							+ extension.getDescription() + ")";
					if(showDetailedOutput) {
						summary.append(StringUtil.HTML_ToRed(s)).append(StringUtil.htmlTag_lineBreak);
					}
					report.append(s).append(StringUtil.newLine);
					report.append("Expected file signature: ").append(HexUtil.bytesToHex(extension.getFileSignature()))
							.append(StringUtil.newLine);
				}
			} else if (extension.getTrailer() != null) {
				if (extension.getTrailer().length <= file.length() && matchesFileSignature(file,
						(int) file.length() - extension.getOffsetTrailer(), extension.getTrailer())) {
					s = MessageFormat.format(matchesTrailer, extension.getExtension());					
					if(showDetailedOutput) {
						summary.append(StringUtil.HTML_ToGreen(s)).append(StringUtil.htmlTag_lineBreak);
					}
					report.append(s).append(StringUtil.newLine);
				} else {
					match = false;
					s = MessageFormat.format(notMatchesTrailer, extension.getExtension());					
					if(showDetailedOutput) {
						summary.append(StringUtil.HTML_ToRed(s)).append(StringUtil.htmlTag_lineBreak);
					}
					report.append(s).append(StringUtil.newLine);
					report.append(expectedFileSignature).append(HexUtil.bytesToHex(extension.getTrailer()))
							.append(StringUtil.newLine);
				}
			}
			i++;
		}
		if (!match) {
			mismatches++;
		}
		report.append(StringUtil.newLine);
		if (extractMetadata) {
			try {
				Metadata metadata = MetadataExtractor.getMetadataFromImage(file);
				if (metadata != null) {
					String metadataAcc = "";
					String[] names = metadata.names();
					for (String name : names) {
						metadataAcc += name + ": " + metadata.get(name) + StringUtil.newLine;
					}
					report.append(metadataExtraction).append(StringUtil.newLine);
					report.append(StringUtil.separator);
					report.append(metadataAcc);
					report.append(StringUtil.newLine);
				}
			} catch (TikaException | SAXException e) {
				e.printStackTrace();
			}
		}
		if(showDetailedOutput) {
			summary.append(StringUtil.htmlTag_lineBreak);
		}
		return summary.toString();
	}

	/**
	 * Matches file signature.
	 *
	 * @param inputFile the input file
	 * @param offset the offset
	 * @param signature the signature
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean matchesFileSignature(File inputFile, int offset, byte[] signature) throws IOException {
		RandomAccessFile file = new RandomAccessFile(inputFile, "r");
		int length = signature.length;
		byte[] bytes = new byte[length];
		file.readFully(bytes, offset, length);
		file.close();
		return startsWith(bytes, 0, signature);
	}

	
	/**
	 * Starts with.
	 *
	 * @param file the file
	 * @param offset the offset
	 * @param signature the signature
	 * @return true, if successful
	 */
	public boolean startsWith(byte[] file, int offset, byte[] signature) {
		if (signature.length > (file.length - offset)) {
			return false;
		}
		for (int i = 0; i < signature.length; i++) {
			if (file[offset + i] != signature[i]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Gets the file extensions by signature.
	 *
	 * @param signature the signature
	 * @param fsExtensions the fs extensions
	 * @return the file extensions by signature
	 */
	public List<FsExtension> getFileExtensionsBySignature(byte[] signature, List<FsExtension> fsExtensions) {
		List<FsExtension> result = new ArrayList<FsExtension>();
		for (FsExtension extension : fsExtensions) {
			if (extension.getFileSignature().equals(signature)) {
				result.add(extension);
			}
		}
		return result;
	}

	/**
	 * Gets the file extensions by extension name.
	 *
	 * @param name the name
	 * @param fsExtensions the fs extensions
	 * @return the file extensions by extension name
	 */
	public List<FsExtension> getFileExtensionsByExtensionName(String name, List<FsExtension> fsExtensions) {
		List<FsExtension> result = new ArrayList<FsExtension>();
		for (FsExtension extension : fsExtensions) {
			if (extension.getExtension().equalsIgnoreCase(name)) {
				result.add(extension);
			}
		}
		return result;
	}

	/**
	 * Initialize.
	 */
	private void initialize() {
		report = new StringBuilder();
		mismatches = 0;
		unrecognized = 0;
		files = new ArrayList<File>();
		if(!stringsLoaded) {
			loadStrings();
		}
	}

}
