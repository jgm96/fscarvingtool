package logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;

import org.apache.commons.lang.SystemUtils;

import ui.gui.Messages;
import util.StringUtil;

/**
 * The Class RDSQueryTool.
 */
public class RDSQueryTool {

	/** The hashes. */
	private String hashes;

	/** The strings loaded. */
	private boolean stringsLoaded = false;

	/** The analyzing dir. */
	private String analyzingDir;

	/** The server. */
	private String server;

	/** The port. */
	private String port;

	private HashMap<String, String> hashesMap;

	/** The rds misses hashes str. */
	private String rdsMissesHashesStr;

	/** The elapsed time str. */
	private String elapsedTimeStr;

	/** The seconds. */
	private String seconds;

	/** The rds number of misses str. */
	private String rdsNumberOfMissesStr;

	/** The rds connection error. */
	private String rdsConnectionError;

	/** The rds misses. */
	private int rdsMisses;

	/** The sec. */
	private float sec;

	/** The app. */
	private Application app;

	/**
	 * Instantiates a new RDS query tool.
	 *
	 * @param app the app
	 */
	public RDSQueryTool(Application app) {
		this.app = app;
		initialize();
	}

	/**
	 * Load strings.
	 */
	private void loadStrings() {
		analyzingDir = Messages.getString("Reports.AnalyzingDir.text");
		server = Messages.getString("Reports.Server.text");
		port = Messages.getString("Reports.Port.text");
		rdsMissesHashesStr = Messages.getString("Reports.RDSMissesHashes.text");
		elapsedTimeStr = Messages.getString("Reports.ElapsedTime.text");
		seconds = Messages.getString("Reports.Seconds.text");
		rdsNumberOfMissesStr = Messages.getString("Reports.NumberOfRDSMisses.text");
		rdsConnectionError = Messages.getString("Reports.RDSConnectionError.text");
		stringsLoaded = true;
	}

	/**
	 * Initialize.
	 */
	private void initialize() {
		rdsMisses = 0;
		sec = 0;
		hashes = "";
		hashesMap = new HashMap<String, String>();
	}

	/**
	 * Analyse directory.
	 *
	 * @param directory the directory
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String analyseDirectory(File directory) throws IOException {
		rdsMisses = 0;
		if (!stringsLoaded) {
			loadStrings();
		}
		Instant start = Instant.now();
		StringBuilder sb = new StringBuilder();

		String server = app.getFsSettingsManager().getSettings().get("rdsServer");
		String port = app.getFsSettingsManager().getSettings().get("rdsPort");

		InputStream md5DeepReport = executeM5Deep(directory);
		BufferedReader r = new BufferedReader(new InputStreamReader(md5DeepReport, "UTF-8"));
		String line = null;
		while ((line = r.readLine()) != null) {
			String[] lineContent = line.split("  ");
			System.out.println(lineContent);
			hashesMap.put(lineContent[0].toUpperCase(), lineContent[1]);			
		}

		InputStream result = executeNsrllookup2(server, port, directory);

		r = new BufferedReader(new InputStreamReader(result, "UTF-8"));
		line = null;
		while ((line = r.readLine()) != null) {
			if (line.startsWith("Could")) {
				sb.append(rdsConnectionError).append(" ").append(server);
				break;
			} else {
				sb.append(line).append(" - ").append(hashesMap.get(line)).append(StringUtil.htmlTag_lineBreak);
				rdsMisses++;
			}
		}
		hashes = sb.toString();
		Instant finish = Instant.now();
		long elapsed = Duration.between(start, finish).toMillis();
		sec = elapsed / 1000F;
		return buildSummary(directory);
	}

	/**
	 * Execute nsrllookup.
	 *
	 * @param server the server
	 * @param port   the port
	 * @param hash   the hash
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unused")
	private String executeNsrllookup(String server, String port, String hash) throws IOException {
		String command1 = "cd nsrllookup";
		String command2 = "echo " + hash + " | nsrllookup.exe -u -s " + server + " -p " + port;
		String command = command1 + " && " + command2;
		InputStream result = executeCommand("cmd.exe", "/c", command);
		BufferedReader r = new BufferedReader(new InputStreamReader(result));
		return r.readLine();
	}

	/**
	 * Execute M 5 deep.
	 *
	 * @param directory the directory
	 * @return the input stream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private InputStream executeM5Deep(File directory) throws IOException {
		String command1 = "cd nsrllookup";
		String command2 = "";
		if (SystemUtils.IS_OS_LINUX) {
			command2 = "md5deep64  -r " + " " + directory.getAbsolutePath();
		} else {
			command2 = "md5deep64.exe  -r " + "\"" + directory.getAbsolutePath() + "\"";
		}
		String commands = command1 + " && " + command2;
		if (SystemUtils.IS_OS_LINUX) {
			return executeCommand("bash", "-c", commands);
		}
		else {
			return executeCommand("cmd.exe", "/c", commands);
		}
		
	}

	private InputStream executeNsrllookup2(String server, String port, File directory) throws IOException {
		String command1 = "cd nsrllookup";
		String command2 = "";
		if(SystemUtils.IS_OS_LINUX) {
			command2 = "md5deep  -r " + " " + directory.getAbsolutePath() + " | nsrllookup -u -s " + 
			           server + " -p " + port;
		}
		else {
			command2 = "md5deep64.exe  -r " + "\"" + directory.getAbsolutePath() + "\"" + " | nsrllookup.exe -u -s " + 
					server + " -p " + port;
		}
		String commands = command1 + " && " + command2;		
		if(SystemUtils.IS_OS_LINUX) {
				return executeCommand("bash", "-c", commands);
			}
		else{
			return executeCommand("cmd.exe", "/c", commands);
			}
	}

	/**
	 * Execute command.
	 *
	 * @param exec     the exec
	 * @param param    the param
	 * @param commands the commands
	 * @return the input stream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private InputStream executeCommand(String exec, String param, String commands) throws IOException {
		ProcessBuilder builder = new ProcessBuilder(exec, param, commands);
		builder.redirectErrorStream(true);
		Process p = builder.start();
		return p.getInputStream();
	}

	/**
	 * Builds the summary.
	 *
	 * @param directory the directory
	 * @return the string
	 */
	private String buildSummary(File directory) {
		StringBuilder sb = new StringBuilder();
		sb.append(StringUtil.HTML_ToBold(server)).append(app.getFsSettingsManager().getSettings().get("rdsServer"))
				.append(StringUtil.htmlTag_lineBreak).append(StringUtil.htmlTag_lineBreak);
		sb.append(StringUtil.HTML_ToBold(port)).append(app.getFsSettingsManager().getSettings().get("rdsPort"))
				.append(StringUtil.htmlTag_lineBreak).append(StringUtil.htmlTag_lineBreak);
		sb.append(StringUtil.HTML_ToBold(analyzingDir)).append(directory.getAbsolutePath()).append(StringUtil.sDots)
				.append(StringUtil.htmlTag_lineBreak).append(StringUtil.htmlTag_lineBreak);
		sb.append(StringUtil.HTML_ToBold(rdsMissesHashesStr)).append(StringUtil.htmlTag_lineBreak);
		sb.append(hashes).append(StringUtil.htmlTag_lineBreak).append(StringUtil.htmlTag_lineBreak);
		if (rdsMisses > 0) {
			sb.append(StringUtil.HTML_ToBold(rdsNumberOfMissesStr)).append(rdsMisses)
					.append(StringUtil.htmlTag_lineBreak).append(StringUtil.htmlTag_lineBreak);
		}
		sb.append(StringUtil.HTML_ToBold(elapsedTimeStr)).append(sec).append(seconds)
				.append(StringUtil.htmlTag_lineBreak).append(StringUtil.htmlTag_lineBreak);
		return StringUtil.toHTML(sb.toString(), "font-family:Tahoma; font-size: 9px");
	}

	/**
	 * Gets the report.
	 *
	 * @param directory the directory
	 * @return the report
	 */
	public String getReport(File directory) {
		StringBuilder sb = new StringBuilder();
		sb.append(server).append(app.getFsSettingsManager().getSettings().get("rdsServer")).append(StringUtil.newLine)
				.append(StringUtil.newLine);
		sb.append(port).append(app.getFsSettingsManager().getSettings().get("rdsPort")).append(StringUtil.newLine)
				.append(StringUtil.newLine);
		sb.append(analyzingDir).append(directory.getAbsolutePath()).append(StringUtil.sDots).append(StringUtil.newLine)
				.append(StringUtil.newLine);
		sb.append(rdsMissesHashesStr).append(StringUtil.newLine);
		sb.append(hashes.replaceAll("<br>", StringUtil.newLine)).append(StringUtil.newLine).append(StringUtil.newLine);
		if (rdsMisses > 0) {
			sb.append(rdsNumberOfMissesStr).append(rdsMisses).append(StringUtil.newLine).append(StringUtil.newLine);
		}
		return sb.toString();
	}

	/**
	 * Gets the hashes.
	 *
	 * @return the hashes
	 */
	public String getHashes() {
		return hashes;
	}

}
