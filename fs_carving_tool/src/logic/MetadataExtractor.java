package logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

/**
 * The Class MetadataExtractor.
 */
public class MetadataExtractor {

	/**
	 * Gets the metadata from image.
	 *
	 * @param f the f
	 * @return the metadata from image
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TikaException the tika exception
	 * @throws SAXException the SAX exception
	 */
	public static Metadata getMetadataFromImage(File f) throws IOException, TikaException, SAXException {		
		Parser parser = new AutoDetectParser();
		BodyContentHandler handler = new BodyContentHandler(-1);
		Metadata metadata = new Metadata();
		FileInputStream inputstream = new FileInputStream(f);
		ParseContext context = new ParseContext();
		parser.parse(inputstream, handler, metadata, context);
		return metadata;
	}
	
	/**
	 * Gets the media type.
	 *
	 * @param f the f
	 * @return the media type
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static String getMediaType(File f) throws IOException {
		Detector detector = new DefaultDetector();
	    Metadata metadata = new Metadata();
	    FileInputStream inputstream = new FileInputStream(f);
	    MediaType mediaType = detector.detect(inputstream, metadata);
	    return mediaType.toString();
	}	

}
